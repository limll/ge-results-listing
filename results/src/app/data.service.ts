import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";


@Injectable({
  providedIn: "root"
})
export class DataService {
  constructor(private http: HttpClient) {}

  retrieveAllData() {
    return this.http.get(
      "https://st-graphics-dev-json.s3-ap-southeast-1.amazonaws.com/1HH5MLZu2lukbjDGMWhW_wdT7Jm0PxJN37i6EC3CaaD0/1665104133.json"
    );
  }

  retreiveParty() {
    return this.http.get(
      "https://spreadsheets.google.com/feeds/list/1DAKQDjXNP-HUyLAEiLeb4s-dbrijWbFqDiqjRG4sfeM/2/public/values?alt=json"
    );
  }

  retreiveConsituency() {
    return this.http.get(
      "https://spreadsheets.google.com/feeds/list/1DAKQDjXNP-HUyLAEiLeb4s-dbrijWbFqDiqjRG4sfeM/1/public/values?alt=json"
    );
  }

}
