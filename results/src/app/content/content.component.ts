import { Component, OnInit } from '@angular/core';
import { DataService } from "../data.service";
import * as Highcharts from 'highcharts';
import Itemchart from 'highcharts/modules/item-series';
import $ from 'jquery';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss', '../app.component.scss']
})
export class ContentComponent implements OnInit {
 
  data;
  party = [];
  constituency;
  totalSeats = 0;
  filteredArr;
  chart;

  constructor(private dataService: DataService) {
    Itemchart(Highcharts);

    this.dataService.retrieveAllData().subscribe(data => {
        this.data = data;
        this.getParty();
        // console.log(this.data)
    })

    this.dataService.retreiveConsituency().subscribe(data => {
      this.constituency = data['feed']['entry'];
      
      // console.log(this.constituency);
      // console.log(this.data)
    })
   
     
        
   }

  ngOnInit(): void {
    
  }

  loadChart(){
    
    this.chart = Highcharts.chart('chart-container', {
      chart: {
        type: 'item'
      },
      title: {
        text: ''
      },
      legend: {
        labelFormat: '{name} <span style="opacity: 0.4">{y}</span>',
        symbolHeight: 10,
        symbolWidth: 10,
        symbolRadius: 0
        
      },
      series: [
        {
          name: '赢得议席',
          marker : {
            symbol : 'square'
          },
          
          type: undefined,
          keys: ['name', 'y', 'color', 'label'],
          data: [
            ["人民行动党", 10, "#ff4351", "人民行动党"],
        
          ],
          dataLabels: {
            enabled: true,
            format: '{point.label}'
         
          }
        }
      ]
    
    });
  }

  getParty(){
    this.dataService.retreiveParty().subscribe(data => {
      
      this.party = data['feed']['entry'];
      for(let i =0; i < this.party.length; i++){
        this.party[i]['noOfSeats'] = 0;
        this.party[i]['votes'] = 0;
        this.party[i]['total_voters'] = 0;
      }
      this.calculateTotalSeats();
      this.getPartyVotes();
      // this.chartData();
      // console.log(this.data)
    })
  }

  calculateTotalSeats() {
    this.totalSeats = 0;
    const seen = new Set();
    this.filteredArr = this.data.filter(el => {
      const duplicate = seen.has(el.constituency);
      seen.add(el.constituency);
      return !duplicate;
    });

    for(let i = 0; i < this.filteredArr.length; i++){
      this.totalSeats += this.filteredArr[i]['constituency_type_index']
    }
    // console.log(this.totalSeats);
  }

  getPartyVotes(){
  
      let data =[]

      for(let i = 0; i < this.data.length; i++){
        for(let j = 0; j < this.party.length; j++){
          if(this.data[i]['party'] === this.party[j]['gsx$partyen']['$t']){
            if(this.data[i]['ge2020_validvotes'] != undefined){
              this.party[j]['votes'] += this.data[i]['ge2020_validvotes'];
              this.party[j]['total_voters'] += this.data[i]['ge2020_voters'];
              this.party[j]['percentage'] = (this.party[j]['votes'] / this.party[j]['total_voters']) * 100;
              this.party[j]['difference'] = this.party[j]['percentage'] - this.party[j]['gsx$percentage2015']["$t"];
              this.party[j]['noOfSeats'] += this.data[i]['constituency_type_index'];
             
            
            }
          }
        }
      }

      
      for(let j = 0; j < this.party.length; j++){
        data.push([this.party[j]['gsx$partycn']['$t'], this.party[j]['noOfSeats'], this.party[j]['gsx$color']['$t'], this.party[j]['gsx$partycn']['$t']])
      }
    //  this.chartData()
    this.loadChart();
      this.chart.series[0].update({
        data: data
      }, true);
      
      this.chart.redraw;
      // console.log(this.party);
      $('.highcharts-credits').css('display', 'none');
    }

    /***** DO NOT DELETE. FOR BACKUP ******/
    
    // chartData(){
    //   let chartData = [];
    //   let constituency = this.constituency[0]['gsx$cn']['$t']
    //   for(let i = 0; i < this.data.length; i++){
    //     for(let j = 0; j < this.constituency.length; j++){
    //       if(this.data[i]['constituency'] === this.constituency[j]['gsx$en']['$t']){
    //         if(this.data[i]['constituency_type_index'] !== 0){
    //           let numofSeats = 0;
    //           let party;
    //           let colour;
    //           if(constituency !== this.constituency[j]['gsx$cn']['$t']){
    //             constituency = this.constituency[j]['gsx$cn']['$t'];
    //             numofSeats += this.data[i]['constituency_type_index'];
    //             party = this.data[i]['party'];
    //             chartData.push([constituency, numofSeats, party, constituency])
    //             // console.log(constituency + " " + numofSeats + " " + party);
    //           }
    //         }
    //       }
    //     }
    //   }
    //   // console.log(chartData.length);
    //   // console.log(this.party);
    //  for(let i =0; i < this.party.length; i++){
    //   for(let j = 0; j < chartData.length; j++){
    //     // console.log(chartData[i]);
    //     if(chartData[j][2] === this.party[i]['gsx$partyen']['$t']) {
    //       chartData[j][2] = this.party[i]['gsx$color']['$t'];
    //     }
    //   }
    //  }
    // //  console.log(chartData)
    //  this.loadChart();
    //  this.chart.series[0].update({
    //    data: chartData
    //  }, true);
     
    //  this.chart.redraw;
    //  // console.log(this.party);
    //  $('.highcharts-credits').css('display', 'none');
    // }
    
    

}
