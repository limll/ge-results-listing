
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"> 
<meta charset="utf-8">

<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
dataLayer.push({
    "level2": "INTERACTIVE-NAME",
    "title": "HEADLINE TITLE",
});
</script>
<!-- Google Tag Manager
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVFMLMK');</script> -->
<!-- End Google Tag Manager -->

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="index,follow">
<meta name="GOOGLEBOT" content="index,follow">
<meta http-equiv="cache-control" content="max-age=0, no-cache, no-store, must-revalidate" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="author" content="" />

<title>大选指南</title>
<meta name="description" content="SOMETHING SHORT AND BRIEF ENOUGH TO DESCRIBE THE TOPIC" />
<meta name="keywords" content="早报, KEYWORDS HIGHLY RELEVANT TO THE TOPIC, NAMES ARE IMPORTANT TOO" />
<link rel="image_src" href="//interactive.zaobao.com/YYYY/FOLDER-NAME/IMGS/SOCIAL-SHARE-IMG" />

<?php include "assets/inc/global.html"?>

</head>

<body id="page-polling-guide">
<?php include "assets/inc/gtm.html"?>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

<?php include "assets/inc/brand.html"?>

  <div class="content mdl-layout__content">
    <a name="top"></a>

    <div class="tab-wrap no-swiper">

      <div class="tab-container">
        <div class="000tab-container-inner">
          <div class="000swiper-wrapper">
            <div class="000swiper-slide">
              <div class="content-section 000full-width">
                <p class="padding-10">在这场前所未有的“疫情大选”中，许多你我熟悉的竞选和投票程序都将出现变化。作为选民，有哪些你必须注意的事项？掌握以下重点，便可轻松适应本届大选的“新常态”.</p>
              </div>

              <div class="000mdl-grid">
                <nav class="stick-nav">
                  <a class="stick-nav-link" href="#schedule">时间点</a>
                  <a class="stick-nav-link" href="#poll-station">投票站新设备</a>
                  <a class="stick-nav-link" href="#procedure">投票日新程序</a>
                  <a class="stick-nav-link" href="#faq">必要须知</a>
                </nav>

                <div class="segment" id="schedule"><div class="content-section">
                  <h2>下来该注意哪些重要时间点？</h2>

                  <div class="segment-box schedule">
                    <p>根据法定要求，政府最迟得在2021年4月举行大选，而大选前一般有五大值得关注的时间点，标志着大选的脚步已经逼近。</p>
                    <ul class="classic timeline first">
                      <li>
                        <div class="sub-title"><span>设立选区划分委员会</span></div>
                        <div class="desc"><em class="num">08.01.2019</em>政府已经在2019年8月1日设立选区划分委员会，根据以往大选的经验，选区划分委员会一般会在成立后两到七个月之间公布选区划分委员会报告。</div>
                      </li>
                      <li>
                        <div class="sub-title"><span>选区划分委员会报告出炉</span></div>
                        <div class="desc"><em class="num">03.13.2020</em>
                          此次报告于今年3月13日出炉，距离委员会成立有7个多月，算是间隔时间较长的一次。<br /><br />选区划分委员会报告出炉，代表大选战火正式点燃。各个政党得确定候选人名单、标语和竞选政纲，并向选民介绍候选人。</div>
                      </li>
                      <li>
                        <div class="sub-title"><span>解散国会颁布选举令状</span></div>
                        <div class="desc"><em class="num">00.00.2020</em>
                          报告出炉的下一步，就是解散国会。总统会根据总理的提议，宣布解散国会和颁布选举令状。<br /><br />在2001年大选中，政府在报告出炉后一天内解散国会；2011年大选中，则是过了1个月26天才解散国会，是历届大选中间隔时间最长的一次。今年报告出炉后碰上两个月的病毒阻断措施，<u>【至今已经超过三个月，间隔时间是2011年的两倍。】</u></div>
                      </li>
                    </ul>
                    <ul class="classic timeline last">
                      <li>
                        <div class="sub-title"><span>提名日</span></div>
                        <div class="desc"><em class="num">00.00.2020</em>
                          提名日必须在选举令状颁布后不少于五天、不超过一个月内举行，日期由总理宣布。在以往大选中，一般会在选举令状颁布的一周后举行提名日。换言之，选举令状在星期一颁布，下星期一就是提名日。</div>
                      </li>
                      <li>
                        <div class="sub-title"><span>投票日</span></div>
                        <div class="desc"><em class="num">00.00.2020</em>
                          一般是星期六，也是公定假日。投票站从早上8时，开到晚上8时。<br /><br />竞选期从提名日当天开始计算，根据法令，投票日最早可在竞选期的第10天举行，最迟得在第56天举行。过往大选中，投票日一般会在竞选期第11天或之前举行，竞选期一般介于9到10天。竞选期结束后、投票日之前，有一天的冷静日，各候选人在这24小时内不得举办任何竞选活动，以便让选民有时间静下心来确定自己的选择。</div>
                      </li>
                    </ul>

                  </div>
                </div></div>

              </div>



            </div>
            
          </div>
        </div>
      </div>

    </div>

  </div>
</div>
<?php include "assets/inc/news-feed.html"?>
<?php include "assets/inc/footer.html"?>

<?php include "assets/inc/scripts.html"?>
<script>
$(document).ready(function() {
    $('a[href*=#]').bind('click', function(e) {
        e.preventDefault();
        var target = $(this).attr("href");
        $('html, body').stop().animate({
            scrollTop: $(target).offset().top
        }, 600, function() {
            location.hash = target;
        });
        return false;
    });
});

$(window).scroll(function() {
    var scrollDistance = $(window).scrollTop();
    $('.segment').each(function(i) {
        if ($(this).position().top <= scrollDistance) {
            $('.stick-nav a.active').removeClass('active');
            $('.stick-nav a.stick-nav-link').eq(i).addClass('active');
        }
    });
}).scroll();
</script>

</body>
</html>
