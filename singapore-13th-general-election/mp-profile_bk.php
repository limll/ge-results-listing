
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"> 
<meta charset="utf-8">

<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
dataLayer.push({
    "level2": "INTERACTIVE-NAME",
    "title": "HEADLINE TITLE",
});
</script>
<!-- Google Tag Manager
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVFMLMK');</script> -->
<!-- End Google Tag Manager -->

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="index,follow">
<meta name="GOOGLEBOT" content="index,follow">
<meta http-equiv="cache-control" content="max-age=0, no-cache, no-store, must-revalidate" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="author" content="" />

<title>候选人与选民的距离</title>
<meta name="description" content="SOMETHING SHORT AND BRIEF ENOUGH TO DESCRIBE THE TOPIC" />
<meta name="keywords" content="早报, KEYWORDS HIGHLY RELEVANT TO THE TOPIC, NAMES ARE IMPORTANT TOO" />
<link rel="image_src" href="//interactive.zaobao.com/YYYY/FOLDER-NAME/IMGS/SOCIAL-SHARE-IMG" />

<link rel="stylesheet" href="//interactive.zaobao.com/lib/css/bootstrap/v3.3.7/bootstrap.min.css">
<?php include "assets/inc/global.html" ?>
</head>

<body id="page-mp-profile" ng-app="App.controllers" ng-controller="MPCtrl" ng-cloak>
<?php include "assets/inc/gtm.html" ?>

<div class="000mdl-layout mdl-js-layout mdl-layout--fixed-header">

<?php include "assets/inc/brand.html" ?>

  <div class="content 000mdl-layout__content">
    <a name="top"></a>

    <div class="tab-wrap no-swiper">
      <div class="tab-container">
        <div class="tab-container-inner">
          <div class="000swiper-wrapper">
            <div class="swiper-slide">
              <div class="content-section full-width">
                <div id="MPFilter">
                  <div id="sticky"><div id="filter-option">
                    <select name="constituency" id="constituency" class="select">
                      <option value="0">选区</option>
                      <option value="阿裕尼集选区">阿裕尼集选区</option>
                      <option value="宏茂桥集选区">宏茂桥集选区</option>
                      <option value="碧山—大巴窑集选区">碧山—大巴窑集选区</option>
                      <option value="武吉巴督单选区">武吉巴督单选区</option>
                      <option value="武吉班让单选区">武吉班让单选区</option>
                      <option value="蔡厝港集选区">蔡厝港集选区</option>
                      <option value="东海岸集选区">东海岸集选区</option>
                      <option value="荷兰—武吉知马集选区">荷兰—武吉知马集选区</option>
                      <option value="丰加北单选区">丰加北单选区</option>
                      <option value="后港单选区">后港单选区</option>
                      <option value="后港单选区">后港单选区</option>
                      <option value="裕廊集选区">裕廊集选区</option>
                      <option value="哥本峇鲁单选区">哥本峇鲁单选区</option>
                      <option value="麦波申单选区">麦波申单选区</option>
                      <option value="马林百列集选区">马林百列集选区</option>
                      <option value="马西岭—油池集选区">马西岭—油池集选区</option>
                      <option value="玛丽蒙单选区">玛丽蒙单选区</option>
                      <option value="蒙巴登单选区">蒙巴登单选区</option>
                      <option value="义顺集选区">义顺集选区</option>
                      <option value="白沙—榜鹅集选区">白沙—榜鹅集选区</option>
                      <option value="先驱单选区">先驱单选区</option>
                      <option value="波东巴西单选区">波东巴西单选区</option>
                      <option value="榜鹅西单选区">榜鹅西单选区</option>
                      <option value="拉丁马士单选区">拉丁马士单选区</option>
                      <option value="三巴旺集选区">三巴旺集选区</option>
                      <option value="盛港集选区">盛港集选区</option>
                      <option value="淡滨尼集选区">淡滨尼集选区</option>
                      <option value="丹戎巴葛集选区">丹戎巴葛集选区</option>
                      <option value="西海岸集选区">西海岸集选区</option>
                      <option value="杨厝港单选区">杨厝港单选区</option>
                      <option value="裕华单选区">裕华单选区</option>
                    </select>
                    <select name="party" id="party" class="select">
                      <option value="0">政党</option>
                      <option value="People's Action Party">人民行动党</option>
                      <option value="Workers' Party">工人党</option>
                      <option value="Singapore Democratic Party">新加坡民主党</option>
                      <option value="Progress Singapore Party">新加坡前进党</option>
                      <option value="Singapore People's Party">新加坡人民党</option>
                      <option value="Reform Party">革新党</option>
                      <option value="Singapore Democratic Alliance">新加坡民主联盟</option>
                      <option value="Singaporeans First">国人为先党</option>
                      <option value="National Solidarity Party">国民团结党</option>
                      <option value="People's Power Party">人民力量党</option>
                      <option value="People's Voice">人民之声</option>
                      <option value="Democratic Progressive Party">民主进步党</option>
                      <option value="Independent Candidate">独立候选人</option>
                    </select>
                    <select name="term" id="term" class="select">
                      <option value="0">任期</option>
                      <option value="2">一届</option>
                      <option value="3">两届</option>
                      <option value="4">三届或以上</option>
                    </select>
                    <select name="gender" id="gender" class="select">
                      <option value="0">性别</option>
                      <option value="2">男</option>
                      <option value="3">女</option>
                    </select>
                    <select name="participation" id="participation" class="select">
                      <option value="0">新面孔</option>
                      <option value="2">新人</option>
                      <option value="3">老将</option>
                    </select>

                    <select name="sort" id="sort" class="select sort">
                      <option value="0">排序方式</option>
                      <option value="years">从政资历</option>
                      <option value="age">年龄</option>
                    </select>

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable filter-search">
                      <label class="mdl-button mdl-js-button mdl-button--icon" for="site-search">
                        <i class="material-icons">search</i>
                      </label>
                      <div class="mdl-textfield__expandable-holder">
                        <input class="mdl-textfield__input" type="search" id="site-search" placeholder="快捷搜索" />
                        <label class="mdl-textfield__label" for="site-search">Search</label>
                      </div>
                    </div>
                    <!--div id="event-change"></div-->
                  </div></div>
                  
                  <div class="mp-panel">
                    <div class="row list">
                      <div class="active-with-click">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <article class="material-card party-member-info">
                            <h2 class="party-member-label">
                              <div class="member-territory">
                                <div class="member-name"><span class="ch">安宁阿敏</span></div>
                                <div class="constituency-name"><span class="ch">三巴旺集选区</span></div>
                              </div>
                              <div class="party-logo party-pap"></div>
                            </h2>
                            <div class="mc-content">
                              <div class="img-container"><img class="profile-img" src="party/imgs/pap/amin-amrin.png"></div>
                              <div class="mc-detail">
                                <div class="member-designation">内政部兼卫生部高级政务次长</div>
                                <div class="result-panel">
                                  <div class="header-title">大选成绩</div>
                                  <div class="election-performance">
                                    <div class="election-vote current">
                                      <label>2020年</label>
                                      <span>72.3<em>%</em></span>
                                      <small>13万5115票</small>
                                    </div>
                                    <div class="election-vote current">
                                      <label>2015年</label>
                                      <span>72.3<em>%</em></span>
                                      <small>9万6639票</small>
                                    </div>
                                  </div>
                                </div>
                                <div class="serving-term">
                                  <label>任期</label>
                                  <div class="term-no"><span>2015</span></div>
                                </div>
                                <div class="member-profile">
                                  <div class="member-age"><span class="en">42</span><em>岁</em></div> 2015年加入政坛，之前是律师。
                                </div>
                                <div class="get-social"><a href="//www.facebook.com/amrin.page/" class="mdi mdi-facebook icon-facebook" target="_blank"></a></div>
                              </div>
                            </div>
                            <a class="mc-btn-action"><i class="mdi mdi-arrow-down"></i></a>
                            <div class="mc-footer">
                              <div class="party-profile">
                                <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                                <div class="party-logo party-pap"></div>
                              </div>
                            </div>
                          </article>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <article class="material-card party-member-info">
                            <h2 class="party-member-label">
                              <div class="member-territory">
                                <div class="member-name"><span class="ch">洪鼎基</span></div>
                                <div class="constituency-name"><span class="ch">宏茂桥集选区</span></div>
                              </div>
                              <div class="party-logo party-pap"></div>
                            </h2>
                            <div class="mc-content">
                              <div class="img-container"><img class="profile-img" src="party/imgs/pap/ang-hin-kee.png"></div>
                              <div class="mc-detail">
                                <div class="member-designation">议员</div>
                                <div class="result-panel">
                                  <div class="header-title">大选成绩</div>
                                  <div class="election-performance">
                                    <div class="election-vote current">
                                      <label>2020年</label>
                                      <span>78.6<em>%</em></span>
                                      <small>13万5115票</small>
                                    </div>
                                    <div class="election-vote current">
                                      <label>2015年</label>
                                      <span>78.6<em>%</em></span>
                                      <small>13万5115票</small>
                                    </div>
                                  </div>
                                </div>
                                <div class="serving-term">
                                  <label>任期</label>
                                  <div class="term-no"><span>2015</span><span>2011</span></div>
                                </div>
                                <div class="member-profile">
                                  <div class="member-age"><span class="en">55</span><em>岁</em></div> 2011年加入政坛，是全国德士师傅协会和全国私人出租车司机协会顾问，也是职总助理总干事兼自由业者与自雇署署长。
                                </div>
                                <div class="get-social"><a href="//www.facebook.com/anghinkee/" class="mdi mdi-facebook icon-facebook" target="_blank"></a></div>
                              </div>
                            </div>
                            <a class="mc-btn-action"><i class="mdi mdi-arrow-down"></i></a>
                            <div class="mc-footer">
                              <div class="party-profile">
                                <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                                <div class="party-logo party-pap"></div>
                              </div>
                            </div>
                          </article>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <article class="material-card party-member-info">
                            <h2 class="party-member-label">
                              <div class="member-territory">
                                <div class="member-name"><span class="ch">洪维能</span></div>
                                <div class="constituency-name"><span class="ch">裕廊集选区</span></div>
                              </div>
                              <div class="party-logo party-pap"></div>
                            </h2>
                            <div class="mc-content">
                              <div class="img-container"><img class="profile-img" src="party/imgs/pap/ang-wei-neng.png"></div>
                              <div class="mc-detail">
                                <div class="member-designation">议员</div>
                                <div class="result-panel">
                                  <div class="header-title">大选成绩</div>
                                  <div class="election-performance">
                                    <div class="election-vote current">
                                      <label>2020年</label>
                                      <span>79.3<em>%</em></span>
                                      <small>9万5080票</small>
                                    </div>
                                    <div class="election-vote current">
                                      <label>2015年</label>
                                      <span>79.3<em>%</em></span>
                                      <small>9万5080票</small>
                                    </div>
                                  </div>
                                </div>
                                <div class="serving-term">
                                  <label>任期</label>
                                  <div class="term-no"><span>2015</span><span>2011</span></div>
                                </div>
                                <div class="member-profile">
                                  <div class="member-age"><span class="en">53</span><em>岁</em></div> 2011年加入政坛，是康福德高德士总裁。
                                </div>
                                <div class="get-social"><a href="//www.facebook.com/angweineng.mp/" class="mdi mdi-facebook icon-facebook" target="_blank"></a></div>
                              </div>
                            </div>
                            <a class="mc-btn-action"><i class="mdi mdi-arrow-down"></i></a>
                            <div class="mc-footer">
                              <div class="party-profile">
                                <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                                <div class="party-logo party-pap"></div>
                              </div>
                            </div>
                          </article>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <article class="material-card party-member-info">
                            <h2 class="party-member-label">
                              <div class="member-territory">
                                <div class="member-name"><span class="ch">马炎庆</span></div>
                                <div class="constituency-name"><span class="ch">淡滨尼集选区</span></div>
                              </div>
                              <div class="party-logo party-pap"></div>
                            </h2>
                            <div class="mc-content">
                              <div class="img-container"><img class="profile-img" src="party/imgs/pap/baey-yam-keng.png"></div>
                              <div class="mc-detail">
                                <div class="member-designation">交通部兼文化、社区及青年部高级政务次长</div>
                                <div class="result-panel">
                                  <div class="header-title">大选成绩</div>
                                  <div class="election-performance">
                                    <div class="election-vote current">
                                      <label>2020年</label>
                                      <span>72.1<em>%</em></span>
                                      <small>9万5202票</small>
                                    </div>
                                    <div class="election-vote current">
                                      <label>2015年</label>
                                      <span>72.1<em>%</em></span>
                                      <small>9万5202票</small>
                                    </div>
                                  </div>
                                </div>
                                <div class="serving-term">
                                  <label>任期</label>
                                  <div class="term-no"><span>2015</span><span>2011</span><span>2006</span></div>
                                </div>
                                <div class="member-profile">
                                  <div class="member-age"><span class="en">50</span><em>岁</em></div> 2006年加入政坛，2015年受委政治职务，也是华社自助理事会董事会成员。
                                </div>
                                <div class="get-social"><a href="//www.facebook.com/BaeyYamKeng/" class="mdi mdi-facebook icon-facebook" target="_blank"></a></div>
                              </div>
                            </div>
                            <a class="mc-btn-action"><i class="mdi mdi-arrow-down"></i></a>
                            <div class="mc-footer">
                              <div class="party-profile">
                                <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                                <div class="party-logo party-pap"></div>
                              </div>
                            </div>
                          </article>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <article class="material-card party-member-info">
                            <h2 class="party-member-label">
                              <div class="member-territory">
                                <div class="member-name"><span class="ch">陈慧玲</span></div>
                                <div class="constituency-name"><span class="ch">凤山单选区</span></div>
                              </div>
                              <div class="party-logo party-pap"></div>
                            </h2>
                            <div class="mc-content">
                              <div class="img-container"><img class="profile-img" src="party/imgs/pap/cheryl-chan.png"></div>
                              <div class="mc-detail">
                                <div class="member-designation">议员</div>
                                <div class="result-panel">
                                  <div class="header-title">大选成绩</div>
                                  <div class="election-performance">
                                    <div class="election-vote current">
                                      <label>2020年</label>
                                      <span>57.5<em>%</em></span>
                                      <small>1万2398票</small>
                                    </div>
                                    <div class="election-vote current">
                                      <label>2015年</label>
                                      <span>57.5<em>%</em></span>
                                      <small>1万2398票</small>
                                    </div>
                                  </div>
                                </div>
                                <div class="serving-term">
                                  <label>任期</label>
                                  <div class="term-no"><span>2015</span></div>
                                </div>
                                <div class="member-profile">
                                  <div class="member-age"><span class="en">44</span><em>岁</em></div> 2015年以政治新人的身份对垒工人党陈立峰，最终夺下凤山单选区。她是能源公司副总裁，也是人民行动党妇女团成员。
                                </div>
                                <div class="get-social"><a href="//www.facebook.com/cherylchanwl/" class="mdi mdi-facebook icon-facebook" target="_blank"></a></div>
                              </div>
                            </div>
                            <a class="mc-btn-action"><i class="mdi mdi-arrow-down"></i></a>
                            <div class="mc-footer">
                              <div class="party-profile">
                                <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                                <div class="party-logo party-pap"></div>
                              </div>
                            </div>
                          </article>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <article class="material-card party-member-info">
                            <h2 class="party-member-label">
                              <div class="member-territory">
                                <div class="member-name"><span class="ch">李显龙</span></div>
                                <div class="constituency-name"><span class="ch">宏茂桥集选区</span></div>
                              </div>
                              <div class="party-logo party-pap"></div>
                            </h2>
                            <div class="mc-content">
                              <div class="img-container"><img class="profile-img" src="party/imgs/pap/lee-hsien-loong.png"></div>
                              <div class="mc-detail">
                                <div class="member-designation">总理、人民行动党秘书长</div>
                                <div class="result-panel">
                                  <div class="header-title">大选成绩</div>
                                  <div class="election-performance">
                                    <div class="election-vote current">
                                      <label>2020年</label>
                                      <span>78.6<em>%</em></span>
                                      <small>13万5115票</small>
                                    </div>
                                    <div class="election-vote current">
                                      <label>2015年</label>
                                      <span>78.6<em>%</em></span>
                                      <small>13万5115票</small>
                                    </div>
                                  </div>
                                </div>
                                <div class="serving-term">
                                  <label>任期</label>
                                  <div class="term-no"><span>2015</span><span>2011</span><span>2006</span><span>2001</span><span>1996</span><span>1991</span><span>1988</span><span>1984</span></div>
                                </div>
                                <div class="member-profile">
                                  <div class="member-age"><span class="en">68</span><em>岁</em></div> 李显龙总理是新加坡第三任总理，1984年加入政坛，2004年担任总理，已带领行动党出征三届大选。他近年来积极通过面簿与公众互动，包括分享不少私下出游的照片，因此也被喻为“新加坡第一网红”。
                                </div>
                                <div class="get-social"><a href="//www.facebook.com/leehsienloong/" class="mdi mdi-facebook icon-facebook" target="_blank"></a></div>
                              </div>
                            </div>
                            <a class="mc-btn-action"><i class="mdi mdi-arrow-down"></i></a>
                            <div class="mc-footer">
                              <div class="party-profile">
                                <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                                <div class="party-logo party-pap"></div>
                              </div>
                            </div>
                          </article>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>

    </div>

  </div>
</div>

<?php include "assets/inc/news-feed.html"?>
<?php include "assets/inc/footer.html"?>
<script src="../lib/js/jquery/v3.3.1/jquery.min.js"></script>
<script src="../lib/js/angular/v1.4.9/angular.min.js"></script>
<script src="../lib/js/angular/angular-sanitize/v1.5.8/angular-sanitize.min.js"></script>
<script src="assets/js/controllers.js"></script>
<script src="../lib/js/progressbar-prognroll.js"></script>
<script src="assets/js/swiper.jquery-v3.3.1.min.js"></script>
<script src="../lib/js/material-design/material-v1.3.0.min.js"></script>
<script src="assets/js/script.js"></script>

<script src="assets/js/listjs-v1.5.0.min.js"></script>
<script src="assets/js/tinysort-2.2.2.min.js" type="text/javascript" charset="utf-8"></script>
<script src="assets/js/script-filter.js"></script>

</body>
</html>
