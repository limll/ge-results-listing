(function() {

  "use strict";

  var App = angular.module("App.controllers", []);

  App.filter("unsafe", function($sce) {
    return $sce.trustAsHtml;
  });
 

  App.controller("MainCtrl",function ($scope, $http) {
    
    $scope.newsFeeds = [];
    $scope.allDatas = [];
    $scope.partyName = [];
    

  	$scope.getNewsFeeds = function(){
    
       $http({
         method: "GET",
         url: "assets/php/getNewsFeeds.php",
         async: true,
         cache: false,
         headers: {
           "Content-Type": "application/x-www-form-urlencoded"
         },
         transformRequest: function(obj) {
           var str = [];
           for (var p in obj)
             str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
           return str.join("&");
         },
         data: {}
       })
         .success(function($data, $status, $headers, $config) {

           if ($data) {
            //  console.log($data);
            for(var i = 0; i < 4; i++){
              $scope.newsFeeds.push($data.hits[i])
            }
            // console.log($scope.newsFeeds)
             
           } else {
           }
         })
         .error(function($data, $status, $headers, $config) {
           // if (failurecb) {
           //     failurecb($data, $status, $headers, $config, "Error retreiving data");
           // }
         });
  }
  $scope.getNewsFeeds();

  $scope.getOverallData = function(){
    $http({
      method: "GET",
      url: "https://spreadsheets.google.com/feeds/list/1GxVVDj4TwbaeVnoImxp8nqXxd2C9b_56FrEfeCBe4pQ/1/public/values?alt=json",
      async: true,
      cache: false,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      transformRequest: function(obj) {
        var str = [];
        for (var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      },
      data: {}
    })
      .success(function($data, $status, $headers, $config) {

        if ($data) {
          $scope.allDatas = $data.feed.entry;
          $scope.calTotalContituency();
          // console.log($scope.allDatas);
          $scope.loadDataForMobile();
          mapClick();
          
        } else {
        }
      })
      .error(function($data, $status, $headers, $config) {
        // if (failurecb) {
        //     failurecb($data, $status, $headers, $config, "Error retreiving data");
        // }
      });

  }
  $scope.getOverallData();

  $scope.getNumberofVoters = function(){
    $http({
      method: "GET",
      url: "https://spreadsheets.google.com/feeds/list/1GxVVDj4TwbaeVnoImxp8nqXxd2C9b_56FrEfeCBe4pQ/2/public/values?alt=json",
      async: true,
      cache: false,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      transformRequest: function(obj) {
        var str = [];
        for (var p in obj)
          str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
      },
      data: {}
    })
      .success(function($data, $status, $headers, $config) {

        if ($data) {
          // console.log($data)
          var length = $data.feed.entry.length;
          $scope.totalVoters2020 = $data.feed.entry[length-1]['gsx$year2020']['$t'];
          $scope.totalVoters2020 = $scope.totalVoters2020.replace(",", "万");
          $(".electoral-details > .voters > .en").html($scope.totalVoters2020);
        } else {
        }
      })
      .error(function($data, $status, $headers, $config) {
        // if (failurecb) {
        //     failurecb($data, $status, $headers, $config, "Error retreiving data");
        // }
      });

  }

  $scope.getNumberofVoters();



  $scope.calTotalContituency = function(){
    // console.log($scope.allDatas);
    $scope.singleConstituent = 0;
    $scope.notSingleConstituent = 0;
    for(var i = 0; i < $scope.allDatas.length; i++){
      if($scope.allDatas[i]['gsx$constituentseats']['$t'] === "单选区"){
        $scope.singleConstituent ++;
      } else {
        $scope.notSingleConstituent ++;
      }
    }
    $(".electoral-details > .smc > .en").html($scope.singleConstituent);
    $(".electoral-details > .grc > .en").html($scope.notSingleConstituent);
  }

  
  $('#constituency-detail').hide();
  
  //For Desktop Version
  var description = $(".tooltiptext");
  $("g").hover(function(){
    var g = $(this).attr("class");
    
    $(this).css('cursor', 'pointer');
    $(this).children().addClass('hover');

    $(this).addClass("tooltip active");
    
    description.addClass("active");
   
    description.html(g.replace('tooltip active', ''));
  },function(){
    description.removeClass("active");
    $(this).children().removeClass('hover');
  })

  $(document).on("mousemove", function (e) {
    description.css({
      left: e.pageX - 120,
      top: e.pageY - 600
    });
  });



  function mapClick(){
    $( "g" ).click(function() {
      let id = $(this).attr('id');
      $("#sg-electoral-boundaries .contest-party > .party-group").empty();
      $("#sg-electoral-boundaries .other-members").empty();
      $("#sg-electoral-boundaries .party-leader").empty();
      $('#tab2015 > .party-result').empty();
      $('#tab2020 > .party-result').empty();
      $("#sg-electoral-boundaries .party-group .party-profile").empty();
      $("g").children().removeClass('active');
      $("g").children().removeClass('hover');
      $(this).children().addClass('active');
  
      var oppositionParty = [];
      var oppositionPartyName = [];
      var oppositionParty2020 = [];
      var oppositionPartyName2020 = [];
      var party_members_images = [];
      var party_members_names = [];
      var party_members_images_2020 =[];
      var party_members_names_2020 = [];
      var percentage_2015 = [];
      var percentage_2020 = [];
      var new_constituency = false;
      var winning_party_desktop_2015;
      var winning_party_desktop_2020;
  
      var party_election_2015_en;
      var party_election_2015_cn;
      var party_election_2020_en;
      var party_election_2020_cn;
  
      for(var i = 0; i < $scope.allDatas.length; i++){
        // console.log($scope.allDatas[i]['gsx$constituencyen']['$t'])
        if($scope.allDatas[i]['gsx$constituencyen']['$t'] == id){
          
          $('#sg-electoral-boundaries .constituency-profile > .constituency-name > .ch').html($scope.allDatas[i]['gsx$constituencycn']['$t'] + $scope.allDatas[i]['gsx$constituentseats']['$t'])
          $('#sg-electoral-boundaries .constituency-profile > .constituency-name > .en').html(id + " " + $scope.allDatas[i]['gsx$constituentseatsen']['$t'])
          $('#sg-electoral-boundaries .constituency-profile > .constituency-voter > .num').html($scope.allDatas[i]['gsx$totalvoters2020']['$t']);
          $('#tab2020 > .constituency-voter > .num').html($scope.allDatas[i]['gsx$totalvoters2020']['$t']);
          $('#tab2015 > .constituency-voter > .num').html($scope.allDatas[i]['gsx$totalvoters2015']['$t']);
  
          oppositionParty = $scope.allDatas[i]['gsx$oppositionpartyen']['$t'].split(",");
          oppositionPartyName = $scope.allDatas[i]['gsx$oppositionparty']['$t'].split("、");
          party_members_images = $scope.allDatas[i]['gsx$partymembersimages2015']['$t'].split(",");
          party_members_names = $scope.allDatas[i]['gsx$partymembers2015']['$t'].split("、");
          party_members_images_2020 = $scope.allDatas[i]['gsx$partymembersimages2020']['$t'].split(",");
          party_members_names_2020 = $scope.allDatas[i]['gsx$partymembers2020']['$t'].split("、");
  
          oppositionParty2020 = $scope.allDatas[i]['gsx$oppositionpartyen2020']['$t'].split(",");
          oppositionPartyName2020 = $scope.allDatas[i]['gsx$oppositionparty2020']['$t'].split("、");
  
          percentage_2015 = $scope.allDatas[i]['gsx$percentage2015']['$t'].split(",");
          percentage_2020 = $scope.allDatas[i]['gsx$percentage2020']['$t'].split(",");
  
          if($scope.allDatas[i]['gsx$newconstituency']['$t'] == "Yes"){
            new_constituency = true;
          } else {
            new_constituency = false;
          }
  
          winning_party_desktop_2015 = $scope.allDatas[i]['gsx$winningparty2015']['$t'];
          winning_party_desktop_2020 = $scope.allDatas[i]['gsx$winningparty2020']['$t'];
  
          var party_2015_en = $scope.allDatas[i]['gsx$party2015electionpartyen']['$t'].split(",");
          var party_2015_cn = $scope.allDatas[i]['gsx$party2015electionpartycn']['$t'].split("、");
  
          party_election_2015_en = party_2015_en;
          party_election_2015_cn = party_2015_cn;
  
          var party_2020_en = $scope.allDatas[i]['gsx$party2020electionpartyen']['$t'].split(",");
          var party_2020_cn = $scope.allDatas[i]['gsx$party2020electionpartycn']['$t'].split("、");
  
          party_election_2020_en = party_2020_en;
          party_election_2020_cn = party_2020_cn;
        
        }
     
      }
  
     
      $("#sg-electoral-boundaries .party-group .party-profile").append(`<div class="party-logo party-${winning_party_desktop_2015} "></div>`);
  
      if(winning_party_desktop_2015 == "pap"){
       
        $("#sg-electoral-boundaries .party-group .party-profile").append('<div class="party-name">人民行动党</div>')
      } else if (winning_party_desktop_2015 == "wp"){
        $("#sg-electoral-boundaries .party-group .party-profile").append('<div class="party-name">工人党</div>')
      }
    
      
      oppositionParty.forEach(element => {
       
          var className = "party-" + element;
         
          var html =
           `<div class="party-profile"><div class="party-logo ${className}"></div><div class="party-name"></div></div>`
         
          $("#sg-electoral-boundaries .contest-party > .party-group").append(html);
       
       
      });
  
     
      $("#sg-electoral-boundaries #constituency-detail .party-profile").each(function(index){
      
        $(this).find('.party-name').html(oppositionPartyName[index-1])
       
      })
     
   
    var party_leader_html = `<div class="party-grc-leader ${party_members_names[0]}"></div><div class="member-img"><img src="${party_members_images[0]}" /></div><div class="member-name">${party_members_names[0]}</div>`
  
    var party_leader_2020_html = `<div class="party-grc-leader"></div><div class="member-img"><img src="${party_members_images_2020[0]}" /></div><div class="member-name">${party_members_names_2020[0]}</div>`
  
     
     if(party_members_images[0] !== ""){
      $("#sg-electoral-boundaries .party-leader").append(party_leader_html);
      // $(".party-details").show();
     } else {
      
       $("#sg-electoral-boundaries .party-leader").append(party_leader_2020_html);
      //  $(".party-details").hide();
     }
  
     if($("#sg-electoral-boundaries .constituency-name > .en").html().includes("SMC")){
        $("#sg-electoral-boundaries .party-grc-leader").css('display', 'none')
     }
      
    for(var i = 1; i < party_members_images.length; i++){
      var html = `<div class="party-member"><div class="member-img"><img src="${party_members_images[i]}" /></div><div class="member-name">${party_members_names[i]}</div></div>`
      $("#sg-electoral-boundaries .other-members").append(html);
    }
    for(var i = 1; i < party_members_images_2020.length; i++){
      var html = `<div class="party-member"><div class="member-img"><img src="${party_members_images_2020[i]}" /></div><div class="member-name">${party_members_names_2020[i]}</div></div>`
      $("#sg-electoral-boundaries .other-members").append(html);
    }
  
      // console.log(percentage_2015)
   
    for(var i = 0; i < percentage_2015.length; i++){
      var result_2015_desktop = `<div class="party-group ${party_election_2015_en[i]}"><div class="party-logo party-${party_election_2015_en[i]}" title="${party_election_2015_cn[i]}"></div><div class="party-result-bar"><div class="party-bar" style="width:${percentage_2015[i]}%"></div><div class="party-percent">${percentage_2015[i]}<em>%</em></div></div></div>`
  
      $('#tab2015 > .party-result').append(result_2015_desktop);
    }
  
    $('.tab-content #tab2015 .party-group').each(function(){
      if($(this).attr("class").includes(winning_party_desktop_2015)){
        
        $(this).find('.party-bar').addClass('win');
      } else {
       
        $(this).find('.party-bar').addClass('lost');
      }
    })
  
      // console.log(percentage_2020)
    
      for(var i = 0; i < percentage_2020.length; i++){
        var result_2020_desktop = `<div class="party-group ${party_election_2020_en[i]}"><div class="party-logo party-${party_election_2020_en[i]}" title="${party_election_2020_cn[i]}"></div><div class="party-result-bar"><div class="party-bar" style="width:${percentage_2020[i]}%"></div><div class="party-percent">${percentage_2020[i]}<em>%</em></div></div></div>`
    
        $('#tab2020 > .party-result').append(result_2020_desktop);
      }
  
      $('.tab-content #tab2020 .party-group').each(function(){
        if($(this).attr("class").includes(winning_party_desktop_2020)){
          
          $(this).find('.party-bar').addClass('win');
        } else {
         
          $(this).find('.party-bar').addClass('lost');
        }
      })
  
      if(new_constituency == true){
        $(".new-constituency").show();
        $("#tab1-tab").click();
        $("#tab2-tab").hide();
        $("#sg-electoral-boundaries span.yellow-bar").css('width', '100%')
      } else {
        $(".new-constituency").hide();
        $("#tab2-tab").show();
        $("#sg-electoral-boundaries span.yellow-bar").css('width', '50%')
      }
      $('#constituency-detail').show();
     
  
    });
  }
  

  $('.icon-close').click(function(){
    $('#constituency-detail').hide();
  })



  
  //For Mobile Version
  var smc = [];
  var smc_html;

  var four_grc = [];
  var four_grc_html;

  var five_grc = [];
  var five_grc_html;
  $scope.loadDataForMobile = function(){
    
   
    smc_html = '<div class="accordion-item"> <input type="radio" class="smc-radio" id="" name="smc"> <label class="accordion-label" for=""> <div class="constituency-name"><span class="ch"></span><span class="en"></span></div></label> <div class="accordion-content"> <div class="constituency-profile"> <div class="constituency-name"><span class="ch"></span><span class="en"></span></div></div><div class="party-slice card-container mdl-grid"> <div class="party-slice--item mdl-cell mdl-cell--6-col mdl-cell--6-col-tablet mdl-cell--6-col-phone mdl-card mobile-winning-party"> <div class="party-details"> <div class="header-title">现任议员</div><div class="party-group"> <div class="party-profile"> <div class="party-logo"></div><div class="party-name"></div></div></div></div></div><div class="party-slice--item mdl-cell mdl-cell--6-col mdl-cell--6-col-tablet mdl-cell--6-col-phone mdl-card mobile-opp-party"> <div class="contest-party"> <div class="header-title">有意竞选反对党</div><div class="party-group"> </div></div></div></div><div class="result-panel"> <div class="header-title">大选成绩</div><div class="container"> <div class="result-tabs"> <a id="" class="active smc-2020-tab tab-2020">2020年</a> <a id="" class="smc-2015-tab tab-2015">2015年</a> <span class="yellow-bar" style="width:50%"></span> </div><div class="tab-content"> <div id="" class="smc-2020-tab-content tab-2020-content"> <div class="constituency-voter"> <span class="ch">选民人数</span> <span class="num"></span> </div><div class="party-result"> </div></div><div id="" class="smc-2015-tab-content tab-2015-content"> <div class="constituency-voter"> <span class="ch">选民人数</span> <span class="num"></span> </div><div class="party-result"> </div></div></div></div></div></div></div>'

    four_grc_html = '<div class="accordion-item"> <input type="radio" class="four-grc-radio" id="" name="four-grc"> <label class="accordion-label" for=""> <div class="constituency-name"><span class="ch"></span><span class="en"></span></div></label> <div class="accordion-content"> <div class="constituency-profile"> <div class="constituency-name"><span class="ch"></span><span class="en"></span></div></div><div class="party-slice card-container mdl-grid"> <div class="party-slice--item mdl-cell mdl-cell--6-col mdl-cell--6-col-tablet mdl-cell--6-col-phone mdl-card mobile-winning-party"> <div class="party-details"> <div class="header-title">现任议员</div><div class="party-group"> <div class="party-profile"> <div class="party-logo"></div><div class="party-name"></div></div></div></div></div><div class="party-slice--item mdl-cell mdl-cell--6-col mdl-cell--6-col-tablet mdl-cell--6-col-phone mdl-card mobile-opp-party"> <div class="contest-party"> <div class="header-title">有意竞选反对党</div><div class="party-group"> </div></div></div></div><div class="result-panel"> <div class="header-title">大选成绩</div><div class="container"> <div class="result-tabs"> <a id="" class="active four-grc-2020-tab tab-2020">2020年</a> <a id="" class="four-grc-2015-tab tab-2015">2015年</a> <span class="yellow-bar" style="width:50%"></span> </div><div class="tab-content"> <div id="" class="four-grc-2020-tab-content tab-2020-content"> <div class="constituency-voter"> <span class="ch">选民人数</span> <span class="num"></span> </div><div class="party-result"> </div></div><div id="" class="four-grc-2015-tab-content"> <div class="constituency-voter"> <span class="ch">选民人数</span> <span class="num"></span> </div><div class="party-result"> </div></div></div></div></div></div></div>'

    five_grc_html = '<div class="accordion-item"> <input type="radio" class="five-grc-radio" id="" name="five-grc"> <label class="accordion-label" for=""> <div class="constituency-name"><span class="ch"></span><span class="en"></span></div></label> <div class="accordion-content"> <div class="constituency-profile"> <div class="constituency-name"><span class="ch"></span><span class="en"></span></div></div><div class="party-slice card-container mdl-grid"> <div class="party-slice--item mdl-cell mdl-cell--6-col mdl-cell--6-col-tablet mdl-cell--6-col-phone mdl-card mobile-winning-party"> <div class="party-details"> <div class="header-title">现任议员</div><div class="party-group"> <div class="party-profile"> <div class="party-logo"></div><div class="party-name"></div></div></div></div></div><div class="party-slice--item mdl-cell mdl-cell--6-col mdl-cell--6-col-tablet mdl-cell--6-col-phone mdl-card mobile-opp-party"> <div class="contest-party"> <div class="header-title">有意竞选反对党</div><div class="party-group"> </div></div></div></div><div class="result-panel"> <div class="header-title">大选成绩</div><div class="container"> <div class="result-tabs"> <a id="" class="active five-grc-2020-tab tab-2020">2020年</a> <a id="" class="five-grc-2015-tab tab-2015">2015年</a> <span class="yellow-bar" style="width:50%"></span> </div><div class="tab-content"> <div id="" class="five-grc-2020-tab-content tab-2020-content"> <div class="constituency-voter"> <span class="ch">选民人数</span> <span class="num"></span> </div><div class="party-result"> </div></div><div id="" class="five-grc-2015-tab-content tab-2015-content"> <div class="constituency-voter"> <span class="ch">选民人数</span> <span class="num"></span> </div><div class="party-result"> </div></div></div></div></div></div></div>'
    
    
    
    for(var i =0; i < $scope.allDatas.length; i++){
   
      let constituency_en = $scope.allDatas[i]['gsx$constituentseatsen']['$t'];
      let isnew = false;
      let percent_2015,percent_2020,party_members_2015,party_members_2015_imgs, oppositionParty, oppositionParty_en,  party_2015_election_en, party_2015_election_cn,party_2020_election_en, party_2020_election_cn;
     
      if(constituency_en.includes('SMC')){
       
        $("#tab-smc > .accordion").append(smc_html);

        if($scope.allDatas[i]['gsx$newconstituency']['$t']){
          isnew = true;
        } else {
          isnew = false;
        }

        percent_2015 = $scope.allDatas[i]['gsx$percentage2015']['$t'];
        party_members_2015 = $scope.allDatas[i]['gsx$partymembers2015']['$t'];
        party_members_2015_imgs = $scope.allDatas[i]['gsx$partymembersimages2015']['$t'];
        oppositionParty = $scope.allDatas[i]['gsx$oppositionparty']['$t'];
        oppositionParty_en = $scope.allDatas[i]['gsx$oppositionpartyen']['$t'];
        party_2015_election_en = $scope.allDatas[i]['gsx$party2015electionpartyen']['$t'];
        party_2015_election_cn = $scope.allDatas[i]['gsx$party2015electionpartycn']['$t'];
        percent_2020 = $scope.allDatas[i]['gsx$percentage2020']['$t'];
        party_2020_election_en = $scope.allDatas[i]['gsx$party2020electionpartyen']['$t'];
        party_2020_election_cn = $scope.allDatas[i]['gsx$party2020electionpartycn']['$t']

        smc.push({"cn": $scope.allDatas[i]['gsx$constituencycn']['$t'], "en":$scope.allDatas[i]['gsx$constituencyen']['$t'],"seats_en": $scope.allDatas[i]['gsx$constituentseatsen']['$t'], "seats_cn": $scope.allDatas[i]['gsx$constituentseats']['$t'],"new": isnew, "2015percent":percent_2015.split(","), "2015membersname_cn": party_members_2015.split("、"),'winning_party_2015': $scope.allDatas[i]['gsx$winningparty2015']['$t'], 'winning_party_2015_imgs':party_members_2015_imgs.split(","), oppositionParty_cn: oppositionParty.split('、'), oppositionParty_en: oppositionParty_en.split(","), total2015voters: $scope.allDatas[i]['gsx$totalvoters2015']['$t'], total2020voters: $scope.allDatas[i]['gsx$totalvoters2020']['$t'], "party_2015_election_party_en": party_2015_election_en.split(","),"party_2015_election_party_cn": party_2015_election_cn.split("、"),"2020percent": percent_2020.split(","), "party_2020_election_party_en": party_2020_election_en.split(","), "party_2020_election_party_cn": party_2020_election_cn.split("、"), 'winning_party_2020': $scope.allDatas[i]['gsx$winningparty2020']['$t']});
        
        
      } else if(constituency_en.includes('4-Member')){

        $("#tab-4grc > .accordion").append(four_grc_html);

        if($scope.allDatas[i]['gsx$newconstituency']['$t']){
          isnew = true;
        } else {
          isnew = false;
        }

        percent_2015 = $scope.allDatas[i]['gsx$percentage2015']['$t'];
        party_members_2015 = $scope.allDatas[i]['gsx$partymembers2015']['$t'];
        party_members_2015_imgs = $scope.allDatas[i]['gsx$partymembersimages2015']['$t'];
        oppositionParty = $scope.allDatas[i]['gsx$oppositionparty']['$t'];
        oppositionParty_en = $scope.allDatas[i]['gsx$oppositionpartyen']['$t'];
        party_2015_election_en = $scope.allDatas[i]['gsx$party2015electionpartyen']['$t'];
        party_2015_election_cn = $scope.allDatas[i]['gsx$party2015electionpartycn']['$t'];
        percent_2020 = $scope.allDatas[i]['gsx$percentage2020']['$t'];
        party_2020_election_en = $scope.allDatas[i]['gsx$party2020electionpartyen']['$t'];
        party_2020_election_cn = $scope.allDatas[i]['gsx$party2020electionpartycn']['$t']

        four_grc.push({"cn": $scope.allDatas[i]['gsx$constituencycn']['$t'], "en":$scope.allDatas[i]['gsx$constituencyen']['$t'],"seats_en": $scope.allDatas[i]['gsx$constituentseatsen']['$t'], "seats_cn": $scope.allDatas[i]['gsx$constituentseats']['$t'],"new": isnew, "2015percent":percent_2015.split(","), "2015membersname_cn": party_members_2015.split("、"),'winning_party_2015': $scope.allDatas[i]['gsx$winningparty2015']['$t'], 'winning_party_2015_imgs':party_members_2015_imgs.split(","), oppositionParty_cn: oppositionParty.split('、'), oppositionParty_en: oppositionParty_en.split(","), total2015voters: $scope.allDatas[i]['gsx$totalvoters2015']['$t'], total2020voters: $scope.allDatas[i]['gsx$totalvoters2020']['$t'], "party_2015_election_party_en": party_2015_election_en.split(","),"party_2015_election_party_cn": party_2015_election_cn.split("、"),"2020percent": percent_2020.split(","), "party_2020_election_party_en": party_2020_election_en.split(","), "party_2020_election_party_cn": party_2020_election_cn.split("、"), 'winning_party_2020': $scope.allDatas[i]['gsx$winningparty2020']['$t']});
        
      } else if(constituency_en.includes('5-Member')){

        $("#tab-5grc > .accordion").append(five_grc_html);

        if($scope.allDatas[i]['gsx$newconstituency']['$t']){
          isnew = true;
        } else {
          isnew = false;
        }

        percent_2015 = $scope.allDatas[i]['gsx$percentage2015']['$t'];
        party_members_2015 = $scope.allDatas[i]['gsx$partymembers2015']['$t'];
        party_members_2015_imgs = $scope.allDatas[i]['gsx$partymembersimages2015']['$t'];
        oppositionParty = $scope.allDatas[i]['gsx$oppositionparty']['$t'];
        oppositionParty_en = $scope.allDatas[i]['gsx$oppositionpartyen']['$t'];
        party_2015_election_en = $scope.allDatas[i]['gsx$party2015electionpartyen']['$t'];
        party_2015_election_cn = $scope.allDatas[i]['gsx$party2015electionpartycn']['$t'];
        percent_2020 = $scope.allDatas[i]['gsx$percentage2020']['$t'];
        party_2020_election_en = $scope.allDatas[i]['gsx$party2020electionpartyen']['$t'];
        party_2020_election_cn = $scope.allDatas[i]['gsx$party2020electionpartycn']['$t']

        five_grc.push({"cn": $scope.allDatas[i]['gsx$constituencycn']['$t'], "en":$scope.allDatas[i]['gsx$constituencyen']['$t'],"seats_en": $scope.allDatas[i]['gsx$constituentseatsen']['$t'], "seats_cn": $scope.allDatas[i]['gsx$constituentseats']['$t'],"new": isnew, "2015percent":percent_2015.split(","), "2015membersname_cn": party_members_2015.split("、"),'winning_party_2015': $scope.allDatas[i]['gsx$winningparty2015']['$t'], 'winning_party_2015_imgs':party_members_2015_imgs.split(","), oppositionParty_cn: oppositionParty.split('、'), oppositionParty_en: oppositionParty_en.split(","), total2015voters: $scope.allDatas[i]['gsx$totalvoters2015']['$t'], total2020voters: $scope.allDatas[i]['gsx$totalvoters2020']['$t'], "party_2015_election_party_en": party_2015_election_en.split(","),"party_2015_election_party_cn": party_2015_election_cn.split("、"),"2020percent": percent_2020.split(","), "party_2020_election_party_en": party_2020_election_en.split(","), "party_2020_election_party_cn": party_2020_election_cn.split("、"), 'winning_party_2020': $scope.allDatas[i]['gsx$winningparty2020']['$t']});

      }
      
     
    }
    
    
    // SMC
    $("#tab-smc > .accordion > .accordion-item").each(function( index ) {
      $(this).find(".smc-radio").attr('id',`smc-${index+1}`)
      $(this).find(".accordion-label").attr('for',`smc-${index+1}`)
      $(this).find('.accordion-label .ch').html(smc[index]['cn']);
      $(this).find(".accordion-label .en").html(smc[index]['en']);
      $(this).find(".accordion-content > .constituency-profile > .constituency-name > .ch").html(smc[index]['cn']+smc[index]['seats_cn']);
      $(this).find(".accordion-content > .constituency-profile > .constituency-name > .en").html(smc[index]['en']+ " " + smc[index]['seats_en']);
      $(this).find(".accordion-label .new-constituency").html('新')
     
      if(smc[index]['new'] == true){
        $(this).find(".accordion-label .constituency-name").prepend('<span class="new-constituency">新</span>');
        $(this).find(".smc-2015-tab").hide();
        $(this).find("span.yellow-bar").css("width", "100%")
      } else {
        $(this).find(".smc-2015-tab").show();
        $(this).find("span.yellow-bar").css("width", "50%")
      }
      
      if(smc[index]['winning_party_2015'] === 'pap'){
        $(this).find(".accordion-content .party-logo").addClass('party-pap')
        $(this).find(".accordion-content .party-name").html('人民行动党')
      } else if(smc[index]['winning_party_2015'] == 'wp'){
        $(this).find(".accordion-content .party-logo").addClass('party-wp')
        $(this).find(".accordion-content .party-name").html('工人党')
      }

      $(this).find(".accordion-content .mobile-winning-party .party-group").append(`<div class="party-box"><div class="party-leader"><div class="member-img"><img src="${smc[index]['winning_party_2015_imgs'][0]}"></div><div class="member-name">${smc[index]['2015membersname_cn'][0]}</div></div></div>`);

      for(var i = 1; i < smc[index]['winning_party_2015_imgs'].length; i++ ){
        $(this).find(".accordion-content .mobile-winning-party .party-group").append(`<div class="party-box"><div class="party-member"><div class="member-img"><img src="${smc[index]['winning_party_2015_imgs'][i]}"></div><div class="member-name">${smc[index]['2015membersname_cn'][i]}</div></div></div>`);
      }
      
      for(var i = 0; i < smc[index]['oppositionParty_cn'].length; i++){
        var mobile_opp_party_html = `<div class="party-profile"><div class="party-logo party-${smc[index]['oppositionParty_en'][i]}"></div><div class="party-name">${smc[index]['oppositionParty_cn'][i]}</div></div>`;

        $(this).find('.mobile-opp-party .party-group').append(mobile_opp_party_html)
      }


      $(this).find('.smc-2020-tab').attr("id", `smc-2020-tab-${index}`)
      $(this).find('.smc-2020-tab-content').attr("id", `smc-2020-tab-content-${index}`)
      
      $(this).find('.smc-2015-tab').attr("id", `smc-2015-tab-${index}`)
      $(this).find('.smc-2015-tab-content').attr("id", `smc-2015-tab-content-${index}`)
      
      
      $(this).find("#smc-2015-tab-content-" + index).hide();
  
      $(this).find("#smc-2020-tab-content-" + index + " .constituency-voter .num").html(smc[index]['total2020voters'])
      $(this).find("#smc-2015-tab-content-" + index + " .constituency-voter .num").html(smc[index]['total2015voters'])
      
      $(this).find(`#smc-2015-tab-${index}`).click(function(){
        $(this).addClass('active');
        $(`#smc-2020-tab-${index}`).removeClass('active');
        $(this).siblings('span.yellow-bar').css('left', "50%");
        $(`#smc-2015-tab-content-${index} .party-result`).empty()
        
        for(var i = 0; i <  smc[index]['2015percent'].length;i++){
          var smc_party_en = smc[index]["party_2015_election_party_en"][i];
          
          var smc_party_group_html = `<div class="party-group ${smc[index]["party_2015_election_party_en"][i]}"><div class="party-logo party-${smc[index]["party_2015_election_party_en"][i]}" title="${smc[index]["party_2015_election_party_cn"][i]}-${smc_party_en.toUpperCase()}"></div><div class="party-result-bar"><div class="party-bar" style=width:${smc[index]['2015percent'][i]}%></div><div class="party-percent">${smc[index]['2015percent'][i]}<em>%</em></div></div></div>`;

          $(`#smc-2015-tab-content-${index} .party-result`).append(smc_party_group_html);

        }

        $(`#smc-2015-tab-content-${index} .party-result .party-group`).each(function(){
          
          if($(this).attr("class").includes(smc[index]['winning_party_2015'])){
            $(this).find('.party-bar').addClass("win");

          } else {
            $(this).find('.party-bar').addClass("lost");
          }
          
        })

        $(`#smc-2020-tab-content-${index}`).hide();
        $(`#smc-2015-tab-content-${index}`).show();
      })

      $(this).find(`#smc-2020-tab-${index}`).click(function(){
        $(this).addClass('active');
        $(`#smc-2015-tab-${index}`).removeClass('active');
        $(this).siblings('span.yellow-bar').css('left', "0%");
        $(`#smc-2020-tab-content-${index} .party-result`).empty();
        appendSMC2020(index)

        $(`#smc-2020-tab-content-${index}`).show();
        $(`#smc-2015-tab-content-${index}`).hide();
        
      })


      //2020 SMC Version
      appendSMC2020(index);

      //REMOVE WHEN 2020 RESULTS ARE OUT
      $(".tab-2015").click();
   
    })


    // 4 GRC
    $("#tab-4grc > .accordion > .accordion-item").each(function( index ) {
      $(this).find(".four-grc-radio").attr('id',`four-grc-radio${index+1}`)
      $(this).find(".accordion-label").attr('for',`four-grc-radio${index+1}`)
      $(this).find('.accordion-label .ch').html(four_grc[index]['cn']);
      $(this).find(".accordion-label .en").html(four_grc[index]['en']);
      $(this).find(".accordion-content > .constituency-profile > .constituency-name > .ch").html(four_grc[index]['cn']+four_grc[index]['seats_cn']);
      $(this).find(".accordion-content > .constituency-profile > .constituency-name > .en").html(four_grc[index]['en']+ " " + four_grc[index]['seats_en']);
      $(this).find(".accordion-label .new-constituency").html('新')
     
      if(four_grc[index]['new'] == true){
        $(this).find(".accordion-label .constituency-name").prepend('<span class="new-constituency">新</span>');
        $(this).find(".four-grc-2015-tab").hide();
        $(this).find("span.yellow-bar").css("width", "100%")
      } else {
        $(this).find(".four-grc-2015-tab").show();
        $(this).find("span.yellow-bar").css("width", "50%")
      }
      
      if(four_grc[index]['winning_party_2015'] === 'pap'){
        $(this).find(".accordion-content .party-logo").addClass('party-pap')
        $(this).find(".accordion-content .party-name").html('人民行动党')
      } else if(four_grc[index]['winning_party_2015'] == 'wp'){
        $(this).find(".accordion-content .party-logo").addClass('party-wp')
        $(this).find(".accordion-content .party-name").html('工人党')
      }

      $(this).find(".accordion-content .mobile-winning-party .party-group").append(`<div class="party-box"><div class="party-leader"><div class="party-grc-leader ${four_grc[index]['2015membersname_cn'][0]}"></div><div class="member-img"><img src="${four_grc[index]['winning_party_2015_imgs'][0]}"></div><div class="member-name">${four_grc[index]['2015membersname_cn'][0]}</div></div></div>`);

      $(this).find(".accordion-content .mobile-winning-party .party-group").append(`<div class="party-box other-members"></div>`);

      for(var i = 1; i < four_grc[index]['winning_party_2015_imgs'].length; i++ ){
        $(this).find(".accordion-content .mobile-winning-party .other-members").append(`<div class="party-member"><div class="member-img"><img src="${four_grc[index]['winning_party_2015_imgs'][i]}"></div><div class="member-name">${four_grc[index]['2015membersname_cn'][i]}</div></div>`);
      }
      
      for(var i = 0; i < four_grc[index]['oppositionParty_cn'].length; i++){
        var mobile_opp_party_html = `<div class="party-profile"><div class="party-logo party-${four_grc[index]['oppositionParty_en'][i]}"></div><div class="party-name">${four_grc[index]['oppositionParty_cn'][i]}</div></div>`;

        $(this).find('.mobile-opp-party .party-group').append(mobile_opp_party_html)
      }


      $(this).find('.four-grc-2020-tab').attr("id", `four-grc-2020-tab-${index}`)
      $(this).find('.four-grc-2020-tab-content').attr("id", `four-grc-2020-tab-content-${index}`)
      

      $(this).find('.four-grc-2015-tab').attr("id", "four-grc-2015-tab-" + index)
      $(this).find('.four-grc-2015-tab-content').attr("id", "four-grc-2015-tab-content-" + index)
      
      $(this).find("#four-grc-2015-tab-content-" + index).hide();
  
      $(this).find(`#four-grc-2020-tab-content-${index} .constituency-voter .num`).html(four_grc[index]['total2020voters'])
      $(this).find(`#four-grc-2015-tab-content-${index} .constituency-voter .num`).html(four_grc[index]['total2015voters'])
      
      $(this).find(`#four-grc-2015-tab-${index}`).click(function(){
        $(this).addClass('active');
        $(`#four-grc-2020-tab-${index}`).removeClass('active');
        $(this).siblings('span.yellow-bar').css('left', "50%");
        $(`#four-grc-2015-tab-content-${index} .party-result`).empty()
        
        for(var i = 0; i <  four_grc[index]['2015percent'].length;i++){
          var four_grc_party_en = four_grc[index]["party_2015_election_party_en"][i];
          
          var four_grc_party_group_html = '<div class="party-group '+ four_grc[index]["party_2015_election_party_en"][i] +'"><div class="party-logo party-'+ four_grc[index]["party_2015_election_party_en"][i] + '"title="'+ four_grc[index]["party_2015_election_party_cn"][i] + " - " + four_grc_party_en.toUpperCase() +'"></div><div class="party-result-bar"><div class="party-bar" style=width:'+ four_grc[index]['2015percent'][i] +'%></div><div class="party-percent">'+ four_grc[index]['2015percent'][i] +'<em>%</em></div></div></div>';

          $("#four-grc-2015-tab-content-" + index +' .party-result').append(four_grc_party_group_html);

          
     
        }
        $("#four-grc-2015-tab-content-" + index +' .party-result .party-group').each(function(){
          
          if($(this).attr("class").includes(four_grc[index]['winning_party_2015'])){
            $(this).find('.party-bar').addClass("win");

          } else {
            $(this).find('.party-bar').addClass("lost");
          }
          
        })

        $("#four-grc-2020-tab-content-" + index).hide();
        $("#four-grc-2015-tab-content-" + index).show();
      })

      $(this).find("#four-grc-2020-tab-" + index).click(function(){
        $(this).addClass('active');
        $("#four-grc-2015-tab-" + index).removeClass('active');
        $(this).siblings('span.yellow-bar').css('left', "0%");
        $("#four-grc-2020-tab-content-" + index +' .party-result').empty();
        appendfourgrc2020(index)

        $("#four-grc-2020-tab-content-" + index).show();
        $("#four-grc-2015-tab-content-" + index).hide();
        
      })


      //2020 Four GRC Version
      appendfourgrc2020(index)

      //REMOVE WHEN 2020 RESULTS ARE OUT
      $(".tab-2015").click();
      
    })


    // 5 GRC
    $("#tab-5grc > .accordion > .accordion-item").each(function( index ) {
      $(this).find(".five-grc-radio").attr('id',"five-grc-radio"+(index+1))
      $(this).find(".accordion-label").attr('for',"five-grc-radio"+(index+1))
      $(this).find('.accordion-label .ch').html(five_grc[index]['cn']);
      $(this).find(".accordion-label .en").html(five_grc[index]['en']);
      $(this).find(".accordion-content > .constituency-profile > .constituency-name > .ch").html(five_grc[index]['cn']+five_grc[index]['seats_cn']);
      $(this).find(".accordion-content > .constituency-profile > .constituency-name > .en").html(five_grc[index]['en']+ " " + five_grc[index]['seats_en']);
      $(this).find(".accordion-label .new-constituency").html('新')
     
      if(five_grc[index]['new'] == true){
        $(this).find(".accordion-label .constituency-name").prepend('<span class="new-constituency">新</span>');
        $(this).find(".five-grc-2015-tab").hide();
        $(this).find("span.yellow-bar").css("width", "100%")
      } else {
        $(this).find(".five-grc-2015-tab").show();
        $(this).find("span.yellow-bar").css("width", "50%")
      }
      
      if(five_grc[index]['winning_party_2015'] === 'pap'){
        $(this).find(".accordion-content .party-logo").addClass('party-pap')
        $(this).find(".accordion-content .party-name").html('人民行动党')
      } else if(five_grc[index]['winning_party_2015'] == 'wp'){
        $(this).find(".accordion-content .party-logo").addClass('party-wp')
        $(this).find(".accordion-content .party-name").html('工人党')
      }

      $(this).find(".accordion-content .mobile-winning-party .party-group").append('<div class="party-box"><div class="party-leader"><div class="party-grc-leader"></div><div class="member-img"><img src="'+ five_grc[index]['winning_party_2015_imgs'][0] +'"></div><div class="member-name">'+ five_grc[index]['2015membersname_cn'][0] +'</div></div></div>');

      $(this).find(".accordion-content .mobile-winning-party .party-group").append(`<div class="party-box other-members"></div>`);

      for(var i = 1; i < five_grc[index]['winning_party_2015_imgs'].length; i++ ){
        $(this).find(".accordion-content .mobile-winning-party .other-members").append(`<div class="party-member"><div class="member-img"><img src="${five_grc[index]['winning_party_2015_imgs'][i]}"></div><div class="member-name">${five_grc[index]['2015membersname_cn'][i]}</div></div>`);
      }

      for(var i = 0; i < five_grc[index]['oppositionParty_cn'].length; i++){
        var mobile_opp_party_html = '<div class="party-profile"><div class="party-logo party-'+ five_grc[index]['oppositionParty_en'][i] +'"></div><div class="party-name">'+ five_grc[index]['oppositionParty_cn'][i] +'</div></div>';

        $(this).find('.mobile-opp-party .party-group').append(mobile_opp_party_html)
      }


      $(this).find('.five-grc-2020-tab').attr("id", "five-grc-2020-tab-" + index)
      $(this).find('.five-grc-2020-tab-content').attr("id", "five-grc-2020-tab-content-" + index)
      // $(this).find("#smc-2020-tab-" + index).attr("href", "#smc-2020-tab-content-" + index)
      

      $(this).find('.five-grc-2015-tab').attr("id", "five-grc-2015-tab-" + index)
      $(this).find('.five-grc-2015-tab-content').attr("id", "five-grc-2015-tab-content-" + index)
      // $(this).find("#smc-2015-tab-" + index).attr("href", "#smc-2015-tab-content-" + index)
      
      $(this).find("#five-grc-2015-tab-content-" + index).hide();
  
      $(this).find("#five-grc-2020-tab-content-" + index + " .constituency-voter .num").html(five_grc[index]['total2020voters'])
      $(this).find("#five-grc-2015-tab-content-" + index + " .constituency-voter .num").html(five_grc[index]['total2015voters'])
      
      $(this).find("#five-grc-2015-tab-" + index).click(function(){
        $(this).addClass('active');
        $("#five-grc-2020-tab-" + index).removeClass('active');
        $(this).siblings('span.yellow-bar').css('left', "50%");
        $("#five-grc-2015-tab-content-" + index +' .party-result').empty()
        
        for(var i = 0; i <  five_grc[index]['2015percent'].length;i++){
          var five_grc_party_en = five_grc[index]["party_2015_election_party_en"][i];
          
          var five_grc_party_group_html = '<div class="party-group '+ five_grc[index]["party_2015_election_party_en"][i] +'"><div class="party-logo party-'+ five_grc[index]["party_2015_election_party_en"][i] + '"title="'+ five_grc[index]["party_2015_election_party_cn"][i] + " - " + five_grc_party_en.toUpperCase() +'"></div><div class="party-result-bar"><div class="party-bar" style=width:'+ five_grc[index]['2015percent'][i] +'%></div><div class="party-percent">'+ five_grc[index]['2015percent'][i] +'<em>%</em></div></div></div>';

          $("#five-grc-2015-tab-content-" + index +' .party-result').append(five_grc_party_group_html);

          
     
        }
        $("#five-grc-2015-tab-content-" + index +' .party-result .party-group').each(function(){
          
          if($(this).attr("class").includes(five_grc[index]['winning_party_2015'])){
            $(this).find('.party-bar').addClass("win");

          } else {
            $(this).find('.party-bar').addClass("lost");
          }
          
        })

        $("#five-grc-2020-tab-content-" + index).hide();
        $("#five-grc-2015-tab-content-" + index).show();
      })

      $(this).find("#five-grc-2020-tab-" + index).click(function(){
        $(this).addClass('active');
        $("#five-grc-2015-tab-" + index).removeClass('active');
        $(this).siblings('span.yellow-bar').css('left', "0%");
        $("#five-grc-2020-tab-content-" + index +' .party-result').empty();
        appendfivegrc2020(index)

        $("#five-grc-2020-tab-content-" + index).show();
        $("#five-grc-2015-tab-content-" + index).hide();
        
      })


      //2020 Five GRC Version
      appendfivegrc2020(index)
      
      //REMOVE WHEN 2020 RESULTS ARE OUT
      $(".tab-2015").click();
   
    })

   
  }
    
 //2020 SMC Version
   function appendSMC2020(index){
    for(var i = 0; i <  smc[index]['2020percent'].length;i++){
      var smc_party_en = smc[index]["party_2020_election_party_en"][i];
     
      var smc_party_group_html = '<div class="party-group '+ smc[index]["party_2020_election_party_en"][i] +'"><div class="party-logo party-'+ smc[index]["party_2020_election_party_en"][i] + '"title="'+ smc[index]["party_2020_election_party_cn"][i] + " - " + smc_party_en.toUpperCase() +'"></div><div class="party-result-bar"><div class="party-bar" style=width:'+ smc[index]['2020percent'][i] +'%></div><div class="party-percent">'+ smc[index]['2020percent'][i] +'<em>%</em></div></div></div>';

      $("#smc-2020-tab-content-" + index +' .party-result').append(smc_party_group_html);

      
 
    }
    $("#smc-2020-tab-content-" + index +' .party-result .party-group').each(function(){
      
      if($(this).attr("class").includes(smc[index]['winning_party_2020'])){
        $(this).find('.party-bar').addClass("win");

      } else {
        $(this).find('.party-bar').addClass("lost");
      }
      
    })
   }

   //2020 Four GRC Version
   function appendfourgrc2020(index){
    for(var i = 0; i <  four_grc[index]['2020percent'].length;i++){
      var four_grc_party_en = four_grc[index]["party_2020_election_party_en"][i];
     
      var four_grc_party_group_html = '<div class="party-group '+ four_grc[index]["party_2020_election_party_en"][i] +'"><div class="party-logo party-'+ four_grc[index]["party_2020_election_party_en"][i] + '"title="'+ four_grc[index]["party_2020_election_party_cn"][i] + " - " + four_grc_party_en.toUpperCase() +'"></div><div class="party-result-bar"><div class="party-bar" style=width:'+ four_grc[index]['2020percent'][i] +'%></div><div class="party-percent">'+ four_grc[index]['2020percent'][i] +'<em>%</em></div></div></div>';

      $("#four-grc-2020-tab-content-" + index +' .party-result').append(four_grc_party_group_html);

      
 
    }
    $("#four-grc-2020-tab-content-" + index +' .party-result .party-group').each(function(){
      
      if($(this).attr("class").includes(four_grc[index]['winning_party_2020'])){
        $(this).find('.party-bar').addClass("win");

      } else {
        $(this).find('.party-bar').addClass("lost");
      }
      
    })
   }

    //2020 Five GRC Version
    function appendfivegrc2020(index){
      for(var i = 0; i <  five_grc[index]['2020percent'].length;i++){
        var five_grc_party_en = five_grc[index]["party_2020_election_party_en"][i];
       
        var five_grc_party_group_html = '<div class="party-group '+ five_grc[index]["party_2020_election_party_en"][i] +'"><div class="party-logo party-'+ five_grc[index]["party_2020_election_party_en"][i] + '"title="'+ five_grc[index]["party_2020_election_party_cn"][i] + " - " + five_grc_party_en.toUpperCase() +'"></div><div class="party-result-bar"><div class="party-bar" style=width:'+ five_grc[index]['2020percent'][i] +'%></div><div class="party-percent">'+ five_grc[index]['2020percent'][i] +'<em>%</em></div></div></div>';
  
        $("#five-grc-2020-tab-content-" + index +' .party-result').append(five_grc_party_group_html);
  
        
   
      }
      $("#five-grc-2020-tab-content-" + index +' .party-result .party-group').each(function(){
        
        if($(this).attr("class").includes(five_grc[index]['winning_party_2020'])){
          $(this).find('.party-bar').addClass("win");
  
        } else {
          $(this).find('.party-bar').addClass("lost");
        }
        
      })
     }

  });


  App.controller("MPCtrl",function ($scope, $http) {
    $scope.filter = {
      constituency: "",
      party: "",
      term:"",
      gender:"",
      participation: "",
      sort:""
    };
    // $scope.search;
    $scope.ministers = [];
    $scope.datas = [];
    $scope.hide = false;
    $scope.searchText = "";
    $scope.showSpinner = true;
   

    $scope.MPData = function(){
      $http({
        method: "GET",
        url: "https://spreadsheets.google.com/feeds/list/19fmQKlyfHSR8gfvIAj0ssyw0a6SrQXIaDeNqY-M0vF8/1/public/values?alt=json",
        async: true,
        cache: false,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          return str.join("&");
        },
        data: {}
      })
        .success(function($data, $status, $headers, $config) {
  
          if ($data) {
          //  console.log($data.feed.entry)
           $scope.ministers = $data.feed.entry;
           $scope.ministers.forEach((element, index)=>{
            let term = element.gsx$term.$t;
            $scope.ministers[index]['gsx$term']= term.split(",");
          })

          $scope.showSpinner = false;
           var hash = location.hash;

           if(hash !== ""){
            
              var constituency_party = hash.substring(1, hash.length);
              var party ="";
              var constituency = "";
              if(constituency_party.toLowerCase().indexOf("party-") >= 0){
                party = hash.substring(7, hash.length);
                $scope.searchText = party;
                $('.party-member-info').ready(()=>{
                  $('.party-member-info').parent().css('display', 'none');
                  $('.party-member-info').each((index,element)=> {
                   
                    if($(element).attr('data-political-party').toLowerCase().indexOf(party) !== -1){
                     
                      $(element).parent().css('display', 'initial')
                    }
                   
                  })
                })
                // console.log(party);
              } else {
                constituency = hash.substring(1, hash.length);
                constituency = constituency.replace(/\-/g, ' ');
                $scope.searchText = constituency;

                $('.party-member-info').ready(()=>{
                  $('.party-member-info').parent().css('display', 'none');
                  $('.party-member-info').each((index,element)=> {
                  
                    if($(element).attr('data-constituency').toLowerCase().indexOf(constituency) !== -1){
                    
                      $(element).parent().css('display', 'initial')
                    }
                  
                  })
                })
              }
            } 

           
            
          //  console.log($scope.ministers)
           
          } 
        })
        .error(function($data, $status, $headers, $config) {
          // if (failurecb) {
          //     failurecb($data, $status, $headers, $config, "Error retreiving data");
          // }
        });
  
    }
    
    $scope.MPData();
    $scope.filterMinister = function(){

      // Check if Object value type not string then set it to empty string
      for(const prop in $scope.filter){
        if(typeof $scope.filter[prop] !== "string"){
          $scope.filter[prop] = "";
        }
      }
    
      $(".minister").css('display', "initial");
      let party_info = $('.party-member-info');

      // ONE LEVEL FILTERING START
      if($scope.filter.constituency !==""){
        party_info.each((index,element)=>{
            if($(element).find('.constituency-name .ch').html() !== $scope.filter.constituency){
              $(element).parent().css("display", "none");
              $(element).parent().removeClass('active');
            } else {
              // $(element).parent().css("display", "initial");
              $(element).parent().addClass('active');
            }
        })
      }

      if($scope.filter.party !==""){
        party_info.each((index,element)=>{
          if($(element).find('.party-name .ch').html() !== $scope.filter.party){
            $(element).parent().css("display", "none");
          } else {
            $(element).parent().css("display", "initial");
          }
        })
      }

      if($scope.filter.term !== ""){
      
         party_info.each((index,element)=>{
            // console.log($(element).find('.term-no').hasClass('2-term'))
            if($scope.filter.term === "1"){
              if(!$(element).find('.term-no').hasClass('1-term')){
                $(element).parent().css("display", "none");
                
              } else {
                $(element).parent().css("display", "initial")
              }
            } else if($scope.filter.term === "2"){
              if(!$(element).find('.term-no').hasClass('2-term')){
                $(element).parent().css("display", "none")
              } else {
                $(element).parent().css("display", "initial")
              }
            } else if($scope.filter.term === "3"){
              if(($(element).find('.term-no').hasClass('1-term')) || ($(element).find('.term-no').hasClass('2-term'))){
                $(element).parent().css("display", "none")
              } else {
                $(element).parent().css("display", "initial")
              }
            }
          })
      }

      if($scope.filter.gender !==""){
        party_info.each((index,element)=>{
          if($scope.filter.gender === "M"){
            if(!$(element).hasClass("M")){
              $(element).parent().css("display", "none")
            } else {
              $(element).parent().css("display", "initial")
            }
          } else {
            if(!$(element).hasClass("F")){
              $(element).parent().css("display", "none")
            } else {
              $(element).parent().css("display", "initial")
            }
          }
          
        })
      }

      if($scope.filter.participation !==""){
        party_info.each((index,element)=>{
         if($scope.filter.participation === 'new'){
          if(!$(element).hasClass("new-face")){
            $(element).parent().css("display", "none")
          } else {
            $(element).parent().css("display", "initial")
          }
         } else {
            if($(element).hasClass("new-face")){
              $(element).parent().css("display", "none")
            } else {
              $(element).parent().css("display", "initial")
            }
         }
        })
      }
   
      if($scope.filter.sort !== ""){

        var result;
        if($scope.filter.sort === "years"){
          result = $(".minister ").sort(function (a, b) {

            var contentA =parseInt( $(a).data('years'));
            var contentB =parseInt( $(b).data('years'));
            return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
         });
        } else if($scope.filter.sort === "age"){
          result = $(".minister ").sort(function (a, b) {
            var contentA =parseInt( $(a).data('age'));
            var contentB =parseInt( $(b).data('age'));
            return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
         });
        }
       
       $(".active-with-click").append(result)
      } else if($scope.filter.sort === "") {
        var result;
        result = $(".minister ").sort(function (a, b) {
          var contentA = ( $(a).find('.party-member-info').data('en-name'));
          var contentB = ( $(b).find('.party-member-info').data('en-name'));
          return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
       });
       $(".active-with-click").append(result)
      }
      // ONE LEVEL FILTERING END

      // TWO LEVEL FILTERING START

      // Constituency & Party
      if($scope.filter.constituency !== "" && $scope.filter.party !==""){
        party_info.each((index,element)=>{
          if(($(element).find('.constituency-name .ch').html() === $scope.filter.constituency) && ($(element).find('.party-name .ch').html() === $scope.filter.party)){
            $(element).parent().css("display", "initial");
          } else {
            $(element).parent().css("display", "none");
          }
        })
      }

      // Constituency & Term
      if($scope.filter.constituency !=="" && $scope.filter.term !==""){
        party_info.each((index,element)=>{
         
            if($scope.filter.term === "1"){
              if((($(element).find('.term-no').hasClass('1-term')) && ($(element).find('.constituency-name .ch').html() === $scope.filter.constituency))){
                $(element).parent().css("display", "initial");
              } else {
                $(element).parent().css("display", "none");
              }
            } else if($scope.filter.term === "2"){
              if((($(element).find('.term-no').hasClass('2-term')) && ($(element).find('.constituency-name .ch').html() === $scope.filter.constituency))){
                $(element).parent().css("display", "initial");
              } else {
                $(element).parent().css("display", "none");
              }
            } else if($scope.filter.term === "3"){
              if(($(element).find(".term-no").hasClass('1-term') === false) && ($(element).find(".term-no").hasClass('2-term') === false) && ($(element).find('.constituency-name .ch').html() === $scope.filter.constituency)){
                $(element).parent().css("display", "initial");
              } else {
                $(element).parent().css("display", "none");
              }
            }
           
        })
      }

      // Constituency & Gender
      if($scope.filter.constituency !=="" && $scope.filter.gender !==""){
        party_info.each((index,element)=>{
            if($scope.filter.gender === "F"){
              if(($(element).hasClass("F")) && ($(element).find('.constituency-name .ch').html() === $scope.filter.constituency)){
                $(element).parent().css("display", "initial");
              } else {
                $(element).parent().css("display", "none");
              }
            } else if($scope.filter.gender === "M"){
              if(($(element).hasClass("M")) && ($(element).find('.constituency-name .ch').html() === $scope.filter.constituency)){
                $(element).parent().css("display", "initial");
              } else {
                $(element).parent().css("display", "none");
              }
            }
        })
      }

      // Constituency & Participation
      if($scope.filter.constituency !=="" && $scope.filter.participation !==""){
        party_info.each((index,element)=>{
          if($scope.filter.participation === "new"){
            if($(element).hasClass('new-face') && (($(element).find('.constituency-name .ch').html() === $scope.filter.constituency))){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if($scope.filter.participation === "old"){
            if(($(element).hasClass('new-face') === false) && (($(element).find('.constituency-name .ch').html() === $scope.filter.constituency))){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          }
        })
      }

      //Party & Term
      if($scope.filter.party !== "" && $scope.filter.term !== ""){
        party_info.each((index,element)=>{
          if($scope.filter.term === "1"){
            if(($(element).find('.party-name .ch').html() === $scope.filter.party) && ($(element).find(".term-no").hasClass('1-term'))){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if ($scope.filter.term === "2"){
            if(($(element).find('.party-name .ch').html() === $scope.filter.party) && ($(element).find(".term-no").hasClass('2-term'))){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if ($scope.filter.term === "3"){
            if(($(element).find('.party-name .ch').html() === $scope.filter.party) && ($(element).find(".term-no").hasClass('1-term') === false) && ($(element).find(".term-no").hasClass('2-term') === false)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          }
        })
      }

      //Party & Gender
      if($scope.filter.party !== "" && $scope.filter.gender !== ""){
        party_info.each((index,element)=>{
          if($scope.filter.gender === "F"){
            if(($(element).hasClass("F")) && ($(element).find('.party-name .ch').html() === $scope.filter.party)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if ($scope.filter.gender === "M"){
            if(($(element).hasClass("M")) && ($(element).find('.party-name .ch').html() === $scope.filter.party)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } 
        })
      }

      //Party & Participation
      if($scope.filter.party !== "" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          if($scope.filter.participation === "new"){
            if(($(element).hasClass("new-face")) && ($(element).find('.party-name .ch').html() === $scope.filter.party)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if ($scope.filter.participation === "old"){
            if(($(element).hasClass("new-face") === false) && ($(element).find('.party-name .ch').html() === $scope.filter.party)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } 
        })
      }

      //Term & Gender
      if($scope.filter.term !== "" && $scope.filter.gender !== ""){
        party_info.each((index,element)=>{
          if(($scope.filter.term === "1") && ($scope.filter.gender === "M")){
            if(($(element).find(".term-no").hasClass('1-term')) && ($(element).hasClass("M"))){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if(($scope.filter.term === "2") && ($scope.filter.gender === "M")){
            if(($(element).find(".term-no").hasClass('2-term')) && ($(element).hasClass("M"))){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if(($scope.filter.term === "3") && ($scope.filter.gender === "M")){
            if(($(element).find(".term-no").hasClass('1-term') === false) && ($(element).find(".term-no").hasClass('2-term') === false) && ($(element).hasClass("M") === true)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if(($scope.filter.term === "1") && ($scope.filter.gender === "F")){
            if(($(element).find(".term-no").hasClass('1-term')) && ($(element).hasClass("F"))){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if(($scope.filter.term === "2") && ($scope.filter.gender === "F")){
            if(($(element).find(".term-no").hasClass('2-term')) && ($(element).hasClass("F"))){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if(($scope.filter.term === "3") && ($scope.filter.gender === "F")){
            if(($(element).find(".term-no").hasClass('1-term') === false) && ($(element).find(".term-no").hasClass('2-term') === false) && ($(element).hasClass("F") === true)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } 
        })
      }

      //Term & Participation
      if($scope.filter.term !== "" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          if(($scope.filter.term === "1") && ($scope.filter.participation === "new")){
            if(($(element).find(".term-no").hasClass('1-term')) && ($(element).hasClass("new-face"))){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if(($scope.filter.term === "2") && ($scope.filter.participation === "new")){
            if(($(element).find(".term-no").hasClass('2-term')) && ($(element).hasClass("new-face"))){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if(($scope.filter.term === "3") && ($scope.filter.participation === "new")){
            if(($(element).find(".term-no").hasClass('1-term') === false) && ($(element).find(".term-no").hasClass('2-term') === false) && ($(element).hasClass("new-face") === true)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if(($scope.filter.term === "1") && ($scope.filter.participation === "old")){
            if(($(element).find(".term-no").hasClass('1-term')) && ($(element).hasClass("new-face") === false)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if(($scope.filter.term === "2") && ($scope.filter.participation === "old")){
            if(($(element).find(".term-no").hasClass('2-term')) && ($(element).hasClass("new-face") === false)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } else if(($scope.filter.term === "3") && ($scope.filter.participation === "old")){
            if(($(element).find(".term-no").hasClass('1-term') === false) && ($(element).find(".term-no").hasClass('2-term') === false) && ($(element).hasClass("new-face") === false)){
              $(element).parent().css("display", "initial");
            } else {
              $(element).parent().css("display", "none");
            }
          } 
        })
      }

      //Gender & Participation
      if($scope.filter.gender !== "" && $scope.filter.participation !== ""){
          party_info.each((index,element)=>{
            if($scope.filter.gender === "M" && $scope.filter.participation === "old"){
             if(($(element).hasClass("M")) && (!$(element).hasClass('new-face'))){
               $(element).parent().css('display', 'initial')
             } else {
              $(element).parent().css('display', 'none')
             }
             
            } else if($scope.filter.gender === "M" && $scope.filter.participation === "new"){

              if(($(element).hasClass("M")) && ($(element).hasClass('new-face'))){
                $(element).parent().css('display', 'initial')
              } else {
               $(element).parent().css('display', 'none')
              }
             
           
            } else if($scope.filter.gender === "F" && $scope.filter.participation === "old"){
             
              if(($(element).hasClass("F")) && (!$(element).hasClass('new-face'))){
                $(element).parent().css('display', 'initial')
              } else {
                $(element).parent().css('display', 'none')
              }
             
            } else if($scope.filter.gender === "F" && $scope.filter.participation === "new"){
              
              if(($(element).hasClass("F")) && ($(element).hasClass('new-face'))){
                $(element).parent().css('display', 'initial')
              } else {
                $(element).parent().css('display', 'none')
              }
            }
        })
      }
      // TWO LEVEL FILTERING END

      // THREE LEVEL FILTERING START

      // Constituency & Party & Term
      if($scope.filter.constituency !== "" && $scope.filter.party !=="" && $scope.filter.term !== ""){

        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
            var filtered_constituency = $(element);
            if($(filtered_constituency).find('.party-name .ch').html() === $scope.filter.party){
              var filtered_party = $(element);
              if($scope.filter.term === "1"){
                if($(filtered_party).find('.term-no').hasClass('1-term') === true){
                  $(filtered_party).parent().css("display", "initial");
                } else {
                  $(filtered_party).parent().css("display", "none");
                }
              } else if($scope.filter.term === "2"){
                if($(filtered_party).find('.term-no').hasClass('2-term') === true){
                  $(filtered_party).parent().css("display", "initial");
                } else {
                  $(filtered_party).parent().css("display", "none");
                }
              } else if($scope.filter.term === "3"){
                if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false)){
                  $(filtered_party).parent().css("display", "initial");
                } else {
                  $(filtered_party).parent().css("display", "none");
                }
              }
            }
          }
        })
      }

      // Constituency & Party & Gender
      if($scope.filter.constituency !== "" && $scope.filter.party !=="" && $scope.filter.gender !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
            var filtered_constituency = $(element);
            if($(filtered_constituency).find('.party-name .ch').html() === $scope.filter.party){
              var filtered_party = $(element);
              if($scope.filter.gender === "F"){
                if($(filtered_party).hasClass('F') === true){
                  $(filtered_party).parent().css("display", "initial");
                } else {
                  $(filtered_party).parent().css("display", "none");
                }
              } else if($scope.filter.gender === "M"){
                if($(filtered_party).hasClass('M') === true){
                  $(filtered_party).parent().css("display", "initial");
                } else {
                  $(filtered_party).parent().css("display", "none");
                }
              } 
            }
          }
        })
      }

      // Constituency & Party & Participation
      if($scope.filter.constituency !== "" && $scope.filter.party !=="" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
            var filtered_constituency = $(element);
            if($(filtered_constituency).find('.party-name .ch').html() === $scope.filter.party){
              var filtered_party = $(element);
              if($scope.filter.participation === "new"){
                if($(filtered_party).hasClass('new-face') === true){
                  $(filtered_party).parent().css("display", "initial");
                } else {
                  $(filtered_party).parent().css("display", "none");
                }
              } else if($scope.filter.participation === "old"){
                if($(filtered_party).hasClass('new-face') === false){
                  $(filtered_party).parent().css("display", "initial");
                } else {
                  $(filtered_party).parent().css("display", "none");
                }
              } 
            }
          }
        })
      }

      // Constituency & Term & Gender
      if($scope.filter.constituency !== "" && $scope.filter.term !=="" && $scope.filter.gender !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
            var filtered_constituency = $(element);
            if($scope.filter.term === "1" && $scope.filter.gender === "M"){
              if($(filtered_constituency).find('.term-no').hasClass('1-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "M"){
              if($(filtered_constituency).find('.term-no').hasClass('2-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "M"){
              if(($(filtered_constituency).find('.term-no').hasClass('1-term') === false) && ($(filtered_constituency).find('.term-no').hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } else if($scope.filter.term === "1" && $scope.filter.gender === "F"){
              if($(filtered_constituency).find('.term-no').hasClass('1-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "F"){
              if($(filtered_constituency).find('.term-no').hasClass('2-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "F"){
              if(($(filtered_constituency).find('.term-no').hasClass('1-term') === false) && ($(filtered_constituency).find('.term-no').hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } 
          }        
        })

      }

      // Constituency & Term & Participation
      if($scope.filter.constituency !== "" && $scope.filter.term !=="" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
            var filtered_constituency = $(element);
            if($scope.filter.term === "1" && $scope.filter.participation === "new"){
              if($(filtered_constituency).find('.term-no').hasClass('1-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("new-face")){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.participation === "new"){
              if($(filtered_constituency).find('.term-no').hasClass('2-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("new-face")){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.participation === "new"){
              if(($(filtered_constituency).find('.term-no').hasClass('1-term') === false) && ($(filtered_constituency).find('.term-no').hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("new-face")){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } else if($scope.filter.term === "1" && $scope.filter.participation === "old"){
              if($(filtered_constituency).find('.term-no').hasClass('1-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("new-face") === false){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.participation === "old"){
              if($(filtered_constituency).find('.term-no').hasClass('2-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("new-face") === false){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.participation === "old"){
              if(($(filtered_constituency).find('.term-no').hasClass('1-term') === false) && ($(filtered_constituency).find('.term-no').hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("new-face") === false){
                  $(filtered_term).parent().css("display", "initial")
                } else {
                  $(filtered_term).parent().css("display", "none")
                }
              }
            } 
          }        
        })

      }

      // Constituency & Gender & Participation
      if($scope.filter.constituency !== "" && $scope.filter.gender !=="" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
            var filtered_constituency = $(element);
            if($scope.filter.gender ==="M" && $scope.filter.participation === "new"){
              if($(filtered_constituency).hasClass("M")){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face')){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } else if($scope.filter.gender ==="F" && $scope.filter.participation === "new"){
              if($(filtered_constituency).hasClass("F")){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face')){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } else if($scope.filter.gender ==="M" && $scope.filter.participation === "old"){
              if($(filtered_constituency).hasClass("M")){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === false){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } else if($scope.filter.gender ==="F" && $scope.filter.participation === "old"){
              if($(filtered_constituency).hasClass("F")){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === false){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            }
          }
        })
      }

      // Party & Term & Gender
      if($scope.filter.party !=="" && $scope.filter.term !== "" && $scope.filter.gender !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.party-name .ch').html() === $scope.filter.party){
            var filtered_party = $(element);
            if($scope.filter.term === "1" && $scope.filter.gender === "M"){
             if($(filtered_party).find('.term-no').hasClass('1-term')){
              var filtered_term = $(element);
              if($scope.filter.gender === "M"){
                if($(filtered_term).hasClass("M") === true){
                  $(filtered_term).parent().css('display', 'initial')
                } else {
                  $(filtered_term).parent().css('display', 'none')
                }
              }
             }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "M"){
              if($(filtered_party).find('.term-no').hasClass('2-term')){
               var filtered_term = $(element);
               if($scope.filter.gender === "M"){
                 if($(filtered_term).hasClass("M") === true){
                   $(filtered_term).parent().css('display', 'initial')
                 } else {
                   $(filtered_term).parent().css('display', 'none')
                 }
               }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "M"){
              if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false) ){
               var filtered_term = $(element);
               if($scope.filter.gender === "M"){
                 if($(filtered_term).hasClass("M") === true){
                   $(filtered_term).parent().css('display', 'initial')
                 } else {
                   $(filtered_term).parent().css('display', 'none')
                 }
               }
              }
            } else if($scope.filter.term === "1" && $scope.filter.gender === "F"){
              if($(filtered_party).find('.term-no').hasClass('1-term')){
               var filtered_term = $(element);
               if($scope.filter.gender === "F"){
                 if($(filtered_term).hasClass("F") === true){
                   $(filtered_term).parent().css('display', 'initial')
                 } else {
                   $(filtered_term).parent().css('display', 'none')
                 }
               }
              }
             } else if($scope.filter.term === "2" && $scope.filter.gender === "F"){
              if($(filtered_party).find('.term-no').hasClass('2-term')){
               var filtered_term = $(element);
               if($scope.filter.gender === "F"){
                 if($(filtered_term).hasClass("F") === true){
                   $(filtered_term).parent().css('display', 'initial')
                 } else {
                   $(filtered_term).parent().css('display', 'none')
                 }
               }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "F"){
              if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false) ){
               var filtered_term = $(element);
               if($scope.filter.gender === "F"){
                 if($(filtered_term).hasClass("F") === true){
                   $(filtered_term).parent().css('display', 'initial')
                 } else {
                   $(filtered_term).parent().css('display', 'none')
                 }
               }
              }
            }  
          }
        })
      }

      // Party & Term & Participation
      if($scope.filter.party !=="" && $scope.filter.term !== "" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.party-name .ch').html() === $scope.filter.party){
            var filtered_party = $(element);
            if($scope.filter.term === "1" && $scope.filter.participation === "new"){
             if($(filtered_party).find('.term-no').hasClass('1-term')){
              var filtered_term = $(element);
              if($scope.filter.participation === "new"){
                if($(filtered_term).hasClass("new-face") === true){
                  $(filtered_term).parent().css('display', 'initial')
                } else {
                  $(filtered_term).parent().css('display', 'none')
                }
              }
             }
            } else if($scope.filter.term === "2" && $scope.filter.participation === "new"){
              if($(filtered_party).find('.term-no').hasClass('2-term')){
               var filtered_term = $(element);
               if($scope.filter.participation === "new"){
                 if($(filtered_term).hasClass("new-face") === true){
                   $(filtered_term).parent().css('display', 'initial')
                 } else {
                   $(filtered_term).parent().css('display', 'none')
                 }
               }
              }
            } else if($scope.filter.term === "3" && $scope.filter.participation === "new"){
              if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false) ){
               var filtered_term = $(element);
               if($scope.filter.participation === "new"){
                 if($(filtered_term).hasClass("new-face") === true){
                   $(filtered_term).parent().css('display', 'initial')
                 } else {
                   $(filtered_term).parent().css('display', 'none')
                 }
               }
              }
            } else if($scope.filter.term === "1" && $scope.filter.participation === "old"){
              if($(filtered_party).find('.term-no').hasClass('1-term')){
               var filtered_term = $(element);
               if($scope.filter.participation === "old"){
                 if($(filtered_term).hasClass("new-face") === false){
                   $(filtered_term).parent().css('display', 'initial')
                 } else {
                   $(filtered_term).parent().css('display', 'none')
                 }
               }
              }
             } else if($scope.filter.term === "2" && $scope.filter.participation === "old"){
              if($(filtered_party).find('.term-no').hasClass('2-term')){
               var filtered_term = $(element);
               if($scope.filter.participation === "old"){
                 if($(filtered_term).hasClass("new-face") === false){
                   $(filtered_term).parent().css('display', 'initial')
                 } else {
                   $(filtered_term).parent().css('display', 'none')
                 }
               }
              }
            } else if($scope.filter.term === "3" && $scope.filter.participation === "old"){
              if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false) ){
               var filtered_term = $(element);
               if($scope.filter.participation === "old"){
                 if($(filtered_term).hasClass("new-face") === false){
                   $(filtered_term).parent().css('display', 'initial')
                 } else {
                   $(filtered_term).parent().css('display', 'none')
                 }
               }
              }
            }  
          }
        })
      }

      // Term & Gender & Participation
      if($scope.filter.term !=="" && $scope.filter.gender !== "" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($scope.filter.term === "1" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){

            if($(element).find('.term-no').hasClass("1-term")){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('M') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === false){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } 
          } else if($scope.filter.term === "2" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
            if($(element).find('.term-no').hasClass("2-term")){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('M') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === false){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } 
          } else if($scope.filter.term === "3" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
            if(($(element).find('.term-no').hasClass("1-term") === false) && ($(element).find('.term-no').hasClass("2-term") === false)){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('M') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === false){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            }  
          } else if($scope.filter.term === "1" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
            if($(element).find('.term-no').hasClass("1-term")){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('F') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === false){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } 
          } else if($scope.filter.term === "2" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
            if($(element).find('.term-no').hasClass("2-term")){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('F') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === false){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } 
          } else if($scope.filter.term === "3" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
            if(($(element).find('.term-no').hasClass("1-term") === false) && ($(element).find('.term-no').hasClass("2-term") === false)){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('F') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === false){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            }  
          } else if($scope.filter.term === "1" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
            if($(element).find('.term-no').hasClass("1-term")){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('M') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === true){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } 
          } else if($scope.filter.term === "2" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
            if($(element).find('.term-no').hasClass("2-term")){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('M') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === true){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } 
          } else if($scope.filter.term === "3" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
            if(($(element).find('.term-no').hasClass("1-term") === false) && ($(element).find('.term-no').hasClass("2-term") === false)){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('M') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === true){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            }  
          } else if($scope.filter.term === "1" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
            if($(element).find('.term-no').hasClass("1-term")){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('F') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === true){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } 
          } else if($scope.filter.term === "2" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
            if($(element).find('.term-no').hasClass("2-term")){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('F') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === true){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            } 
          } else if($scope.filter.term === "3" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
            if(($(element).find('.term-no').hasClass("1-term") === false) && ($(element).find('.term-no').hasClass("2-term") === false)){
              var filtered_term = $(element);
              if(($(filtered_term).hasClass('F') === true)){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === true){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
              }
            }  
          }
        })
      }

      // THREE LEVEL FILTERING END

      // FOUR LEVEL FILTERING START

      //Constituency & Party & Term & Gender
      if($scope.filter.constituency !== "" && $scope.filter.party !=="" && $scope.filter.term !== "" && $scope.filter.gender !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
         if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
           var filtered_constituency = $(element);
           if($(filtered_constituency).find('.party-name .ch').html() === $scope.filter.party){
             var filtered_party = $(element);
             if($scope.filter.term === "1" && $scope.filter.gender === "M"){
               if(($(filtered_party).find('.term-no').hasClass('1-term'))){
                 var filtered_term = $(element);
                 if($(filtered_term).hasClass('M')){
                   $(filtered_term).parent().css('display', 'initial');
                 } else {
                  $(filtered_term).parent().css('display', 'none');
                 }
               }
             } else if($scope.filter.term === "2" && $scope.filter.gender === "M"){
              if(($(filtered_party).find('.term-no').hasClass('2-term'))){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('M')){
                  $(filtered_term).parent().css('display', 'initial');
                } else {
                 $(filtered_term).parent().css('display', 'none');
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "M"){
              if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('M')){
                  $(filtered_term).parent().css('display', 'initial');
                } else {
                 $(filtered_term).parent().css('display', 'none');
                }
              }
            } else if($scope.filter.term === "1" && $scope.filter.gender === "F"){
              if(($(filtered_party).find('.term-no').hasClass('1-term'))){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('F')){
                  $(filtered_term).parent().css('display', 'initial');
                } else {
                 $(filtered_term).parent().css('display', 'none');
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "F"){
              if(($(filtered_party).find('.term-no').hasClass('2-term'))){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('F')){
                  $(filtered_term).parent().css('display', 'initial');
                } else {
                 $(filtered_term).parent().css('display', 'none');
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "F"){
              if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('F')){
                  $(filtered_term).parent().css('display', 'initial');
                } else {
                 $(filtered_term).parent().css('display', 'none');
                }
              }
            }
           }
         }
        })
      }

      //Constituency & Party & Term & Participation
     if($scope.filter.constituency !== "" && $scope.filter.party !== "" && $scope.filter.term !== "" && $scope.filter.participation !== ""){
      party_info.each((index,element)=>{
        $(element).parent().css('display',"none");
        if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
          var filtered_constituency = $(element);
          if($(filtered_constituency).find('.party-name .ch').html() === $scope.filter.party){
            var filtered_party = $(element);
            if($scope.filter.term === "1" && $scope.filter.participation === "new"){
              if($(filtered_party).find(".term-no").hasClass('1-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('new-face')){
                  $(filtered_term).parent().css('display', 'initial')
                } else {
                  $(filtered_term).parent().css('display', 'none')
                }
              } 
            } else if($scope.filter.term === "2" && $scope.filter.participation === "new"){
              if($(filtered_party).find(".term-no").hasClass('2-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('new-face')){
                  $(filtered_term).parent().css('display', 'initial')
                } else {
                  $(filtered_term).parent().css('display', 'none')
                }
              } 
            } else if($scope.filter.term === "3" && $scope.filter.participation === "new"){
              if(($(filtered_party).find(".term-no").hasClass('1-term')=== false) && ($(filtered_party).find(".term-no").hasClass('2-term')=== false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('new-face')){
                  $(filtered_term).parent().css('display', 'initial')
                } else {
                  $(filtered_term).parent().css('display', 'none')
                }
              } 
            } else if($scope.filter.term === "1" && $scope.filter.participation === "old"){
              if($(filtered_party).find(".term-no").hasClass('1-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('new-face') === false){
                  $(filtered_term).parent().css('display', 'initial')
                } else {
                  $(filtered_term).parent().css('display', 'none')
                }
              } 
            } else if($scope.filter.term === "2" && $scope.filter.participation === "old"){
              if($(filtered_party).find(".term-no").hasClass('2-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('new-face') === false){
                  $(filtered_term).parent().css('display', 'initial')
                } else {
                  $(filtered_term).parent().css('display', 'none')
                }
              } 
            } else if($scope.filter.term === "3" && $scope.filter.participation === "old"){
              if(($(filtered_party).find(".term-no").hasClass('1-term') === false) && ($(filtered_party).find(".term-no").hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass('new-face') === false){
                  $(filtered_term).parent().css('display', 'initial')
                } else {
                  $(filtered_term).parent().css('display', 'none')
                }
              } 
            }
          }
        }
      })
     }

      // Constituency & Party & Gender & Participation
      if($scope.filter.constituency !== "" && $scope.filter.party !=="" && $scope.filter.gender !== "" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
         if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
           var filtered_constituency = $(element);
           if($(filtered_constituency).find('.party-name .ch').html() === $scope.filter.party){
             var filtered_party = $(element);
             if($scope.filter.gender === "M" && $scope.filter.participation === "new"){
               if($(filtered_party).hasClass("M")){
                var filtered_gender = $(element);
                if($(filtered_gender).hasClass('new-face') === true){
                  $(filtered_gender).parent().css('display', 'initial')
                } else {
                  $(filtered_gender).parent().css('display', 'none')
                }
               }
             } else if($scope.filter.gender === "M" && $scope.filter.participation === "old"){
              if($(filtered_party).hasClass("M")){
               var filtered_gender = $(element);
               if($(filtered_gender).hasClass('new-face') === false){
                $(filtered_gender).parent().css('display', 'initial')
              } else {
                $(filtered_gender).parent().css('display', 'none')
              }
             }
            } else if($scope.filter.gender === "F" && $scope.filter.participation === "new"){
              if($(filtered_party).hasClass("F")){
               var filtered_gender = $(element);
               if($(filtered_gender).hasClass('new-face') === true){
                 $(filtered_gender).parent().css('display', 'initial')
               } else {
                 $(filtered_gender).parent().css('display', 'none')
               }
              }
            } else if($scope.filter.gender === "F" && $scope.filter.participation === "old"){
              if($(filtered_party).hasClass("F")){
               var filtered_gender = $(element);
               if($(filtered_gender).hasClass('new-face') === false){
                $(filtered_gender).parent().css('display', 'initial')
              } else {
                $(filtered_gender).parent().css('display', 'none')
              }
             }
            }
           }
         }
        })
      }

       // Constituency & Term & Gender & Participation
       if($scope.filter.constituency !== "" && $scope.filter.term !=="" && $scope.filter.gender !== "" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
            var filtered_constituency = $(element);
            if($scope.filter.term === "1" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
              if($(filtered_constituency).find('.term-no').hasClass('1-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face')){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
              if($(filtered_constituency).find('.term-no').hasClass('2-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face')){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
              if(($(filtered_constituency).find('.term-no').hasClass('1-term') === false) && ($(filtered_constituency).find('.term-no').hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face')){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "1" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
              if($(filtered_constituency).find('.term-no').hasClass('1-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face')){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
              if($(filtered_constituency).find('.term-no').hasClass('2-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face')){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
              if(($(filtered_constituency).find('.term-no').hasClass('1-term') === false) && ($(filtered_constituency).find('.term-no').hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face')){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "1" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
              if($(filtered_constituency).find('.term-no').hasClass('1-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face') === false){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
              if($(filtered_constituency).find('.term-no').hasClass('2-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face') === false){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
              if(($(filtered_constituency).find('.term-no').hasClass('1-term') === false) && ($(filtered_constituency).find('.term-no').hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face') === false){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "1" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
              if($(filtered_constituency).find('.term-no').hasClass('1-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face') === false){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
              if($(filtered_constituency).find('.term-no').hasClass('2-term')){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face') === false){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
              if(($(filtered_constituency).find('.term-no').hasClass('1-term') === false) && ($(filtered_constituency).find('.term-no').hasClass('2-term') === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass('new-face') === false){
                    $(filtered_gender).parent().css('display', 'initial')
                  } else {
                    $(filtered_gender).parent().css('display', 'none')
                  }
                }
              }
            }
          } 
        })
       }

       //Party & Term & Gender & Participation
       if($scope.filter.party !=="" && $scope.filter.term !== "" && $scope.filter.gender !== "" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.party-name .ch').html() === $scope.filter.party){
            var filtered_party = $(element);
            if($scope.filter.term === "1" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
              if($(filtered_party).find('.term-no').hasClass("1-term")){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face")){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
              if($(filtered_party).find('.term-no').hasClass("2-term")){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face")){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
              if(($(filtered_party).find('.term-no').hasClass("1-term") === false) && ($(filtered_party).find('.term-no').hasClass("2-term") === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face")){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "1" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
              if($(filtered_party).find('.term-no').hasClass("1-term")){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face")){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
              if($(filtered_party).find('.term-no').hasClass("2-term")){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face")){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
              if(($(filtered_party).find('.term-no').hasClass("1-term") === false) && ($(filtered_party).find('.term-no').hasClass("2-term") === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face")){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "1" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
              if($(filtered_party).find('.term-no').hasClass("1-term")){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face") === false){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
              
              if($(filtered_party).find('.term-no').hasClass("2-term")){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face") === false){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
              if(($(filtered_party).find('.term-no').hasClass("1-term") === false) && ($(filtered_party).find('.term-no').hasClass("2-term") === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("M")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face") === false){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "1" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
              if($(filtered_party).find('.term-no').hasClass("1-term")){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face") === false){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "2" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
              if($(filtered_party).find('.term-no').hasClass("2-term")){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face") === false){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            } else if($scope.filter.term === "3" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
              if(($(filtered_party).find('.term-no').hasClass("1-term") === false) && ($(filtered_party).find('.term-no').hasClass("2-term") === false)){
                var filtered_term = $(element);
                if($(filtered_term).hasClass("F")){
                  var filtered_gender = $(element);
                  if($(filtered_gender).hasClass("new-face") === false){
                    $(filtered_gender).parent().css("display","initial")
                  } else {
                    $(filtered_gender).parent().css("display","none")
                  }
                }
              }
            }
          }
        })
       }
      // FOUR LEVEL FILTERING END

      // FIVE LEVEL FILTERING START
       if($scope.filter.constituency !== "" && $scope.filter.party !== "" && $scope.filter.term !== "" && $scope.filter.gender !== "" && $scope.filter.participation !== ""){
        party_info.each((index,element)=>{
          $(element).parent().css('display',"none");
          if($(element).find('.constituency-name .ch').html() === $scope.filter.constituency){
            var filtered_constituency = $(element);
            if($(filtered_constituency).find(".party-name .ch").html() === $scope.filter.party){
              var filtered_party = $(element);
              if($scope.filter.term === "1" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
                console.log($(filtered_party));
                console.log($(filtered_party).find('.term-no').hasClass('1-term'));
                if($(filtered_party).find('.term-no').hasClass('1-term')){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("M")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face")){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              } else if($scope.filter.term === "2" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
                if($(filtered_party).find('.term-no').hasClass('2-term')){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("M")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face")){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              } else if($scope.filter.term === "3" && $scope.filter.gender === "M" && $scope.filter.participation === "new"){
                if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false)){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("M")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face")){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              } else if($scope.filter.term === "1" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
                if($(filtered_party).find('.term-no').hasClass('1-term')){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("F")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face")){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              } else if($scope.filter.term === "2" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
                if($(filtered_party).find('.term-no').hasClass('2-term')){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("F")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face")){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              } else if($scope.filter.term === "3" && $scope.filter.gender === "F" && $scope.filter.participation === "new"){
                if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false)){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("F")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face")){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              } else if($scope.filter.term === "1" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
                if($(filtered_party).find('.term-no').hasClass('1-term')){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("M")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face") === false){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                } 
              } else if($scope.filter.term === "2" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
                if($(filtered_party).find('.term-no').hasClass('2-term')){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("M")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face") === false){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              } else if($scope.filter.term === "3" && $scope.filter.gender === "M" && $scope.filter.participation === "old"){
                if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false)){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("M")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face") === false){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              } else if($scope.filter.term === "1" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
                if($(filtered_party).find('.term-no').hasClass('1-term')){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("F")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face") === false){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              } else if($scope.filter.term === "2" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
                if($(filtered_party).find('.term-no').hasClass('2-term')){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("F")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face") === false){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              } else if($scope.filter.term === "3" && $scope.filter.gender === "F" && $scope.filter.participation === "old"){
                if(($(filtered_party).find('.term-no').hasClass('1-term') === false) && ($(filtered_party).find('.term-no').hasClass('2-term') === false)){
                  var filtered_term = $(element);
                  if($(filtered_term).hasClass("F")){
                    var filtered_gender = $(element);
                    if($(filtered_gender).hasClass("new-face") === false){
                      $(filtered_gender).parent().css('display', 'initial');
                    } else {
                      $(filtered_gender).parent().css('display', 'none');
                    }
                  }
                }
              }  
            }
          }
        })
       }
      // FIVE LEVEL FILTERING END
    }

   $scope.searchMinister = function(){
    
    //  console.log($scope.searchText.length);
   
    let searchText = $scope.searchText.toLowerCase();;
 
    let party_info = $('.party-member-info');
    party_info.each((index,element)=>{
    
     
     let member_name = $(element).find('.member-name .ch').html();
     let constituency = $(element).find('.constituency-name .ch').html();
     let designation = $(element).find('.member-designation').html();
     let age = $(element).find(".member-age .en").html();
     let party_name_cn = $(element).find(".party-name .ch").html();
   
     let party_name_en = $(element).find(".party-name .en").html().toLowerCase();
     let term = $(element).find('.term-no .term');
     let percent_2015 = $(element).find('.percent-2015').html();
    //  let percent_2020 = $(element).find('.percent-2020').html();
    // console.log($(element));
    let percent_2020 = $(element).parent().attr('data-2020');

     let member_name_en = $(element).attr('data-en-name').toLowerCase();
     let party_name_shortform = $(element).attr('data-political-party').toLowerCase();
     let constituency_en = $(element).attr('data-constituency').toLowerCase();

     if((member_name.indexOf(searchText) !== -1) || (constituency.indexOf(searchText) !== -1) || (designation.indexOf(searchText) !== -1) || (age.indexOf(searchText) !== -1) || (party_name_cn.indexOf(searchText) !== -1) || (party_name_en.indexOf(searchText) !== -1) || (percent_2015.indexOf(searchText) !== -1) || (percent_2020.indexOf(searchText) !== -1) || (member_name_en.indexOf(searchText) !== -1) || (party_name_shortform.indexOf(searchText) !== -1) || (constituency_en.indexOf(searchText) !== -1)){
        $(element).parent().css('display', 'initial')
     } else {
      term.each((index, term)=>{
        let each_term = $(term).html();
        if(each_term.indexOf(searchText) !== -1){
         $(element).parent().css('display', 'initial')
        } else {
         $(element).parent().css('display', 'none')
        }
      })
     }
    })
    
    
  
     
     
   } 
 
    //script-filter.js START   
    // var items = $('#MPFilter .item');

    // $('select.filter').on('change', function() {
    //   var fltrProd = $('select#constituency');
    //   var fltrPlat = $('select#party');
    //   var prodVal = fltrProd.val();
    //   var platVal = fltrPlat.val();
  
    //   if (prodVal == "0" && platVal == "0") {
    //       items.removeClass('hidden').addClass('visible');
    //   } else {
    //       items.addClass('hidden').removeClass('visible');
  
    //       if (prodVal == "0" && platVal != "0") {
    //           $('.item[data-filter-party="' + platVal + '"]').removeClass('hidden').addClass('visible');
    //       } else if (prodVal != "0" && platVal == "0") {
    //           $('.item[data-filter-constituency="' + prodVal + '"]').removeClass('hidden').addClass('visible');
    //       } else {
    //           $('.item[data-filter-party="' + platVal + '"][data-filter-constituency="' + prodVal + '"]').removeClass('hidden').addClass('visible');
    //       }
    //   }
    // });

    // $('select#sort').on('change', function() {
    //   var sortVal = $(this).val();
    //   tinysort('.item', {
    //       order: 'desc',
    //       data: 'sort-' + sortVal
    //   });
    // });
    
    $scope.name = "";
    $scope.showDetails = function(name){
 
      var card = $(`#${name}`);
      var icon = $(card).find('i');
      $(".material-card i")
          .removeClass('mdi-close')
          .removeClass('spinner')
          .addClass('mdi-arrow-down');
   
      if($scope.name == ""){
        $scope.card = name;
        $scope.name = name;
        icon.addClass('spinner');
        window.setTimeout(function() {
         icon
          .removeClass('mdi-arrow-down')
          .removeClass('spinner')
          .addClass('mdi-close');
        }, 800);

      } else if($scope.name == name) {
        $scope.card = "";
        $scope.name = "";
        icon.addClass('spinner');
        window.setTimeout(function() {
          icon
          .removeClass('mdi-close')
          .removeClass('spinner')
          .addClass('mdi-arrow-down');
  
        }, 800);

      } else {
        $scope.card = name;
        $scope.name = name;
        icon.addClass('spinner');
        window.setTimeout(function() {
          icon
           .removeClass('mdi-arrow-down')
           .removeClass('spinner')
           .addClass('mdi-close');
         }, 800);
      }   
    }
    
    // Cannot Delete this function
    $(function() {
     
    });

    // Filter Nav

   var elActive = '';
    $.fn.selectCF = function( options ) {
      
        var settings = $.extend({
            color: "#FFF",
            backgroundColor: "#053193",
      change: function( ){ },
        }, options );
 
        return this.each(function(){
      
      var selectParent = $(this);
        var list = [],
        html = '';
        
      //parameter CSS
      var width = $(selectParent).width();
      var s, title;
      $(selectParent).hide();
      if( $(selectParent).children('option').length == 0 ){ return; }
      $(selectParent).children('option').each(function(){
        if( $(this).is(':selected') ){ s = 1; title = $(this).text(); }else{ s = 0; }
        list.push({ 
          value: $(this).attr('value'),
          text: $(this).text(),
          selected: s,
        })
      })
      
     
      // style
      var style = " background: "+settings.backgroundColor+"; color: "+settings.color+" ";
       
      html += `<ul class='selectCF'>`;
      html +=   "<li>";
      html +=     "<span class='arrowCF mdi mdi-chevron-down' style='"+style+"'></span>";
      html +=     "<span class='titleCF' style='"+style+"; width:"+width+"px'>"+title+"</span>";
      html +=     "<span class='searchCF' style='"+style+"; width:"+width+"px'><input style='color:"+settings.color+"' /></span>";
      html +=     `<ul class="filter-${$(selectParent).attr('id')}">`;
      $.each(list, function(k, v){ s = (v.selected == 1)? "selected":"";
      html +=       "<li value="+v.value+" class='"+s+"'>"+v.text+"</li>";})    
      html +=     "</ul>";
      html +=   "</li>";
      html += "</ul>";
      $(selectParent).after(html);
      var customSelect = $(this).next('ul.selectCF'); // add Html
      var seachEl = $(this).next('ul.selectCF').children('li').children('.searchCF');
      var seachElOption = $(this).next('ul.selectCF').children('li').children('ul').children('li');
      var seachElInput = $(this).next('ul.selectCF').children('li').children('.searchCF').children('input');
      
      // handle active select
      $(customSelect).unbind('click').bind('click',function(e){
        
        e.stopPropagation();
        if($(this).hasClass('onCF')){ 
          elActive = ''; 
          $(this).removeClass('onCF');
          $(this).removeClass('searchActive'); $(seachElInput).val(''); 
          $(seachElOption).show();
        }else{
          if(elActive != ''){ 
            $(elActive).removeClass('onCF'); 
            $(elActive).removeClass('searchActive'); $(seachElInput).val('');
            $(seachElOption).show();
          }
          elActive = $(this);
          $(this).addClass('onCF');
          $(seachEl).children('input').focus();
        }
      })
      
      // handle choose option
      var optionSelect = $(customSelect).children('li').children('ul').children('li');
      $(optionSelect).bind('click', function(e){
        var value = $(this).attr('value');
        if( $(this).hasClass('selected') ){
          //
        }else{
          $(optionSelect).removeClass('selected');
          $(this).addClass('selected');
          $(customSelect).children('li').children('.titleCF').html($(this).html());
          $(selectParent).val(value);
          settings.change.call(selectParent); // call event change
        }
      })
        
      // handle search 
      $(seachEl).children('input').bind('keyup', function(e){
        var value = $(this).val();
        if( value ){
          $(customSelect).addClass('searchActive');
          $(seachElOption).each(function(){
            if( $(this).text().search(new RegExp(value, "i")) < 0 ) {
              // not item
              $(this).fadeOut();
            }else{
              // have item
              $(this).fadeIn();
            }
          })
        }else{
          $(customSelect).removeClass('searchActive');
          $(seachElOption).fadeIn();
        }
      })
      
    });
    };
    $(document).click(function(){
      if(elActive != ''){
        $(elActive).removeClass('onCF');
        $(elActive).removeClass('searchActive');
      }
    })

    var event_change = $('#event-change');
    
    $( ".select" ).selectCF({
      change: function(){
        var value = $(this).val();
        var text = $(this).children('option:selected').html();
        
        if($(this).attr('id') === 'constituency'){
         $scope.filter.constituency = value;
         $scope.filterMinister(); 
        } 
        if ($(this).attr('id') === 'party'){
          $scope.filter.party = value;
          $scope.filterMinister(); 
        } 
        if ($(this).attr('id') === 'term'){
          $scope.filter.term = value;
          $scope.filterMinister();
        } else if ($(this).attr('id') === 'gender'){
          $scope.filter.gender = value;
          $scope.filterMinister();
        } else if ($(this).attr('id') === 'participation'){
          $scope.filter.participation = value;
          $scope.filterMinister();
        }  else if ($(this).attr('id') === 'sort'){
          $scope.filter.sort = value;
          $scope.filterMinister();
        }
        
        event_change.html(value+' : '+text);
        
      }
      
    });
  
   
  //script-filter.js END



  
})



}());