
// Target Tab
const tabs = document.querySelectorAll('[data-tab-target]')
const tabContents = document.querySelectorAll('[data-tab-content]')

tabs.forEach(tab => {
  tab.addEventListener('click', () => {
    const target = document.querySelector(tab.dataset.tabTarget)
    tabContents.forEach(tabContent => {
      tabContent.classList.remove('active')
    })
    tabs.forEach(tab => {
      tab.classList.remove('active')
    })
    tab.classList.add('active')
    target.classList.add('active')
  })
})


  var tabContainer = new Swiper('.tab-container-inner', {
  	slidesPerView: 1,
  	watchSlidesProgress: true,
  	watchSlidesVisibility: true
  });

$(function() {

    var $el, leftPos, newWidth,
        $mainNav = $(".links-container");

    $mainNav.append("<li id='magic-line'></li>");
    var $magicLine = $("#magic-line");

    $magicLine
        .width($(".active").outerWidth())
        .css("left", $(".active").position().left)
        .data("origLeft", $magicLine.position().left)
        .data("origWidth", $magicLine.width());
	
		// tabContainer.on('slideChangeStart', function() {
		// 	$el = $('.active');
		// 	leftPos = $el.position().left;
    //     newWidth = $el.outerWidth();
    //     $magicLine.stop().animate({
    //         left: leftPos,
    //         width: newWidth
    //     });
		// });
});

// tabContainer.on('slideChangeStart', function() {
// 	$('.nav-link').removeClass('active');
// 	var currentIndex = tabContainer.activeIndex;
// 	$('.nav-link:nth-child('+(currentIndex+1)+')').addClass('active');
// });

$('.nav-link').on('click', function() {
  var $magicLine = $("#magic-line");
  $('.nav-link').removeClass('active');
  $(this).addClass('active');
  $el = $('.active');
  leftPos = $el.position().left;
  newWidth = $el.outerWidth();
  $magicLine.stop().animate({
    left: leftPos,
    width: newWidth
  });
  tabContainer.slideTo($('.nav-link').index($(this)));
});


// Results Tab
$(document).ready(function() {
  $('.result-tabs').each(function() {
    var $active, $content, $links = $(this).find('a');
    $active = $($links[0]);
    $active.addClass('active');
    $content = $($active[0].hash);
    $links.not($active).each(function() {
        $(this.hash).hide();
    });
    $(this).on('click', 'a', function(e) {
      $active.removeClass('active');
      $content.hide();
      $active = $(this);
      $content = $(this.hash);
      $active.addClass('active');
      $content.show();
      e.preventDefault();
    });
  });
});

$(document).ready(function() {
  $('.party-profile-tabs').each(function() {
    var $active, $content, $links = $(this).find('a');
    $active = $($links[0]);
    $active.addClass('active');
    $content = $($active[0].hash);
    $links.not($active).each(function() {
        $(this.hash).hide();
    });
    $(this).on('click', 'a', function(e) {
      $active.removeClass('active');
      $content.hide();
      $active = $(this);
      $content = $(this.hash);
      $active.addClass('active');
      $content.show();
      e.preventDefault();
    });
  });
});

// Progress Bar
$(function() {
$("body").prognroll({
  height: 5,
    color: "#053193",
    custom: false
});
$(window).scroll(function() {
  if( $(window).scrollTop() > 250) {
$(".arrow-down").css("visibility","hidden");
  }else {
   $(".arrow-down").css("visibility","visible");
}
});
});


// Nav Tabs
$(".tab_content").hide();
$(".tab_content:first").show();
$("ul.nav-tabs li").click(function() {

  $(".tab_content").hide();
  var activeTab = $(this).attr("rel"); 
  $("#"+activeTab).fadeIn();    

  $("ul.nav-tabs li").removeClass("active");
  $(this).addClass("active");

$(".tab_drawer_heading").removeClass("d_active");
$(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");

});
$(".tab_drawer_heading").click(function() {
  
  $(".tab_content").hide();
  var d_activeTab = $(this).attr("rel"); 
  $("#"+d_activeTab).fadeIn();

$(".tab_drawer_heading").removeClass("d_active");
  $(this).addClass("d_active");

$("ul.nav-tabs li").removeClass("active");
$("ul.nav-tabs li[rel^='"+d_activeTab+"']").addClass("active");
});

$('ul.nav-tabs li').last().addClass("tab_last");



// Just Stick There
var div_top = $('.sticker').offset().top;
$(window).scroll(function() {
    var window_top = $(window).scrollTop() - 0;
    if (window_top > div_top) {
        if (!$('.sticker').is('.sticky')) {
            $('.sticker').addClass('sticky');
        }
    } else {
        $('.sticker').removeClass('sticky');
    }
});



