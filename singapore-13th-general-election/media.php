
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"> 
<meta charset="utf-8">

<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
dataLayer.push({
    "level2": "INTERACTIVE-NAME",
    "title": "HEADLINE TITLE",
});
</script>
<!-- Google Tag Manager
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVFMLMK');</script> -->
<!-- End Google Tag Manager -->

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="index,follow">
<meta name="GOOGLEBOT" content="index,follow">
<meta http-equiv="cache-control" content="max-age=0, no-cache, no-store, must-revalidate" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="author" content="" />

<title>选战视频/直播</title>
<meta name="description" content="SOMETHING SHORT AND BRIEF ENOUGH TO DESCRIBE THE TOPIC" />
<meta name="keywords" content="早报, KEYWORDS HIGHLY RELEVANT TO THE TOPIC, NAMES ARE IMPORTANT TOO" />
<link rel="image_src" href="//interactive.zaobao.com/YYYY/FOLDER-NAME/IMGS/SOCIAL-SHARE-IMG" />

<?php include "assets/inc/global.html" ?>
</head>

<body id="page-media">
<?php include "assets/inc/gtm.html" ?>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

<?php include "assets/inc/brand.html" ?>

  <div class="content mdl-layout__content">
    <a name="top"></a>

    <div class="video-wall">
      <p style="padding:1em">DO NOT BE ALARMED - IT WAS ONLY 5 YEARS AGO ....</p>

      <div class="BC-playlist">
        <div id="jumbotron-video-display">
            <div class="sph-video-widget" id="jumbotron-video">
                <div class="sph-video-box">
                    <video class="video-js" controls="" data-account="4802324430001" data-embed="default" data-player="Nkwi5hrAe" data-playlist-container="slick-items-playlist" data-playlist-id="4834532228001" id="playlistPlayer" playsinline="">&nbsp;</video>
                    <ol class="vjs-playlist slick-items-playlist"></ol>
                </div>
            </div>
        </div>
      </div>
    </div>

  </div>
</div>

<?php include "assets/inc/news-feed.html"?>
<?php include "assets/inc/footer.html"?>
<?php include "assets/inc/scripts.html"?>

<link href="//www.zaobao.com/sites/all/themes/zb2016/assets/css/style-brightcove.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//www.zaobao.com/sites/all/themes/zb2019/dist/js/slick.min.js?v=1.1.2"></script>
<script type="text/javascript" src="//www.zaobao.com/sites/all/themes/zb2016/assets/OwlCarousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="//www.zaobao.com/sites/all/themes/zb2016/assets/OwlCarousel/additional.js"></script>
<script type="text/javascript" src="//www.zaobao.com/sites/all/themes/zb2016/assets/js/video-slick.min.js"></script>
<script type="text/javascript" src="//players.brightcove.net/4802324430001/Nkwi5hrAe_default/index.min.js"></script>
<script type="text/javascript" src="//www.zaobao.com/sites/all/themes/zb2016/assets/js/video-playlist.js"></script>
<script type="text/javascript" src="//www.zaobao.com/sites/all/themes/zb2016/assets/js/video-playlist-default-layout.js"></script>

</body>
</html>
