const bubbleChart = (_) => {

    let node = _;



    function updateViz(data) {

        let [width, height] = [node.node().clientWidth, node.node().clientHeight];
 

        let svg = node.selectAll('svg').data([0])
        svg = svg.enter().append('svg').merge(svg)
            .attr('width', width)
            .attr('height', height)

        let filter = svg.selectAll('filter').data([0])
        filter = filter.enter().append('filter').merge(filter)
            .attr('id', 'dropshadow')
            .attr('x', -2).attr('y', -2)
            .attr('width', 200).attr('height', 200)
            .append('feDropShadow').attr('stdDeviation', 10)
            .attr('flood-color', '#ccc').attr('flood-opacity', 0.5)




    }

    // updateViz.draw = function(_) {
    //     if (typeof _ === 'undefined') return _draw;
    //     _draw = _;
    //     return this;
    // }


    return updateViz
}