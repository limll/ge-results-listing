const oneMap = (_, Electoral) => {
    let _center;


    let map = L.map(_, {
        renderer: L.canvas(),
        zoomControl: false
    })
    let MINZOOM;
    let tileUrl = '//maps-{s}.onemap.sg/v3/Grey/{z}/{x}/{y}.png'


    const region_layer = L.geoJSON(Electoral, {
        style: feature => {
            return {
                color: '#333',
                opacity: 1,
                weight: 0.7,
                fillColor: zoneColor(this.ELECTORS.find(t => t.Constituency === feature.properties.ED_DESC).Type_ch),
                fillOpacity: 0.3
            };
        }
    }).addTo(map);
    // const boundary_layer = L.polygon(turf.flip(turf.polygon(Electoral.geometry.coordinates)).geometry.coordinates, {
    //     color: '#333',
    //     opacity: 0,
    //     weight: 1,
    //     fillColor: '#ffffff',
    //     fillOpacity: 0
    // })
    const region_layer_bounds = region_layer.getBounds()

    map.fitBounds(region_layer_bounds);
    // map.panInsideBounds(boundary_layer_bounds)


    const basemap = L.tileLayer(tileUrl, {
        maxZoom: 19,
        detectRetina: true,
        attribution: '<img src="https://docs.onemap.sg/maps/images/oneMap64-01.png" style="height:20px;width:20px;"/> New OneMap | Map data &copy; contributors, <a href="http://SLA.gov.sg">Singapore Land Authority</a>'
    });
    basemap.addTo(map);
    region_layer.addTo(map);
    // boundary_layer.bringToBack();
    map.setZoom(12)


    L.control.zoom({ position: 'bottomright' }).addTo(map);

    L.svg().addTo(map);

    let centerGroup = L.featureGroup().addTo(map);


    function updateViz() {

        // const scaleWeight = d3.scaleLinear().domain([13.5, 22]).range([0.3, 1])
        // const scaleFillOpacity = d3.scaleLinear().domain([13.5, 22]).range([0.5, 1])




        if (_center) {
            setUpMarker(_center)


            const bounds = centerGroup.getBounds()
            map.flyToBounds(bounds, { paddingTopLeft: [w * 0.4, 0],maxZoom:14 });


            //get info box
            region_layer.eachLayer((layer) => {
                const [x, y] = _center

                const layerGeo = L.geoJSON(layer.toGeoJSON(), {
                    coordsToLatLng: function(coords) {
                        //                    latitude , longitude, altitude
                        return new L.LatLng(coords[1], coords[0], coords[2]); //Normal behavior
                        // return new L.LatLng(coords[0], coords[1], coords[2]);
                    }
                }).toGeoJSON().features[0].geometry.coordinates

                // console.log(turf.point([+y, +x]),turf.multiPolygon(layerGeo))
                // const isInside = isMarkerInsidePolygon(_center, layer) 
                const isInside = turf.booleanPointInPolygon(turf.point([+y, +x]), turf.multiPolygon(layerGeo));
                if (isInside) { 
                    // console.log(layer)
                    d3.select('#map-result').classed('show', true)
                    .html(d => {
                        const item = this.ELECTORS.find(t => t.Constituency === layer.feature.properties.ED_DESC)
                        console.log(item)
                        return `
                        <div>
                            <p class="yours">你的选区</p>
                            <h5>${item.Constituency_ch}${item.Type_ch}</h5>
                            <p>选民数：${item["Number of Electors"]}</p>
                            <p class="intro">${item.Intro}</p>
                        </div>
                        `})
                }
            })


        } else {
            centerGroup.clearLayers()
        }

        function setUpMarker(center) {
            centerGroup.clearLayers()
            const myIcon = L.icon({
                iconUrl: './static/imgs/marker.svg',
                iconSize: [38, 38],
                iconAnchor: [19, 34],
            });
            L.marker(center, {
                    icon: myIcon
                })
                .addTo(centerGroup)


        }



        // map.on('zoomend', function(e) {
        //     currentZoom = map.getZoom();
        //     console.log(currentZoom)
        //     const currentStyle = zoomStyle(currentZoom);

        //     // pointsGroup.eachLayer(function(layer) {
        //     //     layer.setStyle(currentStyle)
        //     // });

        // });

        // function zoomStyle(currentZoom) {

        //     let style = { radius: 3, fillOpacity: 0.5, weight: 0.3 };
        //     if (currentZoom >= 13.5) {
        //         style.radius = scaleR(currentZoom);
        //         style.weight = scaleWeight(currentZoom)
        //         style.fillOpacity = scaleFillOpacity(currentZoom)
        //     }
        //     return style;
        // }


    }





    updateViz.center = function(_) {
        if (typeof _ === 'undefined') return _center;
        _center = _;
        return this;
    }
    // updateViz.size = function(_) {
    //     if (typeof _ === 'undefined') return _size;
    //     _size = _;
    //     return this;
    // }


    return updateViz
}