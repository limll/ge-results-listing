const GEO = {
    // mainland: d3.json("./static/data/0-mainland.geojson"),
    // region: d3.json("./static/data/1-region.geojson"),
    // streets: d3.json("./static/data/osm_road.geojson")
    
    electoral2015: d3.json("./static/data/electoral-boundary-2015-geojson.geojson"),
    electoral2020: d3.json("./static/data/electoral-boundary-2020-geojson.geojson")
};

const ELECTORS_row = d3.csv('./static/data/GE 2020 electors.csv', parse);


function parse(d){
	d.Constituency = d.Constituency.toUpperCase();
	d.title = d.Constituency_ch
	d.content = d.Intro;
	d.hightlights = [d.Highlight_2020.split(';'),d.Highlight_2015.split(';')]
	return d;
}

const zones = [
        '单选区',
        '四人集选区',
        '五人集选区'

    ]
const zoneColor = d3.scaleOrdinal()
    .domain(zones)
    .range(['#999CCC','#ffd453','#F2637E'])