function geoMap_outline(node, data, projectionData) {
    let _hightlights = []

    function updateViz() {

        let [width, height] = [node.node().clientWidth, node.node().clientHeight];


        const projection = d3.geoMercator();

        projection.fitExtent(
            [
                [0, 0],
                [width, height]
            ],
            projectionData,
        );
        let path = d3.geoPath(projection);
        let svg = node.selectAll('svg').data([0])

        svg = svg.enter().append('svg')
            .attr('class', 'svg')
            .merge(svg)
            .attr('width', width)
            .attr('height', height)

        let border = svg.selectAll('.border').data([0])
        border = border.enter().append('g').attr('class', 'border')
            .merge(border)
        let updateZones = border.selectAll('.zone')
            .data(data.features)
        let enterZones = updateZones.enter().append('path')
            .style('stroke', '#333')
            .style('stroke-width', 1.5).style('stroke-dasharray', "4 2")
            .style('fill', 'none')
            // .attr('id', d => `zone-${d.properties.ED_CODE}`)
            .attr('class', 'zone')
        // .style('cursor', 'pointer')
        enterZones.merge(updateZones).attr('d', path)
            .style('opacity', d => _hightlights.includes(d.properties.Description) ? 1 : 0)

            // .attr('n', d => console.log(_hightlights,d.properties.Description,_hightlights.includes(d.properties.Description)))
        // let updateZones = border.selectAll('.zone')
        //     .data(data.features)
        // let border = svg.selectAll('.border').data([0])
        // border = border.enter().append('path')
        //     .merge(border)
        //     .datum(data)
        //     .attr('class', 'border')
        //     .attr('d', path)
        //     .style('stroke', '#000')
        //     .style('stroke-width', 0.5)
        //     .style('fill', 'none')

    }
    updateViz.hightlights = function(_) {
        if (typeof _ === 'undefined') return _hightlights;
        _hightlights = _;
        return this;
    }

    return updateViz
}