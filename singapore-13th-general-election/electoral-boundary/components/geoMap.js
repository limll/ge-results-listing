function geoMap(node, data,projectionData) {
    let _hightlights = []

    function updateViz() {

        let [width, height] = [node.node().clientWidth, node.node().clientHeight];


        // data = {
        //     type: "FeatureCollection",
        //     features: data
        // }
        // console.log(data)
        const projection = d3.geoMercator();

        projection.fitExtent(
            [
                [0, 0],
                [width, height]
            ],
            projectionData,
        );
        let path = d3.geoPath(projection);
        let svg = node.selectAll('svg').data([0])

        svg = svg.enter().append('svg')
            .attr('class', 'svg')
            .merge(svg)
            .attr('width', width)
            .attr('height', height)

        let border = svg.selectAll('.border').data([0])
        border = border.enter().append('g').attr('class', 'border')
            .merge(border)

        let updateZones = border.selectAll('.zone')
            .data(data.features, d => d.properties.ED_CODE)
        let enterZones = updateZones.enter().append('path')
            .style('stroke', '#ffffff')
            .style('stroke-width', 1)
            .style('fill', d => {
                const type = this.ELECTORS.find(t => t.Constituency === d.properties.ED_DESC).Type_ch
                return zoneColor(type)
            })
            .attr('id', d => `zone-${d.properties.ED_CODE}`)
            .attr('class', 'zone').style('cursor', 'pointer')
        enterZones.merge(updateZones).attr('d', path)
            .on('mouseover', d => {
                // console.log(_hightlights)
                // d3.selectAll('.zone').style('stroke', 'transparent').style('stroke-width', 1.5)
                const item = d3.select(`#zone-${d.properties.ED_CODE}`)
                item.style('stroke-width', 1.5).style('stroke', '#000')
                .moveToFront();
                
                const event = d3.event

                mousetip.classed('hide', false)
                    .style('top', `${event.clientY}px`)
                    .html(n => {
                        const item = this.ELECTORS.find(t => t.Constituency === d.properties.ED_DESC)

                        return `
                        <p class="name">${item.Constituency_ch}</p>
                        <p class="type">${item.Type_ch}</p>
                        <p class="elector">选民数：${item["Number of Electors"]}</p>
                        `
                    })
                if(mousetip.node().clientWidth + event.clientX > width){
                    mousetip.style('left', `${width - mousetip.node().clientWidth}px`)
                }else{
                    mousetip.style('left', `${event.clientX}px`)
                }
                
            })
            .on('mouseout', function() {
                svg.selectAll('.zone').style('stroke', '#fff').style('stroke-width', 1)
                mousetip.classed('hide', true)
            })
            .transition().duration(600)
            .style('opacity', d => ((_hightlights.length === 0) || _hightlights.includes(d.properties.ED_DESC)) ? 1 : 0.2)


    }
    updateViz.hightlights = function(_) {
        if (typeof _ === 'undefined') return _hightlights;
        _hightlights = _;
        return this;
    }

    return updateViz
}