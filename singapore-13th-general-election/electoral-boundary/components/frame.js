function Footer() {
    document.querySelector('footer').innerHTML = `
        <div class="container text-center">
            <div class="row" id="credits">
                <div class="col-md-12 info">
                    <dl>
                        <dt>发布</dt><dd><b>2020-3-13</b></dd>
                    </dl>
    
                    <dl>
                        <dt>文</dt><dd><a href="//www.zaobao.com.sg/byline/huang-xiao-fang" target="_blank">黄小芳</a></dd>
                    </dl>
                    <dl>
                        <dt>设计 + 创建</dt><dd><a href="//www.zaobao.com/data-visualization/yang-liu-hua-ying" target="_blank">杨柳桦樱</a></dd>
                    </dl>
                    <aside>别错过更多 <a href="//zaobao.com/interactive-graphics" target="_blank">zaobao.sg 互动内容</a></aside>
                </div>
            </div>
            <div id="contact">
                <div class="row" id="zbsg-interactive">
                    <div class="col-md-12">
                        <div class="contact-about">
                            <a href="//zaobao.com/" target="_blank"><img src="//interactive.zaobao.com/lib/imgs/zaobaosg-logo-white.svg" title="zaobaosg"></a>
                        </div>
                    </div>
                </div>
                <div class="row" id="zbsg-social">
                    <div class="col-md-12">
                        <h3>关注我们</h3>
                        <div class="social-links">
                            <a href="//www.facebook.com/zaobaosg" target="_blank" class="facebook"><i class="icon-facebook"></i></a>
                            <a href="//twitter.com/zaobaosg" target="_blank" class="twitter"><i class="icon-twitter"></i></a>
                            <a href="//www.instagram.com/zaobaosg" target="_blank" class="instagram"><i class="icon-instagram"></i></a>
                            <a href="//www.youtube.com/c/zaobaosg%E6%97%A9%E6%8A%A5" target="_blank" class="youtube"><i class="icon-youtube-play"></i></a>
                            <a href="//t.me/zaobaosg" target="_blank" class="telegram"><i class="icon-telegram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        © 新加坡报业控股版权所有（公司登记号：198402868E）
                    </div>
                </div>
            </div>
        </div>
`
}
function Header(){
    document.querySelector('header').innerHTML = `
        <nav class="navbar">
            <div class="navbar-header">
                <a class="navbar-brand" href="//www.zaobao.com/" target="_blank"><img src="//interactive.zaobao.com/lib/imgs/zaobaosg-logo.svg" title="zaobao.sg" class="view-desktop"><img src="//interactive.zaobao.com/lib/imgs/zaobaosg-icon.svg" title="zaobao.sg" class="view-mobile"></a>
            </div>
            <div class="social-links"><span class="view-mobile">分享</span>
                <a href="//www.facebook.com/sharer/sharer.php?u=http%3A//interactive.zaobao.com/2020/singapore-13th-general-election/electoral-boundary" target="_blank" class="facebook"><i class="icon-facebook"></i></a>
                <a href="//twitter.com/intent/tweet?text=选区划分委员会报告出炉，选区从原有的29个增至31个，即14个单选区和17个集选区。集选区包括11个五人集选区和6个四人集选区，不再有六人集选区。%20-%20http%3A//interactive.zaobao.com/2020/singapore-13th-general-election/electoral-boundary" target="_blank" class="twitter"><i class="icon-twitter"></i></a>
                <a href="mailto:?subject=[分享] 一览选区划分 你的选区是否有变化？&body=选区划分委员会报告出炉，选区从原有的29个增至31个，即14个单选区和17个集选区。集选区包括11个五人集选区和6个四人集选区，不再有六人集选区。%0D%0Ahttp://interactive.zaobao.com/2020/singapore-13th-general-election/electoral-boundary" target="_blank" class="email"><i class="icon-mail2"></i></a>
            </div>
        </nav>
    `
}



