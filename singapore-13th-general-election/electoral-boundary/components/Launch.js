function Launch() {

    container.append('section').attr('class', 'section start').attr('id', 'start')
        .html(`
            <div class="title">
                <div class="logo"><img src="./static/imgs/${isSmallScreen ? "bg-m" : "bg"}.svg"></div>
                <p>选区划分委员会报告出炉，选区从原有的29个增至31个，即14个单选区和17个集选区。集选区包括11个五人集选区和6个四人集选区，不再有六人集选区。来届大选将有约259万4740名合格选民，议席总数从89个增至93个。</p>
                </div>
            `)

    const Search = container.append('section').attr('class', 'section search')
    const searchContainer = Search.append('div').attr('class', 'search-button')
        .html(`
            <div class="search-input">
                <span><i class="icon-search"></i></span><input type="search" id="postcode-input" placeholder="postcode...">
            </div>
            `)
    // const addressConfirm = searchContainer.select('.search-confirm').append('button')
    //     .attr('class', 'confirm').html(`搜索`)
    const oneMapContainer = Search.append('div').attr('class', 'map-container')
    const oneMapContainer_Map = oneMapContainer.append('div').attr('id', 'onemap')
    const oneMapContainer_result = oneMapContainer.append('div').attr('id', 'map-result')

    const Main = container.append('section').attr('class', 'section main')

    Main.append('div').attr('class', 'section-header').html(`
            <div class="title">
                <h2>一览选区变化</h2>
                </div>
            `)

    const mainMap = Main.append('div').attr('class', 'main-map sticky')
    const mapLegend = mainMap.append('div').attr('class', 'map-legend')
    const newMap = mainMap.append('div').attr('class', 'new-map')
    const oldMap = mainMap.append('div').attr('class', 'old-map')

    const scrollPages = Main.append('div').attr('class', 'scroll-pages')

    // const map2020 = mainMap.append('div').attr('class', 'viz-map')

    Promise.all([GEO.electoral2015, GEO.electoral2020, ELECTORS_row])
        .then(([Electoral2015, Electoral2020, ELECTORS]) => {
            console.log(Electoral2020, Electoral2015, ELECTORS, zones)

            this.ELECTORS = ELECTORS
            this.ELECTORS_CHANGE = ELECTORS.filter(d => d.Change === "新增" || d.Change === "消失")
                .map(d => {
                    d.title = `${d.title}【${d.Change}】`
                    return d;
                })
            this.ELECTORS_MISSING6 = ELECTORS.filter(d => d.Change === "消失的六人集选区")
                .map(d => {
                    d.title = `${d.title}集选区`
                    return d;
                })
            this.ELECTORS_OTHERS = ELECTORS.filter(d => d.Change === "").map(d => {
                d.title = `${d.title}【${d.Type_ch}】`
                return d;
            })

            this.newMapChart = geoMap(newMap, Electoral2020, Electoral2015)
            this.newMapChart()
            this.oldMapChart = geoMap_outline(oldMap, Electoral2015, Electoral2015)
            this.oldMapChart()

            let updateLegend = mapLegend.append('div').attr('class', 'keys')
                .selectAll('.key').data(zones)
            updateLegend = updateLegend.enter().append('div').attr('class', 'key')
                .merge(updateLegend)
                .html(d => `<div><span style="background:${zoneColor(d)}"></span>${d}</div>`)

            mapLegend.select('.keys').append('div').attr('class', 'key')
                .html(d => `<div><span class="old"></span>2015年选区划分</div>`)

            mapLegend.append('div').attr('class', 'note')
                .html(d => `<img src="./static/imgs/hand.svg"><span>查看选区名称</span>`)

            const Pages = [{
                    title: "消失与新增的单选区",
                    content: "单选区的变动最大，共划分出四个新的单选区，现有的三个单选区则被划入集选区。",
                    hightlights: [
                        [],
                        []
                    ],
                    bigger: true
                },
                ...this.ELECTORS_CHANGE,

                {
                    title: "消失的六人集选区",
                    content: "1997年首次出现六人集选区，但如今都已消失。宏茂桥和白沙——榜鹅这次都缩小范围，变成五人集选区，部分范围被划入其他选区。<br>从2006年全国大选至今，集选区的平均规模已从5.36个席位，减少至4.65个席位。李显龙总理早在2009年宣布一系列政治改革时就指出，会逐步减少六人集选区，使集选区的平均议员人数不超过五人。",
                    hightlights: [
                        [],
                        ["ANG MO KIO", "PASIR RIS-PUNGGOL"]
                    ],
                    bigger: true
                },
                ...this.ELECTORS_MISSING6,
                {
                    title: "其他集选区的变化 ",
                    content: "",
                    hightlights: [
                        [],
                        []
                    ],
                    bigger: true
                },
                // ...this.ELECTORS_OTHERS,


            ].map((d, i) => { d.id = `${i}`; return d; })
            let updatePages = scrollPages.selectAll('.scroll-page').data(Pages)
            updatePages = updatePages.enter().append('div').attr('class', 'scroll-page')
                .attr('id', (d, i) => `page-${i}`)
                .merge(updatePages)
                .html(d => `
                <div class="page">
                    <h3 class="${d.bigger ? 'bigger': ''}">${d.title}</h3>
                    <p>${d.content}</p>
                </div>
                `)

            const lastPage = container.select(`#page-${Pages[Pages.length-1].id}`).select('.page')
            lastPage.select('p').remove()
            const explain = lastPage.append('div').attr('id', `zone-explain`)
            lastPage.insert('select', '#zone-explain').attr('id', `zone-option`)
                .on('change', d => {
                    const value = container.select('#zone-option').node().value
                    const pageContent = this.ELECTORS_OTHERS.find(t => t.Constituency_ch === value)
                    this.newMapChart.hightlights(pageContent.hightlights[0])()
                    oldMap.classed('show', true)
                    this.oldMapChart.hightlights(pageContent.hightlights[1])()
                    explain.html(pageContent.Intro)
                })
                .selectAll('option').data(this.ELECTORS_OTHERS)
                .enter().append('option').attr('class', 'options')
                .attr('value', d => d.Constituency_ch)
                .html(d => `${d.Constituency_ch}${d.Type_ch}`)

            container.select('#zone-option').insert('option', '.options')
                .html(`-- 选择一个选区 -- `)
                .property('hidden', true).property('disabled', true).property('selected', true).property('value', true)


            // search postcode
            const searchInput = d3.select('#postcode-input')
            const SearchMap = oneMap('onemap', Electoral2020)
            // searchInput.addEventListener('change', (event) => {
            //     displayMatches(caseInput.value)
            // });
            // searchInput.addEventListener('search', (event) => {
            //     d3.select('.search-suggestion').classed('hide', false)
            //     displayMatches(caseInput.value)
            // });
            searchInput.node().addEventListener('keyup', (event) => {
                // d3.select('.search-suggestion').classed('hide', false)
                if (event.keyCode === 13) {

                    const value = searchInput.value
                    // if (this.CASES.find(d => +d.id === +value)) {
                    //     this.isSearchId = true;
                    // }
                    search_onchange(value)
                } else {
                    // displayMatches(caseInput.value)
                }

            });

            function search_onchange() {
                console.log('sp')
                const input_value = searchInput.property('value');
                if (input_value === "") return;
                d3.json(`https://developers.onemap.sg/commonapi/search?searchVal=${input_value}&returnGeom=Y&getAddrDetails=Y`)
                    .then(d => {

                        if (d.results.length > 0) {
                            console.log(d.results)

                            const result = d.results[0];
                            SearchMap.center([result.LATITUDE, result.LONGTITUDE])()
                            // const home = initialLabel(`${result['BLK_NO']},${result['ROAD_NAME']}`);
                            // const isHDB = HDB.find(n => n.address === home)
                            // if (isHDB) {
                            //     result.LATITUDE = isHDB.lat;
                            //     result.LONGTITUDE = isHDB.lon;
                            // }
                            // console.log(d.results, home, isHDB)
                            // updateBuilding(result, isHDB, priceMap, buildingswithprices, storeyByBuilding)
                        } else {
                            // ;
                            // d3.select('.search-result').classed('hide', false)
                            // d3.select('.building-prices').classed('hide', true)
                            // d3.select('.building-info').html(`
                            //     <div>没有找到该地址。</div>
                            //     `)
                        }
                    });
            }


            //scrolling
            document.addEventListener('scroll', detectScroll);
            let allPages = d3.selectAll('.scroll-page').nodes();
            allPages.push(d3.select('.start').node())
            let currentPage = allPages[0];

            function detectScroll(e) {
                window.requestAnimationFrame(() => {

                    const [top, bottom] = [h * .15, h * .95];
                    allPages.forEach(page => {

                        const pageBox = page.getBoundingClientRect()
                        // console.log(pageBox)
                        const trigger = (pageBox.bottom - pageBox.top) * 0.2 + pageBox.top;
                        if (trigger > top && trigger < bottom) {

                            if (page.id !== currentPage) {
                                currentPage = page.id;
                                updateStory()

                            }

                        }
                    })
                })
            }

            function updateStory() {
                console.log(currentPage)
                switch (currentPage) {
                    case "start":
                        oldMap.classed('show', false)
                        this.newMapChart.hightlights([])()
                        break;
                        // case "0":
                        //     oldMap.classed('show', false)
                        //     break;
                        // case "1":
                        //     oldMap.classed('show', true)
                        //     break;
                    default:
                        // console.log(Pages)
                        const pageContent = Pages.find(d => `page-${d.id}` === currentPage)
                        // console.log(pageContent)
                        this.newMapChart.hightlights(pageContent.hightlights[0])()
                        oldMap.classed('show', currentPage !== "0")
                        this.oldMapChart.hightlights(pageContent.hightlights[1])()

                }
            }
            window.addEventListener("resize", () => {
                getSizes();

                //viz module
                this.newMapChart()
                this.oldMapChart()

            });

        })

}

Launch()