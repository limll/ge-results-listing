
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"> 
<meta charset="utf-8">

<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
dataLayer.push({
    "level2": "INTERACTIVE-NAME",
    "title": "HEADLINE TITLE",
});
</script>
<!-- Google Tag Manager
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVFMLMK');</script> -->
<!-- End Google Tag Manager -->

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="index,follow">
<meta name="GOOGLEBOT" content="index,follow">
<meta http-equiv="cache-control" content="max-age=0, no-cache, no-store, must-revalidate" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="author" content="" />

<title>选区划分</title>
<meta name="description" content="SOMETHING SHORT AND BRIEF ENOUGH TO DESCRIBE THE TOPIC" />
<meta name="keywords" content="早报, KEYWORDS HIGHLY RELEVANT TO THE TOPIC, NAMES ARE IMPORTANT TOO" />
<link rel="image_src" href="//interactive.zaobao.com/YYYY/FOLDER-NAME/IMGS/SOCIAL-SHARE-IMG" />

<?php include "assets/inc/global.html"?>
<!--#include file="assets/inc/global.html" -->

</head>

<body id="page-home" ng-app="App.controllers" ng-controller="MainCtrl" ng-cloak>
<?php include "assets/inc/gtm.html"?>
<!--#include file="assets/inc/gtm.html" -->

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

<?php include "assets/inc/brand.html"?>
<!--#include file="assets/inc/brand.html" -->

  <div class="content mdl-layout__content mdl-typography--text-center">
    <a name="top"></a>

    <div class="jumbotron">
      <img src="imgs/graphics/sg-ge2020-jumbotron-large.svg" class="view-desktop" /><img src="imgs/graphics/sg-ge2020-jumbotron-mobile.svg" class="view-mobile" />
    </div>

    <div class="container">
      <div class="-slice new-smc card-container mdl-grid">
        <div class="-slice-box mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
          <div class="-item">
            <h3>轻松看大选</h3>
            <p>Rally, news coverage, Sheo Be's interviews, AM / PM Daily shows</p>
          </div>
        </div>
        <div class="-slice-box mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
          <div class="-item">
            <h3>Parties Activities</h3>
            <p>Curated Party social media post embeds</p>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<?php include "assets/inc/news-feed.html"?>
<?php include "assets/inc/footer.html"?>
<!--#include file="assets/inc/footer.html" -->

<?php include "assets/inc/scripts.html"?>
<!--#include file="assets/inc/scripts.html" -->


</body>
</html>
