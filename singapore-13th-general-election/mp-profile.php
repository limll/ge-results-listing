
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"> 
<meta charset="utf-8">

<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
dataLayer.push({
    "level2": "INTERACTIVE-NAME",
    "title": "HEADLINE TITLE",
});
</script>
<!-- Google Tag Manager
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVFMLMK');</script> -->
<!-- End Google Tag Manager -->

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="index,follow">
<meta name="GOOGLEBOT" content="index,follow">
<meta http-equiv="cache-control" content="max-age=0, no-cache, no-store, must-revalidate" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="author" content="" />

<title>候选人资料</title>
<meta name="description" content="SOMETHING SHORT AND BRIEF ENOUGH TO DESCRIBE THE TOPIC" />
<meta name="keywords" content="早报, KEYWORDS HIGHLY RELEVANT TO THE TOPIC, NAMES ARE IMPORTANT TOO" />
<link rel="image_src" href="//interactive.zaobao.com/YYYY/FOLDER-NAME/IMGS/SOCIAL-SHARE-IMG" />

<link rel="stylesheet" href="//interactive.zaobao.com/lib/css/bootstrap/v3.3.7/bootstrap.min.css">
<link rel="stylesheet" href="//interactive.zaobao.com/lib/css/font-awesome/v5.8.1/all.css">
<?php include "assets/inc/global.html" ?>

<style>
  ul ul.filter-party{height:auto;}
</style>
</head>

<body id="page-mp-profile" ng-app="App.controllers" ng-controller="MPCtrl" ng-cloak>
<?php include "assets/inc/gtm.html" ?>

<div class="000mdl-layout mdl-js-layout mdl-layout--fixed-header">

<?php include "assets/inc/brand.html" ?>

  <div class="content 000mdl-layout__content">
    <a name="top"></a>

    <div class="tab-wrap no-swiper">
      <div class="tab-container">
        <div class="000tab-container-inner">
          <div class="000swiper-wrapper">
            <div class="000swiper-slide">
              <div class="content-section full-width">
                <div id="MPFilter">
                  <div id="sticky" style="text-align:center">
                    <div id="filter-option">
                      <select name="constituency" id="constituency" class="select">
                        <option value="">选区</option>
                        <option value="阿裕尼集选区">阿裕尼集选区</option>
                        <option value="宏茂桥集选区">宏茂桥集选区</option>
                        <option value="碧山--大巴窑集选区">碧山--大巴窑集选区</option>
                        <option value="武吉巴督单选区">武吉巴督单选区</option>
                        <option value="武吉班让单选区">武吉班让单选区</option>
                        <option value="蔡厝港集选区">蔡厝港集选区</option>
                        <option value="东海岸集选区">东海岸集选区</option>
                        <option value="荷兰--武吉知马集选区">荷兰--武吉知马集选区</option>
                        <option value="丰加北单选区">丰加北单选区</option>
                        <option value="后港单选区">后港单选区</option>
                        <option value="裕廊集选区">裕廊集选区</option>
                        <option value="哥本峇鲁单选区">哥本峇鲁单选区</option>
                        <option value="麦波申单选区">麦波申单选区</option>
                        <option value="马林百列集选区">马林百列集选区</option>
                        <option value="马西岭--油池集选区">马西岭--油池集选区</option>
                        <option value="玛丽蒙单选区">玛丽蒙单选区</option>
                        <option value="蒙巴登单选区">蒙巴登单选区</option>
                        <option value="义顺集选区">义顺集选区</option>
                        <option value="白沙--榜鹅集选区">白沙--榜鹅集选区</option>
                        <option value="先驱单选区">先驱单选区</option>
                        <option value="波东巴西单选区">波东巴西单选区</option>
                        <option value="榜鹅西单选区">榜鹅西单选区</option>
                        <option value="拉丁马士单选区">拉丁马士单选区</option>
                        <option value="三巴旺集选区">三巴旺集选区</option>
                        <option value="盛港集选区">盛港集选区</option>
                        <option value="淡滨尼集选区">淡滨尼集选区</option>
                        <option value="丹戎巴葛集选区">丹戎巴葛集选区</option>
                        <option value="西海岸集选区">西海岸集选区</option>
                        <option value="杨厝港单选区">杨厝港单选区</option>
                        <option value="裕华单选区">裕华单选区</option>
                      </select>
                      <select name="party" id="party" class="select">
                        <option value="">政党</option>
                        <option value="人民行动党">人民行动党</option>
                        <option value="工人党">工人党</option>
                        <!--option value="新加坡民主党">新加坡民主党</option>
                        <option value="新加坡前进党">新加坡前进党</option>
                        <option value="新加坡人民党">新加坡人民党</option>
                        <option value="革新党">革新党</option>
                        <option value="新加坡民主联盟">新加坡民主联盟</option>
                        <option value="国人为先党">国人为先党</option>
                        <option value="国民团结党">国民团结党</option>
                        <option value="人民力量党">人民力量党</option>
                        <option value="人民之声">人民之声</option>
                        <option value="民主进步党">民主进步党</option>
                        <option value="独立候选人">独立候选人</option-->
                      </select>
                      <select name="term" id="term" class="select">
                        <option value="">任期</option>
                        <option value="1">一届</option>
                        <option value="2">两届</option>
                        <option value="3">三届或以上</option>
                      </select>
                      <select name="gender" id="gender" class="select">
                        <option value="">性别</option>
                        <option value="M">男</option>
                        <option value="F">女</option>
                      </select>
                      <select name="participation" id="participation" class="select">
                        <option value="">新面孔</option>
                        <option value="new">新人</option>
                        <!--option value="old">老将</option-->
                      </select>

                      <select name="sort" id="sort" class="select sort">
                        <option value="">排序方式</option>
                        <option value="years">从政资历</option>
                        <option value="age">年龄</option>
                      </select>

                      <!--div id="event-change"></div-->
                    </div>
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable filter-search">
                        <label class="mdl-button mdl-js-button mdl-button--icon" for="site-search">
                          <i class="material-icons">search</i>
                        </label>
                        <div class="mdl-textfield__expandable-holder" style="margin-top: 6px;">
                          <input class="mdl-textfield__input" type="search" id="site-search" placeholder="快捷搜索" ng-model="searchText" ng-change="searchMinister()" style="border: none;"/>
                          <label class="mdl-textfield__label" for="site-search">Search</label>
                        </div>
                      </div>
                  </div>
                  <!-- <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable filter-search">
                    <label class="mdl-button mdl-js-button mdl-button--icon" for="site-search">
                      <i class="material-icons">search</i>
                    </label>
                    <div class="mdl-textfield__expandable-holder">
                      <input class="mdl-textfield__input" type="search" id="site-search" placeholder="快捷搜索" ng-model="searchText" ng-change="searchMinister()"/>
                      <label class="mdl-textfield__label" for="site-search">Search</label>
                    </div>
                  </div>
                  
                  <div> -->
                 
                  
                  <div class="mp-panel">
                    <div class="row text-center" ng-if="showSpinner">
                      <div class="col-12">
                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                      </div>
                    </div>
                   
                    <div class="row list">
                      <div class="active-with-click">
                      
                        <div class="col-md-3 col-sm-6 col-xs-12 minister"  ng-repeat="mp in ministers | orderBy: 'gsx$mpnameen.$t'" data-years="{{mp.gsx$term.length}}" data-age="{{mp.gsx$age.$t}}" data-2020="{{mp.gsx$resultspercentage2020.$t}}">
                          <article class="material-card party-member-info {{mp.gsx$gender.$t}}" id="{{mp.gsx$mpnamecn.$t}}" ng-class="{'mc-active': card === mp.gsx$mpnamecn.$t, 'new-face': mp.gsx$newface.$t === 'Y' }" data-en-name="{{mp.gsx$mpnameen.$t}}" data-political-party="{{mp.gsx$politicalpartyen.$t}}" data-constituency = "{{mp.gsx$consituencyen.$t}}">
                            <h2 class="party-member-label">
                              <div class="member-territory">
                                <div class="member-name"><span class="ch">{{mp.gsx$mpnamecn.$t}}</span></div>
                                <div class="constituency-name"><span class="ch">{{mp.gsx$constituency.$t}}</span></div>
                              </div>
                              <div class="party-logo party-pap" ng-if="mp.gsx$politicalpartycn.$t === '人民行动党'"></div>
                                <div class="party-logo party-wp" ng-if="mp.gsx$politicalpartycn.$t === '工人党'"></div>
                            </h2>
                            <div class="mc-content">
                              <div class="img-container"><img class="profile-img" src="party/imgs/{{mp.gsx$politicalpartyen.$t}}/{{mp.gsx$image.$t}}"></div>
                              <div class="mc-detail">
                                <div class="member-designation">{{mp.gsx$jobtitle.$t}}</div>
                                <div class="result-panel">
                                  <div class="header-title">大选成绩</div>
                                  <div class="election-performance">
                                    <!--div class="election-vote current">
                                      <label>2020年</label>
                                      <span class="percent-2020">{{mp.gsx$resultspercentage2020.$t}}<em>%</em></span>
                                      <small>{{mp.gsx$resultsno.ofvotes2020.$t}}</small>
                                    </div-->
                                    <div class="election-vote current" style="width:100%;">
                                      <label>2015年</label>
                                      <span class="percent-2015">{{mp.gsx$resultspercentage2015.$t}}<em>%</em></span>
                                      <small>{{mp.gsx$resultsno.ofvotes2015.$t}}</small>
                                    </div>
                                  </div>
                                </div>
                                <div class="serving-term">
                                  <label>任期</label>
                                  <div class="term-no {{mp.gsx$term.length}}-term">
                                    <span class="term" ng-repeat="term in mp.gsx$term">{{term}}</span>
                                  </div>
                                </div>
                                <div class="member-profile">
                                  <div class="member-age"><span class="en">{{mp.gsx$age.$t}}</span><em>岁</em></div> {{mp.gsx$introduction.$t}}
                                </div>
                                <div class="get-social"><a ng-href="{{mp.gsx$fblink.$t}}" class="mdi mdi-facebook icon-facebook" target="_blank"></a></div>
                              </div>
                            </div>
                            <a class="mc-btn-action" ng-click="showDetails(mp.gsx$mpnamecn.$t)"><i class="mdi mdi-arrow-down"></i></a>
                            <div class="mc-footer">
                              <div class="party-profile">
                                <div class="party-name">
                                  <span class="ch">{{mp.gsx$politicalpartycn.$t}}</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '人民行动党'">People's Action Party</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '工人党'">Workers' Party</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '新加坡前进党'">Progress Singapore Party</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '革新党'">The Reform Party</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '新加坡民主党'">Singapore Democratic Party</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '国人为先党'">Singaporeans First</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '人民力量党'">Peoples's Power Party</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '人民之声'">Peoples's Voice</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '国民团结党'">National Solidarity Party</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '新加坡民主联盟'">Singapore Democratic Alliance</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '民主进步党'">Democratic Progressive Party</span>
                                  <span class="en" ng-if="mp.gsx$politicalpartycn.$t === '新加坡人民党'">Singapore People's Party</span>
                                </div>
                                <div class="party-logo party-pap" ng-if="mp.gsx$politicalpartycn.$t === '人民行动党'"></div>
                                <div class="party-logo party-wp" ng-if="mp.gsx$politicalpartycn.$t === '工人党'"></div>
                                <div class="party-logo party-psp" ng-if="mp.gsx$politicalpartycn.$t === '新加坡前进党'"></div>
                                <div class="party-logo party-trp" ng-if="mp.gsx$politicalpartycn.$t === '革新党'"></div>
                                <div class="party-logo party-sdp" ng-if="mp.gsx$politicalpartycn.$t === '新加坡民主党'"></div>
                                <div class="party-logo party-sf" ng-if="mp.gsx$politicalpartycn.$t === '国人为先党'"></div>
                                <div class="party-logo party-ppp" ng-if="mp.gsx$politicalpartycn.$t === '人民力量党'"></div>
                                <div class="party-logo party-pv" ng-if="mp.gsx$politicalpartycn.$t === '人民之声'"></div>
                                <div class="party-logo party-nsp" ng-if="mp.gsx$politicalpartycn.$t === '国民团结党'"></div>
                                <div class="party-logo party-sda" ng-if="mp.gsx$politicalpartycn.$t === '新加坡民主联盟'"></div>
                                <div class="party-logo party-dpp" ng-if="mp.gsx$politicalpartycn.$t === '民主进步党'"></div>
                                <div class="party-logo party-spp" ng-if="mp.gsx$politicalpartycn.$t === '新加坡人民党'"></div>
                              </div>
                            </div>
                          </article>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>

    </div>

  </div>
</div>

<?php include "assets/inc/news-feed.html"?>
<?php include "assets/inc/footer.html"?>
<script src="../lib/js/jquery/v3.3.1/jquery.min.js"></script>
<script src="../lib/js/angular/v1.4.9/angular.min.js"></script>
<script src="../lib/js/angular/angular-sanitize/v1.5.8/angular-sanitize.min.js"></script>
<script src="assets/js/controllers.js"></script>
<script src="../lib/js/progressbar-prognroll.js"></script>
<script src="assets/js/swiper.jquery-v3.3.1.min.js"></script>
<script src="../lib/js/material-design/material-v1.3.0.min.js"></script>
<!-- <script src="assets/js/script.js"></script> -->

<!-- <script src="assets/js/listjs-v1.5.0.min.js"></script> -->
<!-- <script src="assets/js/tinysort-2.2.2.min.js" type="text/javascript" charset="utf-8"></script> -->
<!-- <script src="assets/js/script-filter.js"></script> -->


</body>
</html>
