
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"> 
<meta charset="utf-8">

<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
dataLayer.push({
    "level2": "INTERACTIVE-NAME",
    "title": "HEADLINE TITLE",
});
</script>
<!-- Google Tag Manager
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVFMLMK');</script> -->
<!-- End Google Tag Manager -->

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="index,follow">
<meta name="GOOGLEBOT" content="index,follow">
<meta http-equiv="cache-control" content="max-age=0, no-cache, no-store, must-revalidate" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="author" content="" />

<title>政党介绍</title>
<meta name="description" content="SOMETHING SHORT AND BRIEF ENOUGH TO DESCRIBE THE TOPIC" />
<meta name="keywords" content="早报, KEYWORDS HIGHLY RELEVANT TO THE TOPIC, NAMES ARE IMPORTANT TOO" />
<link rel="image_src" href="//interactive.zaobao.com/YYYY/FOLDER-NAME/IMGS/SOCIAL-SHARE-IMG" />

<?php include "assets/inc/global.html" ?>
</head>

<body id="page-party">
<?php include "assets/inc/gtm.html" ?>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

<?php include "assets/inc/brand.html" ?>

  <div class="content mdl-layout__content">
    <a name="top"></a>

    <div class="tab-wrap no-swiper">
      <div class="sticker000">
        <nav class="tab-navigation content-section">
          <ul class="links-container">
            <li class="nav-link active"><a href="party.php">政党介绍</a></li>
            <li class="nav-link"><a href="party-strength.php">政见比一比</a></li>
          </ul>
        </nav>
      </div>
      <div class="tab-container">
        <div class="000tab-container-inner">
          <div class="000swiper-wrapper">
            <div class="000swiper-slide">
              <div class="content-section">
                <!--p class="padding-10">本届全国大选在冠病疫情笼罩之下举行。在全球经济与周遭地缘政治状况不稳定因素加剧的时刻，长期执政的人民行动党将力求平稳过渡，让第四代领导班子顺利接棒。传承换代的也不只行动党而已：由新党魁毕丹星领导的工人党，将尝试在原有的政治基础上继续挺进；少了元老级人物詹时中的人民党，则力求建立新的身份，否则面临泡沫化的命运。此时，在反对党阵营里也冒出新政党，包括由陈清木领导的新加坡前进党，高调造势。后选前，一些规模较小的反对党则尝试组织联盟，但计划频频受挫，各选区是否掀多角战将受到关注。</p-->
                <p class="padding-10">在全球经济低迷与政治不稳定因素加剧的时刻，长期执政的人民行动党将力求平稳过渡，让第四代领导班子顺利解棒。换代的不只是人民行动党，由新党魁毕丹星领导的工人党，将尝试在原有的政治基础上继续挺进；少了元老级人物詹时中的人民党，则力求建立新的身份，否则面临泡沫化的命运。此时，在反对党阵营里冒出由陈清木领导的新政党后，一些小党也在尝试组织联盟。</p>
              </div>

              <div id="partyison">
                <ul class="party-tabs">
                  <li data-tab-target="#party-pap" class="active party-tab"><a href="#party-pap"><div class="party-logo party-pap"></div><div class="party-name">人民行动党</div></a></li>
                  <li data-tab-target="#party-wp" class="party-tab"><a href="#party-wp"><div class="party-logo party-wp"></div><div class="party-name">工人党</div></a></li>
                  <li data-tab-target="#party-sdp" class="party-tab"><a href="#party-sdp"><div class="party-logo party-sdp"></div><div class="party-name">新加坡民主党</div></a></li>
                  <li data-tab-target="#party-psp" class="party-tab"><a href="#party-psp"><div class="party-logo party-psp"></div><div class="party-name">新加坡前进党</div></a></li>
                  <li data-tab-target="#party-spp" class="party-tab"><a href="#party-spp"><div class="party-logo party-spp"></div><div class="party-name">新加坡人民党</div></a></li>
                  <li data-tab-target="#party-trp" class="party-tab"><a href="#party-trp"><div class="party-logo party-trp"></div><div class="party-name">革新党</div></a></li>
                  <li data-tab-target="#party-sda" class="party-tab"><a href="#party-sda"><div class="party-logo party-sda"></div><div class="party-name">新加坡民主联盟</div></a></li>
                  <li data-tab-target="#party-sf" class="party-tab"><a href="#party-sf"><div class="party-logo party-sf"></div><div class="party-name">国人为先党</div></a></li>
                  <li data-tab-target="#party-nsp" class="party-tab"><a href="#party-nsp"><div class="party-logo party-nsp"></div><div class="party-name">国民团结党</div></a></li>
                  <li data-tab-target="#party-ppp" class="party-tab"><a href="#party-ppp"><div class="party-logo party-ppp"></div><div class="party-name">人民力量党</div></a></li>
                  <li data-tab-target="#party-pv" class="party-tab"><a href="#party-pv"><div class="party-logo party-pv"></div><div class="party-name">人民之声</div></a></li>
                  <li data-tab-target="#party-dpp" class="party-tab"><a href="#party-dpp"><div class="party-logo party-dpp"></div><div class="party-name">民主进步党</div></a></li>
                </ul>

                <div class="party-tab-content">
                  <!-- PAP -->
                  <div id="party-pap" class="party-tab-detail active" data-tab-content><a name="party-pap"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>人民行动党</span></h3>
                        <div class="blurb">这将是李显龙总理最后一次以秘书长身份领导该党争取选民委托。第四代领导大致到位的人民行动党，以国家的持久昌盛为目标，强调国家需要一个有能力的团队，才能处理好对外关系，也保持内部团结。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <p class="padding-10">新的领导班子以王瑞杰为“同侪之首”，透过“群策群力，共创未来”（Singapore Together）的全国运动，主张强化实干民主，邀请国人参与国家愿景的塑造。各年轻部长也积极进行选区走访，与民众建立感情。王瑞杰指出，他们意识到执政权需靠争取，不能世袭。行动党本届大选的表现，将被视为人民对新领导班子的信心指标。</p>

                      <div class="duo-quote">
                        <blockquote class="mdl-cell mdl-cell--6-col">
                          <span>这届大选将是一场硬仗，不是办家家酒，而是攸关新加坡的未来。国人的团结是我国外交政策的第一道防线，他国将密切关注这场选举，看行动党能否赢得强有力的委托。</span>
                          <cite>
                            <div class="party-member">
                              <div class="member-name">秘书长<b>李显龙</b></div>
                              <div class="member-img"><img src="party/imgs/pap/lee-hsien-loong.png" /></div>
                            </div>
                          </cite>
                          <div class="plate"></div>
                        </blockquote>
                        <blockquote class="mdl-cell mdl-cell--6-col">
                          <span>只要我们群策群力，就能发挥更大的影响。我们得号召整个社会遗弃决定，我们的目标有哪些、要帮助哪些群体、要做些什么，以及我们能否更紧密合作。</span>
                          <cite>
                            <div class="party-member">
                              <div class="member-name">第一助理秘书长<b>王瑞杰</b></div>
                              <div class="member-img"><img src="party/imgs/pap/heng-swee-keat.png" /></div>
                            </div>
                          </cite>
                          <div class="plate"></div>
                        </blockquote>
                      </div>

                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-pap"></div>
                              <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--6-col"><label>创立年份</label><span>1954年</span></div>
                                <div class="parliament-seats mdl-cell mdl-cell--6-col"><label>国会席数</label><span><b>83</b> <em>/</em> 89</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>68.86<em>%</em></span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-left"><img src="party/imgs/pap/lee-hsien-loong.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>李显龙</span><label>秘书长</label></div></div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="party-members">
                          <div class="party-box other-members">
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/pap/heng-swee-keat.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>王瑞杰</span><label>第一助理秘书长</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/pap/chan-chun-sing.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>陈振声</span><label>第二助理秘书长</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/pap/gan-kim-yong.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>颜金勇</span><label>主席</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/pap/masagos-zulkifli.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>马善高</span><label>副主席</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/pap/tharman-shanmugaratnam.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>尚穆根</span><label>财政</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/pap/ong-ye-kung.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>王乙康</span><label>助理财政</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/pap/grace-fu-hai-yien.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>傅海燕</span><label>组织秘书</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/pap/desmond-lee.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>李智陞</span><label>组织秘书</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/pap/josephine-teo.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>杨莉明</span><label>妇女团主席</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/pap/tan-chuan-jin.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>陈川仁</span><label>乐龄行动小组主席</label></div></div>
                            </div>
                          </div>
                          <div class="party-box other-members other-mp">
                            <h4>其他中委</h4>
                            <div class="party-member">
                              <div class="member-img align-center"><img src="party/imgs/pap/christopher-de-souza.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>迪舒沙</span></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/pap/indranee-thurai-rajah.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>英兰妮</span></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/pap/ng-chee-meng.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>黄志明</span></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-center"><img src="party/imgs/pap/ng-eng-hen.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>黄永宏医生</span></div></div>
                            </div>
                          </div>
                          <div class="party-box other-members other-mp">
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/pap/sitoh-yih-pin.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>司徒宇斌</span></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-center"><img src="party/imgs/pap/vivian-balakrishnan.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>维文医生</span></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/pap/lawrence-wong.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>黄循财</span></div></div>
                            </div>
                          </div>
                          <div class="item">
                            <a href="mp-profile.php#ang-mo-kio" class="mdl-button" target="_blank">候选人履历</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- WP -->
                  <div id="party-wp" class="party-tab-detail" data-tab-content><a name="party-wp"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>工人党</span></h3>
                        <div class="blurb">走过60载，这个老牌政党迎来了一支较年轻的领导团队。当了17年党魁的刘程强将棒子交到毕丹星手中，完成他认为最重要的更新进程。工人党也定下中期目标：竞选与夺下国会三分一议席，以确保议事殿堂中朝野议席数量有“正确平衡”。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <p class="padding-10">在国会，工人党如今是唯一的反对党。该党上届大选中成功保住后港区和阿裕尼集选区，但得票率下降。过去几年，它尝试改善市镇会管理，赢回选民的信任，但关键人物如刘程强和主席林瑞莲，因市镇会财务管理不当的问题而诉讼缠身，也可能让该党的备战应对受到牵制。</p>

                      <div class="duo-quote">
                        <blockquote class="mdl-cell mdl-cell--6-col">
                          <span>我们努力的方向，应是打造更平衡的国会格局，由民选的反对党议员对行动党进行实际、有意义的监督，反映国民的心声，同时累积公共部门的经验，以成为有效的议员和市镇理事会理事。</span>
                          <cite>
                            <div class="party-member">
                              <div class="member-name">秘书长<b>毕丹星</b></div>
                              <div class="member-img"><img src="party/imgs/wp/pritam-singh.png" /></div>
                            </div>
                          </cite>
                          <div class="plate"></div>
                        </blockquote>
                        <blockquote class="mdl-cell mdl-cell--6-col">
                          <span>我认为我们已经顺利转型，成为一个与年轻一代新加坡人有密切联系的政党。这对于政党的进步非常重要。</span>
                          <cite>
                            <div class="party-member">
                              <div class="member-name">前秘书长<b>刘程强</b></div>
                              <div class="member-img"><img src="party/imgs/wp/low-thia-khiang.png" /></div>
                            </div>
                          </cite>
                          <div class="plate"></div>
                        </blockquote>
                      </div>
                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-wp"></div>
                              <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--6-col"><label>创立年份</label><span>1957年</span></div>
                                <div class="parliament-seats mdl-cell mdl-cell--6-col"><label>国会席数</label><span><b>6</b> <em>/</em> 89</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>39.75<em>%</em></span></div>
                              <div class="territory-district"><label>管辖选区</label><span>阿裕尼集选区、后港区</span></div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 东海岸、盛港、马林百列、惹兰勿刹、义顺<br/>
                                <b>单选区</b> 榜鹅东、麦波申、哥本峇鲁</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-right"><img src="party/imgs/wp/pritam-singh.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>毕丹星</span><label>秘书长</label></div></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="party-members">
                          <div class="party-box other-members">
                            <h4>中央委员会成员</h4>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/wp/sylvia-lim-swee-lian.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>林瑞莲</span><label>主席</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/wp/muhamad-faisal-bin-abdul-manap.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>莫哈默费沙</span><label>副主席</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/wp/gerald-giam-yean-song.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>严燕松</span><label>财政</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/wp/lee-li-lian.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>李丽连</span><label>副财政</label></div></div>
                            </div>
                          </div>
                          <div class="party-box other-members">
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/wp/dennis-tan-lip-fong.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>陈立峰</span><label>组织秘书</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/wp/daniel-goh.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>吴佩松</span><label>组织秘书传媒组主席</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/wp/terence-tan-li-chern.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>陈励正</span><label>副组织秘书</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/wp/leon-perera.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>贝里安</span><label>青年团主席</label></div></div>
                            </div>
                          </div>
                          <div class="party-box other-members other-mp">
                            <h4>其他中委</h4>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/wp/low-thia-khiang.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>刘程强</span></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/wp/png-eng-huat.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>方荣发</span></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/wp/firuz-khan.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>费鲁兹</span></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/wp/john-yam-poh-nam.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>任保南</span></div></div>
                            </div>
                          </div>
                          <div class="item">
                            <a href="mp-profile.php#aljunied" class="mdl-button" target="_blank">候选人履历</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- SDP -->
                  <div id="party-sdp" class="party-tab-detail" data-tab-content><a name="party-sdp"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>新加坡民主党</span></h3>
                        <div class="blurb">民主党动作迅速且高调，早在2019年下半年就举办选前群众大会、推出竞选口号“前进之路”，也有完整的竞选纲领。党秘书长徐顺全一再对行动党开弓，批评新旧领导“老调重弹”，也尝试把第四代领导班子描述成墨守成规、无法体恤民情的部长。他认为，与其活在过去国家由行动党领导下取得的成就中，选民应该选择一个能打造开放自由的社会、有胆识面对新世界的政党。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <p class="padding-10">该党过去几年人事与领导结构逐步稳定，党中委几乎是上一届大选同一班人马。这次大选，徐顺全表示将开始推动更新，不排除让更多年轻人参选，社交媒体与网络空间将继续是民主党的“主战场”。</p>

                      <div class="single-quote">
                        <blockquote class="mdl-cell mdl-cell--12-col">
                          <span>如果我们认为能从过去找到开启未来的钥匙，那我们会比想象中碰到更多麻烦，因为我们会活在过去的成就中，没有足够胆识面对这个不断变化的世界。</span>
                          <cite>
                            <div class="party-member">
                              <div class="member-name">秘书长<b>徐顺全</b></div>
                              <div class="member-img"><img src="party/imgs/sdp/chee-soon-juan.png" /></div>
                            </div>
                          </cite>
                          <div class="plate"></div>
                        </blockquote>
                      </div>
                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-sdp"></div>
                              <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--6-col"><label>创立年份</label><span>1980年</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>31.23<em>%</em></span></div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 荷兰—武吉知马、马西岭—油池<br/>
                                <b>单选区</b> 武吉巴督、武吉班让、裕华</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-right"><img src="party/imgs/sdp/chee-soon-juan.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>徐顺全</span><label>秘书长</label></div></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="party-members">
                          <div class="party-box other-members">
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/sdp/paul-tambyah.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>淡马亚</span><label>主席</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/sdp/john-tan-liang-joo.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>陈两裕</span><label>副主席</label></div></div>
                            </div>
                          </div>
                          <div class="party-box other-members other-mp">
                            <h4>其他中委</h4>
                            <div class="party-member-name">
                              <span>洪小平</span><span>达曼胡理</span><span>祖菲丽</span><span>江伟贤</span><span>林文兴</span><span>孙春源</span><span>陈景元</span><span>黄淑仪</span><span>杨柱良</span>
                            </div>
                          </div>
                          <div class="item">
                            <a href="mp-profile.php#party-sdp" class="mdl-button" target="_blank">候选人履历</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- PSP -->
                  <div id="party-psp" class="party-tab-detail" data-tab-content><a name="party-psp"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>新加坡前进党</span></h3>
                        <div class="blurb">79岁的陈清木医生，在执政党旗下担任议员长达26年，这次另起炉灶，成立新的反对党。曾参加总统选举的他说，原本想要从政坛引退，但他观察了我国政治体制的现状后，觉得问责制与透明度正遭受侵蚀，感到忧心。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <p class="padding-10">陈清木也指出，组建政党，就不会甘于一直当反对党。前进党一口气引进上千名党员，而且还全岛“走透透”。陈清木创党时立下目标，要把自己的党员送入国会，并与其他反对党联手，阻挠执政的人民行动党达到修宪所需的三分之二议席。不过，该党在组联盟一事上目前处于被动姿态，党领导层也频频出现变动。</p>

                      <div class="single-quote">
                        <blockquote class="mdl-cell mdl-cell--12-col">
                          <span>我只有很短的时间去栽培有心从政、为国为民的未来国会议员。我们要维护一个有同情心、真正民主、有正确价值观、以民为本的新加坡，而且也必须保持自由选择及无畏发言的精神。</span>
                          <cite>
                            <div class="party-member">
                              <div class="member-name">秘书长<b>陈清木</b></div>
                              <div class="member-img"><img src="party/imgs/psp/tan-cheng-bock.png" /></div>
                            </div>
                          </cite>
                          <div class="plate"></div>
                        </blockquote>
                      </div>
                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-psp"></div>
                              <div class="party-name"><span class="ch">新加坡前进党</span><span class="en">Progress Singapore Party</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--12-col"><label>创立年份</label><span>1980年</span></div>
                              </div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 西海岸、碧山—大巴窑、蔡厝港、丹戎巴葛、裕廊、义顺、三巴旺、惹兰勿刹<br>
                                <b>单选区</b> 先驱、丰加北、拉丁马士、裕华、哥本峇鲁、玛丽蒙、杨厝港</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-right"><img src="party/imgs/psp/tan-cheng-bock.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>陈清木</span><label>秘书长</label></div></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="party-members">
                          <div class="party-box other-members">
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/psp/leong-mun-wai.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>梁文辉</span><label>助理秘书长</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/psp/wang-swee-chuang.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>袁瑞川</span><label>主席</label></div></div>
                            </div>
                          </div>
                          <div class="party-box other-members other-mp">
                            <h4>其他中委</h4>
                            <div class="party-member-name">
                              <span>纳拉卡鲁班</span><span>潘群勤</span><span>蔡德龙</span><span>蔡佩琦</span><span>李秋胜</span><span>李荣辉</span><span>吴柄坤</span><span>王绍荣</span><span>阿都拉曼</span><span>卡纳伽星甘</span><span>陈仲熙</span><span>陈千佳</span><span>王就成</span><span>阮建平</span>
                            </div>
                          </div>
                          <div class="item">
                            <a href="mp-profile.php#party-psp" class="mdl-button" target="_blank">候选人履历</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- SPP -->
                  <div id="party-spp" class="party-tab-detail" data-tab-content><a name="party-spp"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>新加坡人民党</span></h3>
                        <div class="blurb">反对党代表人物之一詹时中卸下党秘书长职务，40多年政治生涯画上句点，他的妻子、原党主席罗文丽也让位新人。经历大换血的人民党中委，如今有不少政治新面孔，这次大选将带来全新的考验。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <p class="padding-10">新任秘书长谢镜丰已表明，该党不会加入反对党联盟。新任主席乔立盟则准备好在波东巴西区上阵，持续在该区耕耘。这意味着波东巴西区在本届大选将出现40年来首位“非詹家”的反对党候选人。</p>

                      <div class="single-quote">
                        <blockquote class="mdl-cell mdl-cell--12-col">
                          <span>人民党坚持的重要信念是，反对党不论政治理念，必须忠于新加坡和新加坡人。虽然我们重视反对党阵营的团结，但我们也理解，唯有做到各政党有共同的价值观、处事方法和理念，才能有真正的团结。</span>
                          <cite>
                            <div class="party-member">
                              <div class="member-name">秘书长<b>谢镜丰</b></div>
                              <div class="member-img"><img src="party/imgs/spp/steve-chia.png" /></div>
                            </div>
                          </cite>
                          <div class="plate"></div>
                        </blockquote>
                      </div>
                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-spp"></div>
                              <div class="party-name"><span class="ch">新加坡人民党</span><span class="en">Singapore People's Party</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--12-col"><label>创立年份</label><span>1994年</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>27.08<em>%</em></span></div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 碧山—大巴窑<br>
                                <b>单选区</b> 巴西、蒙巴登</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-right"><img src="party/imgs/spp/steve-chia.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>谢镜丰</span><label>秘书长</label></div></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="party-members">
                          <div class="party-box other-members">
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/spp/ariffin-sha.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>阿里菲</span><label>助理秘书长</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/spp/jose-raymond.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>乔立盟</span><label>主席</label></div></div>
                            </div>
                            <div class="party-member">
                              <div class="member-img align-left"><img src="party/imgs/spp/williiamson-lee.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>李健铨</span><label>副主席</label></div></div>
                            </div>
                          </div>
                          <div class="item">
                            <a href="mp-profile.php#party-spp" class="mdl-button" target="_blank">候选人履历</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- SF -->
                  <div id="party-sf" class="party-tab-detail" data-tab-content><a name="party-sf"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>国人为先党</span></h3>
                        <div class="blurb">由前总统选举参选人陈如斯成立的这个反对党，目前没有活跃党员，2015年大选后已退出公众视线。陈如斯曾解释说，这是该党刻意采取的策略，目的是让其他反对党知道，他有意组联盟。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <p class="padding-10">反对党阵营越来越拥挤，四个规模较小的反对党——国人为先党、人民力量党、革新党和民主进步党，2020年初开始筹划注册新的反对党联盟，但到了3月却改变初衷，决定加入新加坡民主联盟。不过，随着民联在6月正式表态拒绝，联盟计划也暂时告吹。</p>

                      <div class="single-quote">
                        <blockquote class="mdl-cell mdl-cell--12-col">
                          <span>由于有太多反对党，选民容易混淆，很难区分各党之间的差异。理想做法是合并，但有些党执意要保留自己的党。我愿意放弃我党的旗帜。</span>
                          <cite>
                            <div class="party-member">
                              <div class="member-name">秘书长<b>陈如斯</b></div>
                              <div class="member-img"><img src="party/imgs/sf/tan-jee-say.png" /></div>
                            </div>
                          </cite>
                          <div class="plate"></div>
                        </blockquote>
                      </div>
                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-sf"></div>
                              <div class="party-name"><span class="ch">国人为先党</span><span class="en">Singaporeans First</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--12-col"><label>创立年份</label><span>2014年</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>21.49<em>%</em></span></div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 丹戎巴葛、裕廊</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-left"><img src="party/imgs/sf/tan-jee-say.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>陈如斯</span><label>秘书长</label></div></div>
                              </div>
                            </div>
                            <div class="item">
                              <a href="mp-profile.php#party-sf" class="mdl-button" target="_blank">候选人履历</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- PPP -->
                  <div id="party-ppp" class="party-tab-detail" data-tab-content><a name="party-ppp"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>人民力量党</span></h3>
                        <div class="blurb">人民力量党在2015年大选之后，也不办对外活动，不走访任何选区。该党党魁吴明盛一直推动各反对党成立共同合作平台，也不排除人民力量党在这届大选会把所有资源和人力借出去。该党也是四个要求加入新加坡民主联盟但没有成功的反对党之一。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <div class="single-quote">
                        <blockquote class="mdl-cell mdl-cell--12-col">
                          <span>我们一向都是松散联盟，每次大选都是在桌上奋力一拍，讨论后没有后续发展，不了了之。反对党应该有共同的政策平台。</span>
                          <cite>
                            <div class="party-member">
                              <div class="member-name">秘书长<b>吴明盛</b></div>
                              <div class="member-img"><img src="party/imgs/ppp/goh-meng-seng.png" /></div>
                            </div>
                          </cite>
                          <div class="plate"></div>
                        </blockquote>
                      </div>
                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-ppp"></div>
                              <div class="party-name"><span class="ch">人民力量党</span><span class="en">People's Power Party</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--12-col"><label>创立年份</label><span>2015年</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>23.11<em>%</em></span></div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 蔡厝港</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-right"><img src="party/imgs/ppp/goh-meng-seng.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>吴明盛</span><label>秘书长</label></div></div>
                              </div>
                            </div>
                            <div class="item">
                              <a href="mp-profile.php#party-ppp" class="mdl-button" target="_blank">候选人履历</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- PV -->
                  <div id="party-pv" class="party-tab-detail" data-tab-content><a name="party-pv"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>人民之声</span></h3>
                        <div class="blurb">国民团结党前秘书长林鼎退党后自立的政党。该党不设秘书长或主席职位。党领导林鼎近来不时在个人面簿页面上，对执政党政府2019冠病病毒疫情的处理，提出批评。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <p class="padding-10">林鼎因拖欠商人欠款面对民事诉讼。若宣告破产，他将无法角逐本届全国大选，可能影响该党的前景。</p>

                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-pv"></div>
                              <div class="party-name"><span class="ch">人民之声</span><span class="en">People’s Voice</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--12-col"><label>创立年份</label><span>2018年</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>23.11<em>%</em></span></div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 蔡厝港</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-right"><img src="party/imgs/pv/lim-tean.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>林鼎</span><label>党领导</label></div></div>
                              </div>
                            </div>
                            <div class="item">
                              <a href="mp-profile.php#party-pv" class="mdl-button" target="_blank">候选人履历</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- NSP -->
                  <div id="party-nsp" class="party-tab-detail" data-tab-content><a name="party-nsp"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>国民团结党</span></h3>
                        <div class="blurb">上届大选后，团结党数次换秘书长，一度陷入领导层青危机。曾担任过党秘书长的林鼎、吴明盛和潘群勤都已退党。林鼎和吴明盛过后自组政党；潘群勤则已加入陈清木领导的前进党。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <p class="padding-10">在团结党2016年中委选举中，担任主席超过10年的原主席张培源表明让贤。该党现由全新团队领导，专注于走访过去几届大选竞逐过的淡滨尼集选区。根据团结党党章，主席位居秘书长之上。</p>

                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-nsp"></div>
                              <div class="party-name"><span class="ch">国民团结党</span><span class="en">National Solidarity Party</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--12-col"><label>创立年份</label><span>1987年</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>25.27<em>%</em></span></div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 淡滨尼、三巴旺<br/>
                                <b>单选区</b> 麦波申、先驱</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-left"><img src="party/imgs/nsp/reno-fong.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>冯展良</span><label>主席</label></div></div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="party-members">
                          <div class="party-box other-members">
                            <div class="party-member">
                              <div class="member-img align-right"><img src="party/imgs/nsp/spencer-ng.png" /></div>
                              <div class="party-member-label"><div class="member-name"><span>黄俊宏</span><label>秘书长</label></div></div>
                            </div>
                          </div>
                          <div class="item">
                            <a href="mp-profile.php#party-nsp" class="mdl-button" target="_blank">候选人履历</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- SDA -->
                  <div id="party-sda" class="party-tab-detail" data-tab-content><a name="party-sda"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>新加坡民主联盟</span></h3>
                        <div class="blurb">新加坡民主联盟由反对党元老、新加坡人民党前党魁詹时中在2001年成立，是我国自独立以来的首个政党联盟。当年的反对党大联盟，如今剩下新加坡正义党和马来民族机构。不过，对于国人为先党、人民力量党、革新党和民主进步党的加盟申请，民联已表态拒绝。民联主席林睦荃另外透过自创慈善组织，为年长者和弱势家庭提供免费午餐盒，也举办活动，以此方式备战大选。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-sda"></div>
                              <div class="party-name"><span class="ch">新加坡民主联盟</span><span class="en">Singapore Democratic Alliance</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--12-col"><label>创立年份</label><span>2001年</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>27.11<em>%</em></span></div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 白沙—榜鹅</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-left"><img src="party/imgs/sda/desmond-lim.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>林睦荃</span><label>主席</label></div></div>
                              </div>
                            </div>
                            <div class="item">
                              <a href="mp-profile.php" class="mdl-button" target="_blank">候选人履历</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- TRP -->
                  <div id="party-trp" class="party-tab-detail" data-tab-content><a name="party-trp"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>革新党</span></h3>
                        <div class="blurb">工人党前秘书长惹耶勒南被判入穷籍数年后，成立的政党。目前由长子肯尼斯领导。不过，肯尼斯自2015年大选后，长居海外，目前不清楚他是否参加本届大选。目前主要代该党发言的是主席朱来成。该党也是四个要求加入新加坡民主联盟但没有成功的反对党之一。
</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-trp"></div>
                              <div class="party-name"><span class="ch">革新党</span><span class="en">The Reform Party</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--12-col"><label>创立年份</label><span>2008年</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>20.60<em>%</em></span></div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 宏茂桥、西海岸<br/>
                                <b>单选区</b> 拉丁马士</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-right"><img src="party/imgs/trp/kenneth-jeyaretnam.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>肯尼斯</span><label>秘书长</label></div></div>
                              </div>
                            </div>
                            <div class="item">
                              <a href="mp-profile.php#party-sda" class="mdl-button" target="_blank">候选人履历</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- DPP -->
                  <div id="party-dpp" class="party-tab-detail" data-tab-content><a name="party-dpp"></a>
                    <div class="jumbotron">
                      <div class="jumbotron-content">
                        <h3 class="header-title"><span>民主进步党</span></h3>
                        <div class="blurb">1970年代脱离工人党者成立的政党，2015年大选时由方月光领导，该党当时与人民党合作，组队攻战碧山—大巴窑集选区，但无功而返。方月光目前已退党并改加入民主党。该党也是四个要求加入新加坡民主联盟但没有成功的反对党之一。</div>
                      </div>
                    </div>
                    <div class="content-section">
                      <div class="party-group-profile">
                        <h3 class="header-title mdl-typography--text-center">关键人物</h3>
                        <div class="party-details">
                          <div class="party-group-info">
                            <div class="party-profile">
                              <div class="party-logo party-dpp"></div>
                              <div class="party-name"><span class="ch">民主进步党</span><span class="en">Democratic Progressive Party</span></div>
                            </div>
                            <div class="party-background">
                              <div class="mdl-grid">
                                <div class="setup-year mdl-cell mdl-cell--12-col"><label>创立年份</label><span>1973年</span></div>
                              </div>
                              <div class="election-vote-percent"><label>上届大选参选议席得票率</label><span>26.41<em>%</em> <i>*</i></span><small><i>*</i> 在人民党旗帜下竞选</small></div>
                              <div class="active-district"><label>竞逐过/活跃的选区</label><span><b>集选区</b> 碧山—大巴窑</span></div>
                            </div>
                          </div>
                          <div class="party-member-info">
                            <div class="party-box">
                              <div class="party-leader">
                                <div class="party-grc-leader"></div>
                                <div class="member-img align-left"><img src="party/imgs/dpp/mohamad-hamim-aliyas.png" /></div>
                                <div class="party-member-label"><div class="member-name"><span>莫哈默哈敏</span><label>秘书长</label></div></div>
                              </div>
                            </div>
                            <div class="item">
                              <a href="mp-profile.php#party-dpp" class="mdl-button" target="_blank">候选人履历</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>

    </div>

  </div>
</div>
<?php include "assets/inc/news-feed.html"?>
<?php include "assets/inc/footer.html"?>
<?php include "assets/inc/scripts.html"?>

</body>
</html>
