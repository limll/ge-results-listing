
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"> 
<meta charset="utf-8">

<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
dataLayer.push({
    "level2": "INTERACTIVE-NAME",
    "title": "HEADLINE TITLE",
});
</script>
<!-- Google Tag Manager
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVFMLMK');</script> -->
<!-- End Google Tag Manager -->

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="index,follow">
<meta name="GOOGLEBOT" content="index,follow">
<meta http-equiv="cache-control" content="max-age=0, no-cache, no-store, must-revalidate" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="author" content="" />

<title>政见比一比</title>
<meta name="description" content="SOMETHING SHORT AND BRIEF ENOUGH TO DESCRIBE THE TOPIC" />
<meta name="keywords" content="早报, KEYWORDS HIGHLY RELEVANT TO THE TOPIC, NAMES ARE IMPORTANT TOO" />
<link rel="image_src" href="//interactive.zaobao.com/YYYY/FOLDER-NAME/IMGS/SOCIAL-SHARE-IMG" />

<?php include "assets/inc/global.html" ?>
</head>

<body id="page-party-strength">
<?php include "assets/inc/gtm.html" ?>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

<?php include "assets/inc/brand.html" ?>

  <div class="content mdl-layout__content">
    <a name="top"></a>

    <div class="tab-wrap no-swiper">
      <div class="sticker000">
        <nav class="tab-navigation content-section">
          <ul class="links-container">
            <li class="nav-link"><a href="party.php">政党介绍</a></li>
            <li class="nav-link active"><a href="party-strength.php">政见比一比</a></li>
          </ul>
        </nav>
      </div>

      <div class="tab-container">
        <div class="000tab-container-inner">
          <div class="000swiper-wrapper">
            <div class="000swiper-slide">
              <div class="content-section full-width">
                <p class="padding-10">自上届大选以来，各政党已在哪些新议题上过招？每个政党将竞选焦点放在哪个群体上？疫情期举行大选，虽然少了竞选集会作为各政党展现政见的“擂台”，但他们在不同课题上的主张和立场相信并不会因此失声。</p>
              </div>

              <div class="000mdl-grid">
                <nav class="stick-nav">
                  <a class="stick-nav-link" href="#retirement-plans">生活费与<br class="view-mobile">退休保障</a>
                  <a class="stick-nav-link" href="#politics">政治制衡</a>
                  <a class="stick-nav-link" href="#infrastructure">基础建设</a>
                  <a class="stick-nav-link" href="#population-foreign-worker">人口与<br class="view-mobile">外籍劳工</a>
                  <a class="stick-nav-link" href="#other-topics">其他<br class="view-mobile">热点议题</a>
                </nav>

                <div class="segment" id="retirement-plans">
                  <h2>生活费与退休保障</h2>
                  <div id="sticky">
                    <ul class="nav">
                      <li><a href="#gst">消费税调高</a></li>
                      <li><a href="#water-price">水价上涨</a></li>
                      <li><a href="#worker-insurance">冗员保险</a></li>
                      <li><a href="#cpf">公积金制度设计、<br class="view-mobile">退休与医疗保障</a></li>
                    </ul>
                  </div>

                  <div class="segment-box landscape" id="gst"><a name="gst"></a>
                    <h3>消费税调高</h3>

                    <div class="compare-box scroll"><div class="compare-list dragme">
                      <div class="compare-item" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>原计划在2021年至2025年间，把消费税调高两个百分点至9%，以满足日益增大的公共开支，但考虑到冠病疫情，已宣布2021年不会调高。</p>
                          <p><b>一再强调</b>，虽然明知调高消费税不受欢迎，但提早宣布是诚实的做法。</p>
                          <p>消费税的调高整体是公平且累进式的，政府也会给弱势群体更多补贴。</p>
                          <a href="//www.zaobao.com/special/report/singapore/budget2020/story20200219-1030153" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p><b>调高</b>消费税将影响国人的财务情况，建议考虑其他收入来源，包括调高国家储备金净投资回报贡献（NIRC）顶限，以及使用售卖土地所得收益的一小部分。</p>
                          <p><b>认为</b>政府没有清楚说明长期收入与支出规划，没有充分考虑其他收入来源，以及能否动用国家储备金等。由于认为缺乏信息，各方无法针对消费税调涨展开更成熟的辩论，该党虽然支持政府2018财政年的财政政策整体策略与措施，但还是因系列宣布也涵盖消费税的调涨，投下反对票。</p>
                          <a href="//www.zaobao.com/special/report/singapore/budget2018/news/story20180227-838527" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>废除消费税制是竞选重点之一。<b>认为</b>消费税是极为累退的税务，对低收入人士影响最大。认为日常用品、医疗服务，以及学习用品，不应征收消费税，奢侈品消费税应上调至10%或更高。</p>
                          <p>首1%最高收入阶层的所得税应调高至28%，以增加政府收入。</p>
                          <p><b>不满意</b>政府只承诺消费税2021年不会调涨。一再强调这意味着消费税会在大选之后，即2022年，马上调高。</p>
                        </div>
                      </div>
                      <div class="compare-item" id="psp">
                        <div class="party-profile">
                          <div class="party-logo party-psp"></div>
                          <div class="party-name"><span class="ch">新加坡前进党</span><span class="en">Progress Singapore Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>消费税<b>未来五年不该调高</b>。我国财政实力稳固，足以让新加坡推行可持续的长期政策，从根本上解决生活费、房价和医疗费上涨的课题，无需调高消费税。</p>
                          <p>政府应更有效地利用国家储备金净投资回报贡献（NIRC）实现经济转型。</p>
                        </div>
                      </div>
                      <div class="compare-item" id="ppp">
                        <div class="party-profile">
                          <div class="party-logo party-ppp"></div>
                          <div class="party-name"><span class="ch">人民力量党</span><span class="en">People's Power Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p><b>将废除</b>消费税制设为竞选重点。怀疑2019财政年政府预算案呈现的赤字，是为上调消费税铺路。</p>
                          <a href="//www.zaobao.com/news/singapore/story20190225-934669" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                    </div></div>
                  </div>

                  <div class="segment-box" id="water-price"><a name="water-price"></a>
                    <h3>水价上涨</h3>
                    <div class="compare-list">
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>分两次把水价调高30%，主要考虑上一轮调整水价是2000年，已距离一段时间。</p>
                          <p>既然生产水的成本增加，水价就必须相应提高。</p>
                          <p>许多家庭也获得额外水电费回扣，一房式和两房式组屋家庭的水费没有净增。在疫情期间也推出关怀与援助配套，帮助家庭应付开销。</p>
                          <a href="//www.zaobao.com/special/report/singapore/budget2017/news/story20170221-727107" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>政府应该更密切关注生活费过高的问题，不该毫无预兆调高水价。</p>
                          <p>以水费上调为例，政府曾安抚说，每立方米的水价增幅只有59分，如果咖啡店真的起价，以咖啡中的饮用水成分来计算，价格不该增加超过1分钱，但事实是，一些咖啡店因经营成本上涨而还是调高咖啡价格。这显示政府不体恤民情。</p>
                          <a href="//www.zaobao.com/news/singapore/story20170301-730275" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>质疑水价上涨的时间点，认为在人民面对经济低迷时刻上调水费不近人情，是刻意占国人便宜。</p>
                          <p>该党主席淡马亚医生在芳林公园一场抗议集会上，批评政府在相关信息传达与沟通方面，也做得不好。这显示政府与民意脱节。</p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="segment-box" id="worker-insurance"><a name="worker-insurance"></a>
                    <h3>冗员保险</h3>
                    <div class="compare-list">
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>推行冗员保险会给企业带来更高的成本负担，也可能导致员工形成虚假的安全感。应着重提供就业援助，帮助失业工友重返职场。在疫情期为受影响而失业或收入下跌的员工，提供一次性500元的短期援助基金。</p>
                          <a href="//www.zaobao.com/news/singapore/story20170509-757897" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>2017年在国会提出休会动议，希望政府考虑推行冗员保险，为无法获得裁员福利的工人提供经济保障。</p>
                          <p>根据计划，雇主和员工每月须为就业保障基金缴交月薪的0.05%；自雇人士缴交0.1%；员工一旦失业，每月可获相等于薪水40%的补助金，以半年为限。</p>
                          <p>在疫情时期，也提出应检讨是否为必要服务工人提供“最低生活工资”。</p>
                          <a href="//www.zaobao.com/news/singapore/story20200408-1043740" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>支持推行冗员保险，为被裁退的工人提供长达一年半的失业保险，主张失业首六个月可领取原来薪水四分之三的保险金，半年后递减为一半，最后半年则为25%，金额顶限是国人收入的中位数。</p>
                          <p>支持设最低薪金。</p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="segment-box" id="cpf"><a name="cpf"></a>
                    <h3>公积金制度设计<br class="view-mobile">退休与医疗保障</h3>
                    <div class="compare-list">
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>认为应平衡年长者的退休需求和受雇能力。接受了年长员工劳资政工作小组的方案，下来十年逐步调高退休和重新雇佣年龄，以及年长员工的公积金缴交率。</p>
                          <p>法定退休年龄和重新雇佣年龄不与公积金会员领取每月退休入息的年龄挂钩，会员仍在年满65岁时选择开始领取入息。</p>
                          <p>不认同废除法定退休年龄，因为这意味着雇主可任意解雇员工，不再考虑他们是否已达退休年龄。</p>
                          <p>继推出建国一代配套后，也为立国一代提供更多医药保障，让年长者能享有各类津贴。</p>
                          <a href="//www.zaobao.com/sme/news/story20190306-937313" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>呼吁废除法定退休年龄，把重新雇佣年龄提高至70岁，并调高年长员工的公积金缴交率。</p>
                          <p>建议把公积金的可领取入息年龄调低至60岁，让那些因心理或生理因素，有一段时间无法工作的国人，选择领取部分公积金储蓄应急，直到能重回职场。</p>
                          <p>认为政府应让年满60岁的国人自动享有一系列基本医疗津贴，推行永久性乐龄医疗护理配套。</p>
                          <a href="//www.zaobao.com/sme/news/story20190819-981970" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>只要是年满65岁、不属于首20%高收入阶层的新加坡公民，都应每月获一笔500元的基本退休收入。</p>
                          <p>主张全国医疗投资基金的全民医保概念，认为应废除保健储蓄、终身健保和保健基金等计划，改用统一的保险计划。每名国人只须用50元的公积金储蓄支付保金，就可支付各种医药费，其余医药费由国人缴付的税务等其他政府收入应付。</p>
                          <p>国人一旦入院，只须承担一成的住院费，每年顶限为2000元，其他由基金买单。</p>
                          <p>建议取消公积金退休存款计划，允许满55岁的国人提取所有公积金存款，不提出存款的国人能以“选择加入”的方式，将存款留在户头里。</p>
                          <a href="//www.zaobao.com/finance/singapore/story20191103-1000976" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="segment hero" id="politics">
                  <h2>政治制衡</h2>
                  <div id="sticky">
                    <ul class="nav">
                      <li><a href="#parliament-structure">国会分权与<br class="view-mobile">政治制衡</a></li>
                      <li><a href="#town-council">市镇管理</a></li>
                      <li><a href="#presidential-election">民选总统制度修改</a></li>
                      <li><a href="#non-constituency">非选区议员制</a></li>
                      <li><a href="#opposition-party">反对党团结</a></li>
                    </ul>
                  </div>
                  <div class="segment-box" id="parliament-structure"><a name="parliament-structure"></a>
                    <h3>国会分权与政治制衡</h3>
                    <div class="compare-list">
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>认为国会里朝野议席数量是否有正确平衡不是关键，最重要是政治体制能够良好运作，行之有效。</p>
                          <a href="//www.zaobao.com/realtime/singapore/story20200119-1022419" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>国会里朝野议席数量有正确平衡绝对重要，反对党如今作为国会少数，最重要的角色是确保执政党手上没有让他们为所欲为的空头支票，确保政府对人民的脉动和福利保持敏感，推行政治开放，催生一种不同类型的政治。</p>
                          <a href="//www.zaobao.com/znews/singapore/story20180517-859579" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="spp">
                        <div class="party-profile">
                          <div class="party-logo party-spp"></div>
                          <div class="party-name"><span class="ch">新加坡前进党</span><span class="en">Progress Singapore Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>建议将投票年龄从21岁降低到18岁，给予更多年轻人权力，选择由谁来领导国家。这也会确保我国的选举投票年龄与东南亚其他国家划一。</p>
                          <a href="//www.zaobao.com/znews/singapore/story20190805-978545" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="segment-box" id="town-council"><a name="town-council"></a>
                    <h3>市镇管理</h3>
                    <div class="compare-list">
                      <div class="compare-item mdl-cell mdl-cell--6-col mdl-cell--12-col-phone" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>在国会提出诚信动议，重申议员应维持高标准的诚信和责任感，呼吁出现财务疏失的工人党阿裕尼—后港市镇会履行对居民的责任。要求工人党坦承它在管理市镇理事会上的过失，安排两名工人党议员刘程强和林瑞莲回避市镇会财政事务。</p>
                          <a href="//www.zaobao.com/znews/singapore/story20191106-1002962" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--6-col mdl-cell--12-col-phone" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>针对诚信动议在国会投下反对票，以有意上诉为由拒绝动议，并质疑行动党迫不及待提出动议的动机。市镇会之后表决，通过刘程强和林瑞莲无须回避市镇会财政事务，但由于国家发展部发出指令，市镇会最终还是限制了两人在特定财务决策方面的权限。</p>
                          <p>认为身为负责任的反对党市镇会理事不仅要有诚信，也必须了解所管理的市镇会跟行动党的市镇会不同，要明白政治环境的现实。</p>
                          <a href="//www.zaobao.com/realtime/singapore/story20191105-1002915" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="segment-box" id="presidential-election"><a name="presidential-election"></a>
                    <h3>民选总统制度修改</h3>
                    <div class="compare-list">
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>民选总统制须与时俱进，同时让每个族群都有机会成为总统，使总统能在真正意义上，成为代表所有新加坡人、维持国家团结的国家元首。</p>
                          <p>民选总统制协助政府保障储备金，是有效的监管机制，可加强我国政治体制的稳定性，不应该被全盘否定。</p>
                          <a href="//www.zaobao.com/news/singapore/story20160509-614638" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>一向主张废除该制度。认为政府不应急于落实民选总统保留机制，而是应该举行全国公投，由国人决定如何选出国家元首。</p>
                          <p>倡议由一个民选的参议院，接手总统原有的监管权限，让总统只须扮演团结国人的象征角色。</p>
                          <p>曾在国会提出休会动议，质疑民选总统保留选举从第四位总统黄金辉的任期开始算起的做法。</p>
                          <a href="//www.zaobao.com/news/singapore/story20161109-687729" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="spp">
                        <div class="party-profile">
                          <div class="party-logo party-spp"></div>
                          <div class="party-name"><span class="ch">新加坡前进党</span><span class="en">Progress Singapore Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>党魁陈清木曾入禀高庭，质疑国会启动总统保留选举不合法，指黄金辉不是通过民主选举产生的总统，总统保留机制应从首个民选总统王鼎昌算起，但上诉失败。</p>
                          <p>陈清木在2011年曾竞逐民选总统，但以微差败给陈庆炎博士。</p>
                          <p>陈清木在加入反对党行列后，批评民选总统制的改革，认为通过修宪法案，使机制牵涉到种族，对我国政治产生非常严重的影响。</p>
                          <a href="//www.zaobao.com/news/singapore/story20170509-757861" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="segment-box" id="non-constituency"><a name="non-constituency"></a>
                    <h3>非选区议员制</h3>

                    <div class="compare-box scroll"><div class="compare-list dragme">
                      <div class="compare-item" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>修改宪法，非选区议员人数上限从九人增加到12人。非选区议员与当选议员一样，享有相同投票权，以确保国会有一定的反对声音。</p>
                        </div>
                      </div>
                      <div class="compare-item" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>反对非选区议员制。行动党这么做是利用制度保住自己在国会的绝大多数，这将把新加坡逼入无止境依赖同一政党的循环。</p>
                          <p>非选区议员即便享有投票权，也没有选民或选区作为政治根基，无法同当选议员相提并论。</p>
                          <a href="//www.zaobao.com/news/news/singapore/story20160128-575794" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>修改非选区议员制度只会分散人民的注意力，要真正改革政治制度，应废除集选区制度。</p>
                        </div>
                      </div>
                      <div class="compare-item" id="sda">
                        <div class="party-profile">
                          <div class="party-logo party-sda"></div>
                          <div class="party-name"><span class="ch">新加坡民主联盟</span><span class="en">Singapore Democratic Alliance</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>增加非选区议员议席无法改变政治制度的根本问题，选民如果认定即便反对党候选人不中选，还有非选区议员在国会发言，将使反对党更难中选。</p>
                        </div>
                      </div>
                    </div></div>
                  </div>
                  <div class="segment-box" id="opposition-party"><a name="opposition-party"></a>
                    <h3>反对党团结</h3>

                    <div class="compare-box scroll"><div class="compare-list dragme">
                      <div class="compare-item" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>不加入任何联盟，认为如果反对党能够团结，今天就不会这么多的反对党。不与其他反对党磋商，是避免结盟后反对党阵营最后落得四分五裂，拖慢民主进程。</p>
                        </div>
                      </div>
                      <div class="compare-item" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>有意与反对党合作组联盟，并一直呼吁工人党一同参与讨论。</p>
                          <a href="//www.zaobao.com/special/report/singapore/singapore-13th-ge/news-and-updates/story20190805-978277" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="psp">
                        <div class="party-profile">
                          <div class="party-logo party-psp"></div>
                          <div class="party-name"><span class="ch">新加坡前进党</span><span class="en">Progress Singapore Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>党魁陈清木医生在政党成立后，召集反对党人士开会，以领导姿态探讨如何展现反对党阵营的团结，但之后迟迟没有组织联盟的动作，而且在多个选区插旗。</p>
                          <a href="//www.zaobao.com/znews/singapore/story20191102-1002102" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="spp">
                        <div class="party-profile">
                          <div class="party-logo party-spp"></div>
                          <div class="party-name"><span class="ch">新加坡人民党</span><span class="en">Singapore People's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>无意加入联盟，是最早拒绝加入反对党联盟的政党之一。</p>
                        </div>
                      </div>
                      <div class="compare-item" id="ppp">
                        <div class="party-profile">
                          <div class="party-logo party-ppp"></div>
                          <div class="party-name"><span class="ch">人民力量党</span><span class="en">People's Power Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>不看好反对党组松散联盟的说法，认为反对党必须组织起来，有共同的政策平台，才能形成一股势力。已与另一些反对党集合起来，申请加入民主联盟。</p>
                          <a href="//www.zaobao.com/znews/singapore/story20200104-1018214" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                    </div></div>
                  </div>
                </div>

                <div class="segment" id="infrastructure">
                  <h2>基础建设</h2>
                  <div id="sticky">
                    <ul class="nav">
                      <li><a href="#climate-change">气候变化应对</a></li>
                      <li><a href="#lodging">住房</a></li>
                    </ul>
                  </div>

                  <div class="segment-box" id="climate-change"><a name="climate-change"></a>
                    <h3>气候变化应对</h3>
                    <div class="compare-list">
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>要让新加坡成为本区域及沿海城市气候行动的领导者。以一系列对抗气候变化的长期策略，显示行动党决心正视这个问题，回应以年轻一代为主的提倡环保声浪。</p>
                          <p>青年团也积极与环境议题研究员与非政府组织代表联系，草拟了一份详细的政策书。建议碳税应增加20倍至每公吨100元，并且主张追求可与经济目标吻合的可持续发展议程。</p>
                          <a href="//www.zaobao.com/znews/singapore/story20191021-998914" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>在本地年轻人举办的首场气候集会结束后，该党阐述其议员如何在国会为气候变化议题发声。</p>
                          <p>呼吁政府针对持续增加再生能源制造比率设长远目标，更透明地发表我国碳排放量相关数据，也认为本地主权财富基金应更积极公开他们投资在可持续环境保护上的数额。</p>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>民主党青年团草拟了详细的政策书，与工人党一样主张主权财富基金公开他们的投资项目。</p>
                          <p>认为可持续能源使用率太低，新加坡经济应减少对石油业的依赖，采取大刀阔斧的环境相关改革，即便这意味着要牺牲经济利益。但认为暂时不应调高碳税，除非能找到办法阻止企业将税务成本转嫁给消费者。</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="segment-box" id="lodging"><a name="lodging"></a>
                    <h3>住房</h3>

                    <div class="compare-box scroll"><div class="compare-list dragme">
                      <div class="compare-item" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>政府过去五年来共拨款30亿元进行翻新计划，也让年长屋主将组屋部分剩余屋契卖回给建屋局，让他们能获得每月固定入息。它将制定长期计划，确保国人所居住的组屋维持良好品质，包括透过自愿提早重建计划（VERS），逐步重新发展较旧的组屋市镇。</p>
                          <a href="//www.zaobao.com/realtime/singapore/story20191216-1013692" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>在政策建议书中批评VERS存在诸多不确定性，如政府要如何资助VERS的赔偿金、多数组屋会否获选参与VERS，以及一些VERS项目可能因投票不通过而告吹。</p>
                          <p>认为应为VERS寻找替代方案，也建议扩大屋契回购计划，允许所有剩余屋契不超过30年的屋主，将单位卖回给政府。</p>
                          <a href="//www.zaobao.com/news/singapore/story20191203-1010127" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>在政策建议书中提出非公开市场组屋的概念，即推出一批不允许在公开市场进行买卖的组屋单位，以不包括土地价格的成本价出售部分组屋，降低组屋的售价，让国人能有更多退休存款。民主党认为这将解决组屋市场价值随着屋龄减少的问题。</p>
                          <a href="//www.zaobao.com/news/singapore/story20150829-519973" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="sf">
                        <div class="party-profile">
                          <div class="party-logo party-sf"></div>
                          <div class="party-name"><span class="ch">国人为先党</span><span class="en">Singaporeans First</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>党魁陈如斯认为应以不包括土地价格的成本价出售组屋，并认为现有的选择性整体重建计划（SERS）应继续。</p>
                          <a href="//www.zaobao.com/news/singapore/story20160412-603959" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                    </div></div>
                  </div>
                </div>

                <div class="segment hero" id="population-foreign-worker">
                  <h2>人口与外籍劳工</h2>
                  <div class="segment-box" id="population-foreign-worker"><a name="population-foreign-worker"></a>
                    <div class="compare-list">
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>政府透过劳资政公平雇佣指导原则，加强对违例雇主的惩罚，以消除职场歧视，确保新加坡籍员工获公平对待，打造一个以国人为核心的劳动队伍。</p>
                          <a href="//www.zaobao.com/znews/singapore/story20200115-1021169" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>提出“以新加坡人为先”的政策，建议以积分制度对外籍员工进行遴选与淘汰，确保只有达到学历、技术与经验标准的外籍人士能获聘，加入本地劳动队伍。认为这么做能保护新加坡籍白领员工，避免他们被裁员。</p>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="psp">
                        <div class="party-profile">
                          <div class="party-logo party-psp"></div>
                          <div class="party-name"><span class="ch">新加坡前进党</span><span class="en">Progress Singapore Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>争取新加坡人优先获得聘雇，要求政府给予更多信息，让国人了解各种与他国的经济协定，如何保障新加坡人的就业利益。</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="segment" id="other-topics">
                  <h2>其他热点议题</h2>
                  <div id="sticky">
                    <ul class="nav">
                      <li><a href="#pofma">网络假信息防范与POFMA</a></li>
                      <li><a href="#mp-salary">部长薪金</a></li>
                      <li><a href="#lgbt">同性议题</a></li>
                    </ul>
                  </div>
                  <div class="segment-box" id="pofma"><a name="pofma"></a>
                    <h3>网络假信息防范<br class="view-mobile">与POFMA</h3>

                    <div class="compare-box scroll"><div class="compare-list dragme">
                      <div class="compare-item" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>防止网络假信息和网络操纵法令（<a href="//www.zaobao.com/keywords/pofma" target="_blank">POFMA</a>）是一道“真相之光”，把假信息都照出来，谎言将昭然若揭，人们能自行判断真假。</p>
                          <p>认为判断哪些信息应更正或撤下的权力应赋予部长而非法庭，以确保行政速度。法案对政府的权限定义狭窄，任何人若不服政府的撤销或更正指示，可以寻求司法途径上诉，安排是透明的。</p>
                          <a href="//www.zaobao.com/realtime/singapore/story20190401-945007" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>POFMA将造成寒蝉效应，抑制言论自由。基于认为政府被赋予太多权力，所有工人党议员在国会对相关法案投下反对票。</p>
                          <a href="//www.zaobao.com/zvideos/news/story20190508-954670" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>民主党被人力部援引POFMA后，挑战政府的判断，坚持自己的面簿贴文和网络内容全部属实和正确。</p>
                          <p>民主党认为政府使用POFMA对付政治对手，为达到政治利益，阻拦“合法的批评”。该党要求人力部长杨莉明撤销更正指示，也要求她公开道歉，随后入禀法庭，申请撤销人力部发出的更正指示，但申请最终被高庭法官驳回。</p>
                          <a href="//www.zaobao.com/realtime/singapore/story20200113-1020869" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="psp">
                        <div class="party-profile">
                          <div class="party-logo party-psp"></div>
                          <div class="party-name"><span class="ch">新加坡前进党</span><span class="en">Progress Singapore Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>前进党其中一名成员被援引POFMA后，遵守了指示。但该党秘书长陈清木医生也发表对法令的立场，提出援引POFMA更正与撤销指示的权力不该赋予部长，而应由法庭判断，以确保独立性与透明。</p>
                          <a href="//www.zaobao.com/znews/singapore/story20191126-1008236" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="pv">
                        <div class="party-profile">
                          <div class="party-logo party-pv"></div>
                          <div class="party-name"><span class="ch">人民之声</span><span class="en">Peoples Voice</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>党领导林鼎被政府援引POFMA后，按照指示贴上更正。但他也批评POFMA被用来对付反对党人，并多次质疑政府发出的其他更正指示。</p>
                          <a href="//www.zaobao.com/realtime/singapore/story20191216-1013674" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                    </div></div>
                  </div>
                  <div class="segment-box" id="mp-salary"><a name="mp-salary"></a>
                    <h3>部长薪金</h3>

                    <div class="compare-box scroll"><div class="compare-list dragme">
                      <div class="compare-item" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>部长薪金框架根据三个原则制定，即具竞争力、反映政府公共服务的精神，以及薪金制度的透明，没有隐藏好处的“裸薪”。</p>
                          <p>目前的部长薪金完全透明，初级部长一般年薪在110万元左右，其中，花红由个人表现、国内生产总值和失业率等指标决定。薪金配套不包含其他隐藏收入或好处。</p>
                          <p>认为课题容易被政治化，但会正面应对。我国必须拥有一套公平透明的框架，才能持续吸引人才加入。</p>
                          <a href="//www.zaobao.com/zvideos/news/story20181001-895616" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>曾在2012年部长薪金白皮书辩论中以另一个计算方式计算出部长薪水总额，但被政府认为计算方案与现有框架相差不远。2018年，部长薪金讨论在网络上发酵，该党在国会提出询问，要求政府公布过去五年部长获得的花红，交代个别花红的份额与实际数额。</p>
                          <a href="//www.zaobao.com/news/singapore/story20181002-895652" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>提议削减部长薪金，并提出一套方程式。若按照方程式，总理220万元的年薪，将会削减至67万元，而整个内阁减薪后一年估计可省下1000万元至1200万元。</p>
                          <p>认为省下来的钱可用于资助弱势群体。</p>
                          <a href="//www.zaobao.com/news/singapore/story20190318-940803" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item" id="pv">
                        <div class="party-profile">
                          <div class="party-logo party-pv"></div>
                          <div class="party-name"><span class="ch">人民之声</span><span class="en">Peoples Voice</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>提议削减部长薪金，认为内阁一年的薪金花费应大幅减少，限制在900万元以内。</p>
                        </div>
                      </div>
                    </div></div>
                  </div>
                  <div class="segment-box" id="lgbt"><a name="lgbt"></a>
                    <h3>同性议题</h3>

                    <div class="compare-box"><div class="compare-list">
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="pap">
                        <div class="party-profile">
                          <div class="party-logo party-pap"></div>
                          <div class="party-name"><span class="ch">人民行动党</span><span class="en">People's Action Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>同性性行为合法化问题牵涉到社会价值观，社会对同性问题依然存在巨大分歧，多数国人不支持撤销我国刑事法典<a href="//www.zaobao.com/keywords/section-377A-penal-code" target="_blank">第377A节条文</a>，条文去留应交由社会做决定。</p>
                          <p>新加坡是开放的社会，无论是什么性取向，都欢迎来新加坡工作，而第377A条文保留在法律中，并没有阻碍人们的生活。</p>
                          <a href="//www.zaobao.com/znews/singapore/story20180908-889374" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="wp">
                        <div class="party-profile">
                          <div class="party-logo party-wp"></div>
                          <div class="party-name"><span class="ch">工人党</span><span class="en">Worker's Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>不要求废除刑事法典第377A条文，也不认同将课题“政治化”以换取支持。</p>
                          <p>强调这是个复杂的课题，保守派和自由派都持非常强烈的立场，党内也未就此课题达成共识。</p>
                          <a href="//www.zaobao.com/news/singapore/story20190406-946238" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                      <div class="compare-item mdl-cell mdl-cell--4-col mdl-cell--12-col-phone" id="sdp">
                        <div class="party-profile">
                          <div class="party-logo party-sdp"></div>
                          <div class="party-name"><span class="ch">新加坡民主党</span><span class="en">Singapore Democratic Party</span></div>
                        </div>
                        <div class="compare-desc">
                          <p>多年前曾经公开支持废除刑事法典第377A条文，但近年来似乎已改变立场，对此议题保留意见，立场模糊。该党有党员在上届大选中曾表示反对撤销第377A条文。</p>
                          <a href="//www.zaobao.com/" target="_blank" class="btn-link">报道</a>
                        </div>
                      </div>
                    </div></div>
                  </div>
                </div>
              </div>



            </div>
            
          </div>
        </div>
      </div>

    </div>

  </div>
</div>
<?php include "assets/inc/news-feed.html"?>
<?php include "assets/inc/footer.html"?>
<?php include "assets/inc/scripts.html"?>

<script>
$(document).ready(function() {
    $('a[href*=#]').bind('click', function(e) {
        e.preventDefault();
        var target = $(this).attr("href");
        $('html, body').stop().animate({
            scrollTop: $(target).offset().top
        }, 600, function() {
            location.hash = target;
        });
        return false;
    });
});

$(window).scroll(function() {
    var scrollDistance = $(window).scrollTop();
    $('.segment').each(function(i) {
        if ($(this).position().top <= scrollDistance) {
            $('.stick-nav a.active').removeClass('active');
            $('.stick-nav a.stick-nav-link').eq(i).addClass('active');
        }
    });
}).scroll();


// Drag and Scroll
var compare_list = $('.dragme');

compare_list.each((index)=>{
    const slider = $('.dragme')[index];

    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener('mousedown', (e) => {
        isDown = true;
        slider.classList.add('active');
        startX = e.pageX - slider.offsetLeft;
        scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener('mouseleave', () => {
        isDown = false;
        slider.classList.remove('active');
    });
    slider.addEventListener('mouseup', () => {
        isDown = false;
        slider.classList.remove('active');
    });
    slider.addEventListener('mousemove', (e) => {
        if(!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walk = (x - startX) * 3;
        slider.scrollLeft = scrollLeft - walk;
        console.log(walk);
    });
})
</script>

</body>
</html>
