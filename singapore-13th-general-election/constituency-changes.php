
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8"> 
<meta charset="utf-8">

<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
dataLayer.push({
    "level2": "INTERACTIVE-NAME",
    "title": "HEADLINE TITLE",
});
</script>
<!-- Google Tag Manager
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WVFMLMK');</script> -->
<!-- End Google Tag Manager -->

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="robots" content="index,follow">
<meta name="GOOGLEBOT" content="index,follow">
<meta http-equiv="cache-control" content="max-age=0, no-cache, no-store, must-revalidate" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="author" content="" />

<title>选区划分</title>
<meta name="description" content="SOMETHING SHORT AND BRIEF ENOUGH TO DESCRIBE THE TOPIC" />
<meta name="keywords" content="早报, KEYWORDS HIGHLY RELEVANT TO THE TOPIC, NAMES ARE IMPORTANT TOO" />
<link rel="image_src" href="//interactive.zaobao.com/YYYY/FOLDER-NAME/IMGS/SOCIAL-SHARE-IMG" />

<?php include "assets/inc/global.html" ?>
</head>

<body id="page-electoral-boundaries">
<?php include "assets/inc/gtm.html" ?>

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

<?php include "assets/inc/brand.html" ?>

  <div class="content mdl-layout__content">
    <a name="top"></a>

    <div class="tab-wrap">
      <div class="sticker000">
        <nav class="tab-navigation content-section">
          <ul class="links-container">
            <li class="nav-link"><a href="sg-electoral-boundaries.php">选区一览</a></li>
            <li class="nav-link active"><a href="constituency-changes.php">你不可不知的变化</a></li>
          </ul>
        </nav>
      </div>
      <div class="tab-container">
        <div class="000tab-container-inner">
          <div class="000swiper-wrapper">
            <div class="000swiper-slide">
              <div class="content-section">
                <p class="padding-10">本届大选中，部分选区范围有变，选区总数从原有的29个增至31个。议席总数从89个增至93个。1997年起出现的六人集选区也走入历史，从2006年全国大选至今，集选区的平均规模已从5.36个席位，减少至4.65个席位。本届选区的“四、三、二”重点变化，你不得不知。</p>
              </div>

              <div class="container" id="mapSlice">
                <h2 class="header-title mdl-typography--text-center">四个新选区</h2>
                <div class="map-slice new-smc card-container mdl-grid">
                  <div class="map-slice-box mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
                    <div class="map-slice-item">
                      <img src="imgs/electoral-boundaries/kebun-baru-new-smc.svg" border="0" class="m-slice" />
                      <h3>哥本峇鲁</h3>
                      <p>哥本峇鲁在2011年大选中原属宏茂桥集选区，2015年被划入义顺。2020年被划分为单选区，这里共有2万2413名选民。该区现任议员郭献川是上届大选的行动党新人。</p>
                    </div>
                  </div>
                  <div class="map-slice-box mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
                    <div class="map-slice-item">
                      <img src="imgs/electoral-boundaries/marymount-new-smc.svg" border="0" class="m-slice" />
                      <h3>玛丽蒙</h3>
                      <p>碧山——大巴窑集选区从五人集选区缩小至四人集选区后，碧山北——汤申区一带被划分为全新的玛丽蒙单选区，这里有2万3439名选民。该区现任议员是人力部长杨莉明。</p>
                    </div>
                  </div>
                  <div class="map-slice-box mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
                    <div class="map-slice-item">
                      <img src="imgs/electoral-boundaries/punggol-west-new-smc.svg" border="0" class="m-slice" />
                      <h3>榜鹅西</h3>
                      <p>原为六人集选区的白沙——榜鹅集选区选民人数饱和后，部分范围被划分为榜鹅西单选区，这里共有2万5440名选民。该区现任议员是内政部兼国家发展部高级政务次长孙雪玲。</p>
                    </div>
                  </div>
                  <div class="map-slice-box mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
                    <div class="map-slice-item">
                      <img src="imgs/electoral-boundaries/yio-chu-kang-new-smc.svg" border="0" class="m-slice" />
                      <h3>杨厝港</h3>
                      <p>宏茂桥集选区从六人集选区缩小为五人集选区后，部分范围被划分为杨厝港单选区，这里共有2万6046名选民。该区现任议员是贸工部高级政务部长许宝琨医生。</p>
                    </div>
                  </div>
                </div>
                
                <h2 class="header-title mdl-typography--text-center">3个消失的单选区</h2>
                <div class="map-slice obsolete-smc card-container mdl-grid">
                  <div class="map-slice-box mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
                    <div class="map-slice-item">
                      <img src="imgs/electoral-boundaries/fengshan-smc.svg" border="0" class="m-slice" />
                      <h3>凤山</h3>
                      <p>凤山单选区和东海岸集选区自1997年以来分分合合，凤山曾在1997年并入竞争激烈的东海岸集选区，在2015年大选中被划分为单选区，如今又被并回东海岸集选区。凤山区是工人党竞逐的热点之一，人民行动党的陈慧玲和工人党的陈立峰在上届大选中，展开备受瞩目的新人之争，行动党最终获得57.5%的选票赢得席位。</p>
                    </div>
                  </div>
                  <div class="map-slice-box mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
                    <div class="map-slice-item">
                      <img src="imgs/electoral-boundaries/punggol-east-smc.svg" border="0" class="m-slice" />
                      <h3>榜鹅东</h3>
                      <p>拥有3万5477名选民的榜鹅东在2013年曾因原议员柏默辞职，而举行补选。补选掀起四角战，行动党新人许宝琨医生最终不敌工人党的李丽连。2015年大选，行动党派出七届议员张有福，成功收复失地。榜鹅东本届大选被并入新划分出来的盛港集选区，它和盛港西单选区部分区域，以及部分白沙——榜鹅集选区一起形成一个拥有11万7000多人的盛港集选区。</p>
                    </div>
                  </div>
                  <div class="map-slice-box mdl-cell mdl-cell--4-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
                    <div class="map-slice-item">
                      <img src="imgs/electoral-boundaries/sengkang-west-smc.svg" border="0" class="m-slice" />
                      <h3>盛港西</h3>
                      <p>盛港西是2011年划分出来的单选区，在本届大选中解体，大部分范围被划入新的盛港集选区，一小部分被划入宏茂桥集选区。在上届大选中，盛港西已经有部分范围被划入宏茂桥集选区。</p>
                    </div>
                  </div>
                </div>

                <h2 class="header-title mdl-typography--text-center">2个缩小的六人集选区</h2>
                <div class="map-slice downsize-6-grc card-container mdl-grid">
                  <div class="map-slice-box mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
                    <div class="map-slice-item">
                      <img src="imgs/electoral-boundaries/ang-mo-kio-5grc.svg" border="0" class="m-slice" />
                      <h3>宏茂桥集选区</h3>
                      <p>宏茂桥集选区是李显龙总理领军的老牌集选区，在1991年宏茂桥区、哥本峇魯区、德义区和杨厝港区合并后组成四人集选区，2001年扩大至六人集选区的规模。这次，杨厝港区被划分为单选区，宏茂桥集选区另外吸收了原属盛港西单选区的部分范围。</p>
                      <p>宏茂桥集选区在上届大选中共有18万5577名选民，在此次大选中选民人数虽然减少至18万零186人，但仍取代白沙——榜鹅集选区，成为选民人数最多的集选区。</p>
                    </div>
                  </div>
                  <div class="map-slice-box mdl-cell mdl-cell--6-col mdl-cell--12-col-tablet mdl-cell--12-col-phone mdl-card">
                    <div class="map-slice-item">
                      <img src="imgs/electoral-boundaries/pasir-ris-punggol-5grc.svg" border="0" class="m-slice" />
                      <h3>白沙—榜鹅集选区</h3>
                      <p>白沙——榜鹅集选区2006年便扩大为六人集选区。上届大选中，它是最大的选区，由国务资政兼国家安全统筹部长张志贤领军。该区本届大选的选民人数从24万2225人大幅减少至16万1952人，部分范围被划入淡滨尼集选区，以及新增的盛港集选区和榜鹅西单选区。</p>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>
</div>
<?php include "assets/inc/news-feed.html"?>
<?php include "assets/inc/footer.html"?>
<?php include "assets/inc/scripts.html"?>

</body>
</html>
